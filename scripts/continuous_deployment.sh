#!/bin/bash

set -e

root_directory="$(dirname $0)/.."

cd "$root_directory"

# Install server and test suite
make CONF=dev_prod override_config package
