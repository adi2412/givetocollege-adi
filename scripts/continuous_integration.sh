#!/bin/bash

set -e

root_directory="$(dirname $0)/.."

cd "$root_directory"

# Install server and test suite
make CONF=jenkins clean_reports install_server install_dependencies
make CONF=jenkins override_config

# jslint reporting
make jslint

# documentation generation
make docs

# run grunt tasks
make grunt

# Unit tests
make REPORTER_NODEUNIT=junit tests
make unittests_jenkins

# Acceptance tests
make CONF=jenkins install_tests install_tests_dependencies
# Cleanup the DB and create the superuser
./scripts/recreate_test_db.sh
# Instrument files for coverage
make CONF=jenkins cover install_dependencies_instrumented
# Start the server, run the tests, kill the started server
# The server is started in production environment
cd ./scripts/node-jscoverage-executer
NODE_ENV=production ./node-jscoverage-executer ../../lib_instrumented/app.js & pid=$!
sleep 3
cd ../..
( set +e && make acceptance_tests )
exit_code=$?
kill -2 $pid

if [ $exit_code -ne 0 ]
then
  exit 1
fi

sleep 3

make create_cover_reports
