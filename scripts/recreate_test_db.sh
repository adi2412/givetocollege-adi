#!/bin/bash -x

root_directory=$(dirname "$0")/..

cd "$root_directory"
mongo 127.0.0.1:27017 lib/scripts/drop_test_database.js

node lib/scripts/cli.js adduser -f "first" -l "last" -e "admin@gtc.com" -p "test" -r "superadmin"
node lib/scripts/cli.js import_universities --file ./lib/scripts/data/universities.csv
