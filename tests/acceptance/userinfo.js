// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var expect = require('expect.js'),
    Browser = require('zombie'),
    server_configuration = require('./server_configuration'),
    browser = new Browser();

describe('forbidden user profile page', function() {
  it('should return a 302 redirect if a user is not logged in.', function() {
    browser.visit( server_configuration.url_server + "/user/profile", function() {
      expect(browser.statusCode).to.be(302);
      browser.visit( server_configuration.url_server + "/login", function() {
        expect(browser.success).to.be.ok();
        browser.
          fill("email","admin@gtc.com").
          fill("password","test").
          pressButton("Log in", function() {
            expect(browser.success).to.be.ok();
            expect(browser.text("title")).to.be("Homepage");
            browser.visit( server_configuration.url_server + "/user/profile", function() {
              expect(browser.success).to.be.ok();
              expect(browser.text("title")).to.be("User Information");
              done();
          });
        });
    });

    });
  });
});

