/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var expect = require('expect.js'),
    Browser = require('zombie'),
    uuid = require('node-uuid'),
    server_configuration = require('../server_configuration');

var browser = new Browser();
browser.site = server_configuration.url_server;
browser.debug = true;
describe('forgotten password', function() {
  this.timeout(15000);
  it('should create a user and execute the forgotten password workflow', function(done) {
    var ms, mailServer, handler, port = 2525;
    ms = require('smtp-tester');
    mailServer = ms.init(port);
    var emailTo = uuid.v4() + '@me.com';
    handler = function(address, id, email) {
      expect(email.sender).to.be('support@givetocollege.com');
      expect(email.receivers[emailTo]).to.be(true);
    };
    mailServer.bind(handler);
    browser.visit('/user/register', function() {
      expect(browser.success).to.be.ok();
      browser.
        fill('fullName','First Last').
        fill('email',emailTo).
        fill('password','my_password').
        pressButton('Next', function() {
          expect(browser.success).to.be.ok();
//          expect(browser.location.path).to.be("/user/homepage");
          browser.visit('/logout', function() {
            expect(browser.success).to.be.ok();
            browser.visit('/password/forgot', function() {
              expect(browser.success).to.be.ok();
              browser.
                fill('email',emailTo).
                pressButton('Submit', function() {
                  expect(browser.success).to.be.ok();
                  done();
                });
            });
          });
        });
    });
  });
});

