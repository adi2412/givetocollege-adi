var Browser = require('zombie');
var expect = require('expect');
var server_configuration = require('./server_configuration');
var browser = new Browser();

describe('index page', function() {
  it('should display the forgot password page', function(done) {
    browser.visit(server_configuration.url_server + '/password/forgot', function() {
      expect(browser.success).to.be.ok();
      done();
    });
  });
});
