var expect = require('expect.js'),
    Browser = require('zombie'),
    server_configuration = require('../server_configuration'),
    browser = new Browser();

describe('create organization test', function() {
  it('should login a user and create an organization', function(done) {
    browser.visit( server_configuration.url_server + "/login", function() {
      expect(browser.success).to.be.ok();
      browser.
        fill("email","admin@gtc.com").
        fill("password","test").
        pressButton("Log in", function() {
          expect(browser.success).to.be.ok();
          expect(browser.text("title")).to.be("Homepage");
          browser.visit( server_configuration.url_server + "/organization/create", function() {
            expect(browser.success).to.be.ok();
            expect(browser.text("title")).to.be("Organization Page");
            browser.
              fill("display_name", "Test Organization").
              pressButton("Create", function() {
                expect(browser.success).to.be.ok();
                expect(browser.text("title")).to.be("Organization Page");
                done();
              });
          });
        });
    });
  });
});
