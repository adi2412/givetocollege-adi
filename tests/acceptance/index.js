var expect = require('expect.js'),
    Browser = require('zombie'),
    server_configuration = require('./server_configuration'),
    browser = new Browser();

describe('index page', function(){
  it('should display the index page', function(done){
    browser.visit( server_configuration.url_server + "/")
    .then(function(){
      expect(browser.success).to.be.ok();
      done();
    });
  });
});

describe('login page', function() {
  it('should display the login page', function(done) {
    browser.visit( server_configuration.url_server + "/login", function() {
      expect(browser.success).to.be.ok();
      done();
    });
  });
});

describe('login test', function() {
  it('should login and logout a user', function(done) {
    browser.visit( server_configuration.url_server + "/login", function() {
      expect(browser.success).to.be.ok();
      browser.
        fill("email","admin@gtc.com").
        fill("password","test").
        pressButton("Log in", function() {
          expect(browser.success).to.be.ok();
          expect(browser.text("title")).to.be("Homepage");
          browser.visit( server_configuration.url_server + "/logout", function() {
            expect(browser.success).to.be.ok();
            expect(browser.text("title")).to.be("Give To College");
            done();
          });
        });
    });
  });
});

describe('register page', function(){
  it('should display the register page', function(done){
    browser.visit( server_configuration.url_server + "/user/register", function() {
      expect(browser.success).to.be.ok();
      done();
    });
  });
});

describe('register test', function() {
  it('should register a user', function(done) {
    browser.visit( server_configuration.url_server + "/user/register", function() {
      expect(browser.success).to.be.ok();
      browser.
        fill("fullName","First Last").
        fill("school_name","Test University").
        fill("email","test@test.com").
        fill("password","test").
        pressButton("Sign Up", function(){
          expect(browser.success).to.be.ok();
          expect(browser.text("title")).to.be("Homepage");
          done();
        });
      // TODO: Add registering user test cases.
    });
  });
});
