/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var expect = require('expect.js'),
    Browser = require('zombie'),
    uuid = require('node-uuid'),
    server_configuration = require('../server_configuration');

var browser = new Browser();
browser.site = server_configuration.url_server;
browser.debug = true;
describe('forgotten password', function() {
  this.timeout(5000);
  it('should create a user and change his profile', function(done) {
    var email = uuid.v4() + '@me.com';
    browser.visit('/user/register', function() {
      expect(browser.success).to.be.ok();
      browser.
        fill('fullName','First Last').
        fill('email',email).
        fill('password','my_password').
        pressButton('Next', function() {
          expect(browser.success).to.be.ok();
          browser.clickLink('Profile',function() {
            expect(browser.success).to.be.ok();
            browser.
              fill('first','NewFirst').
              pressButton('Update',function(){
                expect(browser.success).to.be.ok();
                done();
              });
          });
        });
    });
  });
});

