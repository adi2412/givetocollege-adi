var nconf = require('nconf');

var configuration = require('./configuration');
configuration.configureApplication(__dirname + '/config');

exports.url_server = 'http://localhost:' + nconf.get('http:port');
