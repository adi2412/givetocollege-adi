// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var amqp = require('amqp'),
    config = require('./configuration'),
    util = require("util"),
    emailServer = require("./email");

var connectionConfiguration;
var connection;

// Queues
var emailQueueName = "email-queue";

exports.productionEmailSubscriber = function(message) {
  try {
    var email = JSON.parse(message.data.toString());
    emailServer.sendMailToServer(email);
  } catch(e) {
    console.error(e);
    // Republish the message on the queue, after a wait period.
    setTimeOut(function() { publishEmail(message); }, 30000);
  }
};

exports.developmentEmailSubscriber = function(message) {
  var email = JSON.parse(message.data.toString());
  console.log("=== Sending email ===");
  console.log("From: ", email.from);
  console.log("To: ", email.to);
  console.log("Subject: ", email.subject);
  console.log("== Text ==");
  console.log(email.text);
  console.log("== HTML ==");
  console.log(email.html);
  console.log("=== EOF ===");
};

exports.subscribeToQueue = function(subscriber) {
  if (! connection) {
    connectionConfiguration = config.get("rabbitmq:protocol") + "://" +
        config.get("rabbitmq:login") + ":" + config.get("rabbitmq:password") + "@" +
        config.get("rabbitmq:host") + ":" + config.get("rabbitmq:port") + config.get("rabbitmq:vhost");
    connection = amqp.createConnection({ url: connectionConfiguration });
  }

  // If the subscriber is undefined, subscribe the development one.
  if(!subscriber) {
    subscriber = developmentEmailSubscriber;
  }

  // Set up subscriber.
  connection.on('ready', function () {
    connection.queue(emailQueueName, { durable: true, autoDelete: false }, function(emailQueue){
      console.log("Subscribed to " + emailQueueName);
      console.log("subscriber: " + subscriber);
      emailQueue.bind('#');
      emailQueue.subscribe(subscriber);
    });
  });
};

// Publish an email, ie add the email message to the queue.
exports.publishEmail = function(message) {
  if(config.get("rabbitmq:enabled")) {
    connection.publish(emailQueueName, message);
  }
};

exports.closeQueueServerConnection = function() {
  console.log("Closing ampq connection");
  if(connection) {
    connection.end();
  }
};
