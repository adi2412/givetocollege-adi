// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var
  express = require('express.io'),
  MongoStore = require('connect-mongo')(express),
  mongoose = require('mongoose'),
  passport = require('passport'),
  flash = require('connect-flash'),
  emailQueue = require('./queue'),
  eventQueue = require('./event_queue'),
  email = require('./email'),
  configuration = require('./configuration'),
  log4js = require('log4js');

// log4js configuration.
log4js.configure(__dirname + '/config/log4js_configuration.json');
var logger = log4js.getLogger('node');
logger.info('*** Restarting application ***');

var app = express();

configuration.configureApplication(__dirname + '/config');

var sessionStore = new MongoStore(
    {
      db: configuration.get('mongodb:db_name'),
      host: configuration.get('mongodb:server'),
      clear_interval: configuration.get('session:clear_interval')
    });

var cookieParser = express.cookieParser(configuration.get('session:secret'));

app.configure(function() {
  app.set('port', configuration.get('http:port'));
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(cookieParser);
  app.use(express.session({
    secret: configuration.get('session:secret'),
    maxAge: configuration.get('session:maxAge'),
    store: sessionStore
  }));
  app.use(flash());
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(function(request, response, next) {
    response.locals.user = request.user;
    if(request.user) {
      response.locals.roleLevel = request.user.roleLevel;
    }
    next();
  });
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  app.use(express.static(__dirname + '/public'));
  app.locals(require('./views/helpers.js').helpers);
});

app.use(function(req, res, next){
  res.render('error/404', { title: 'Give To College: Page Not Found' });
  next();
});

var connection = 'mongodb://' + configuration.get('mongodb:server') + '/' + configuration.get('mongodb:db_name');
logger.info('Database connection = ' + connection);
mongoose.connect(connection);

app.configure('development', function() {
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  app.use(log4js.connectLogger(logger, { level: log4js.levels.DEBUG }));
  mongoose.set('debug', logger);
  if(configuration.get('rabbitmq:enabled')) {
    eventQueue.initialize();
    emailQueue.subscribeToQueue(emailQueue.developmentEmailSubscriber);
  }
});

app.configure('production', function() {
  app.use(function(error, request, response, next){
    response.render('error/500', { title: 'Give To College: Unrecoverable Error' });
    next();
  });
  mongoose.set('debug', false);
  app.use(log4js.connectLogger(logger, { level: log4js.levels.WARN }));
  // Subscribe to the queues
  if(configuration.get('rabbitmq:enabled')) {
    eventQueue.initialize();
    emailQueue.subscribeToQueue(emailQueue.productionEmailSubscriber);
  }
});

// Configure authentication
require('./authentication')(passport);

// Configure the routes.
require('./routes/controller')(app);

// Initialization
require('./init')();

// Create the http server and set up socket io.
app.http().io();

app.io.configure(function () {
  app.io.set('authorization', function (data, callback) {
    logger.debug('socket.io authorization');
    cookieParser(data, {}, function(err) {
      if(err) {
        logger.debug('Error parsing cookie');
        logger.debug(err);
        callback(err, false);
      } else {
        sessionStore.get(data.signedCookies['connect.sid'], function(err, session) {
          if(err) {
            logger.debug('Session error');
            logger.debug(err);
            callback('Session error', false);
          } else {
            if(!session || !session.passport || !session.passport.user) {
              logger.debug('No Session found');
              callback('Session not found', false);
            } else {
              logger.debug('Session found');
              logger.debug(session);
              data.session = session;
              callback(null, true);
            }
          }
        });
      }
    });
  });
});

app.io.on('connection',function(socket) {
  var session = socket.handshake.session;
  logger.debug('socket.io connection');
  logger.debug(session);
  if(!session || !session.passport || !session.passport.user) {
    logger.debug('No Session found');
  } else {
    socket.join(session.passport.user._id);
  }
});

// Start the server.
app.listen(app.get('port'), function() {
  logger.info('Express server listening on port ' + app.get('port'));
});

process.on('exit', function() {
  if(configuration.get('rabbitmq:enabled')) {
    logger.info('Closing queue connections.');
    emailQueue.closeQueueServerConnection();
    eventQueue.destroy();
  }
  email.closeTransport();
});

process.on('SIGINT', function() {
  logger.info( 'gracefully shutting down from  SIGINT (Crtl-C)' );
  process.exit();
});
