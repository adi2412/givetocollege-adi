// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var check = require('validator').check,
    configuration = require('./configuration'),
    extend = require('extend'),
    facebook = require('./routes/http/facebook'),
    log4js = require('log4js'),
    login = require('./routes/http/login'),
    passwordUtils = require('./common/utils/password_util'),
    twitter = require('./routes/http/twitter'),
    LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    GoogleStrategy = require('passport-google-oauth').Strategy,
    InputErrors = require('./lib/util/InputError');

// Logger
var logger = log4js.getLogger('authentication');

// Schema related imports
var Campaign = require('./lib/models/campaign'),
    User = require('./lib/models/user'),
    UserLogin = require('./lib/models/userLogin');

var AUTHENTICATION_MESSAGES = {
  serverError: 'An error occurred during authentication.  Please try again later.',
  invalidLogin: 'Invalid username/password combination.',
  facebookError: 'An unknown error occurred while trying to access your Facebook account.  Please try again later.'
};

module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
    done(null, JSON.stringify({
      _id: user._id,
      loginType: user.loginType
    }));
  });

  passport.deserializeUser(function(userSerialized, done) {
    var errors = new InputErrors();
    try {
      var user = JSON.parse(userSerialized);
      // Dynamic information needs to be looked up anyways, so
      User.findById(user._id)
        .populate('campaigns')
        .exec(function(err, result) {
          if (err || !result) {
            logger.error('An error occurred while looking up user ' + err);
          } else {
            //logger.debug('Successfully deserialized user: ' + JSON.stringify(result));
          }
          user = extend(result, user);
          return done(errors.getErrors(), user);
      });
    } catch(err) {
      logger.warn('Failed parsing user: ' + err);
      errors.add(AUTHENTICATION_MESSAGES.serverError);
      return done(errors.getErrors(), false);
    }
  });

  passport.use(new LocalStrategy(
    { 
      usernameField: 'email',
      passwordField: 'password'
    },
    function(username, password, done) {
      // Validation of the parameters before sending them to the DB.
      try {
        check(username).isEmail();
      } catch(e) {
        return done(null, false, { message: AUTHENTICATION_MESSAGES.invalidLogin });
      }

      UserLogin.findOne({ userKey: login.getLoginKey('email', username) })
        .populate('userRef')
        .exec(function(err, userLogin) {
          if (err) {
            logger.warn('An error occurred while looking up user for login: ' + err);
            return done(null, false, { message: AUTHENTICATION_MESSAGES.serverError });
          }
          if (!userLogin) {
            return done(null, false, { message: AUTHENTICATION_MESSAGES.invalidLogin });
          }
          var user = userLogin.userRef;
          user.userKey = userLogin.userKey;
          user.loginType = 'email';
          var validated = passwordUtils.validatePassword(userLogin.passwordHash, password);
          if (!validated) {
            return done(null, false, { message: AUTHENTICATION_MESSAGES.invalidLogin });
          }
          logger.debug('Done authenticating user: ' + user);
          return done(null, user);
        });
    }
  ));

  passport.use('facebook', new FacebookStrategy({
    clientID: configuration.get('facebook:clientID'),
    clientSecret: configuration.get('facebook:clientSecret'),
    // Facebook is very strict about their callbacks anyways, so no point in doing anything fancy here.
    callbackURL: configuration.get('facebook:callbackURL')
  }, function(accessToken, refreshToken, params, profile, done) {
    logger.debug('Received callback from Facebook: ' + JSON.stringify(profile));
    // Verify that an incoming request has not expired.
    if (profile === undefined) {
      logger.warn('Did not receive profile object: ' + JSON.stringify(accessToken));
      return done(null, false, { message: AUTHENTICATION_MESSAGES.facebookError });
    }

    facebook.getOrCreateUser(profile, done); 
  }));

  passport.use('twitter', new TwitterStrategy({
    consumerKey: configuration.get('twitter:consumerKey'),
    consumerSecret: configuration.get('twitter:consumerSecret'),
    callbackURL: configuration.get('twitter:callbackURL')
  }, function(accessToken, refreshToken, params, profile, done) {
    logger.debug('Received callback from Twitter: ' + JSON.stringify(profile));
    // Verify that an incoming request has not expired.
    if (profile === undefined) {
      logger.warn('Did not receive profile object: ' + JSON.stringify(accessToken));
      return done(null, false, { message: AUTHENTICATION_MESSAGES.twitterErro });
    }

    twitter.getOrCreateUser(profile, done); 
  }));

  passport.use('google-auth', new GoogleStrategy({
    consumerKey: configuration.get('google:consumerKey'),
    consumerSecret: configuration.get('google:consumerSecret'),
    callbackURL: configuration.get('google:callbackURL'),
    passReqToCallback: true
  }, function(request, token, tokenSecret, profile, done) {
    if(!request.user) {
      // Not authenticated, send back a 403 error, we should already be authenticated.
      return done('error');
    } else {
      var account = new Account();
      account.domain = 'google.com';
      account.uid = profile.id;
      var t = { kind: 'oauth', token: token, attributes: { tokenSecret: tokenSecret } };
      account.tokens.push(t);
      return done(null, account);
    }
  }));
};
