// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
* Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/
var expect = require('expect.js');
var roles = require('../roles'),
    sinon = require('sinon');

describe('role', function() {
  describe('getRoleLevel', function() {
    it('should return the correct role level', function(done) {
      expect(roles.getRoleLevel(requestNone.user)).to.be(0);
      expect(roles.getRoleLevel(requestUser.user)).to.be(1);
      expect(roles.getRoleLevel(requestAdmin.user)).to.be(2);
      expect(roles.getRoleLevel(requestUserAndAdmin.user)).to.be(2);
      expect(roles.getRoleLevel(requestSuperAdmin.user)).to.be(3);
      done();
    });
  });
});

// Define the fake requests and the response spies.
var flashFunction = function() {};
var requestNone = { flash: flashFunction };
var requestUser = { user: { user_roles: [ ], admin_roles: [ ] }, flash: flashFunction };
var requestAdmin = { user: { admin_roles: [ 12345 ] }, flash: flashFunction };
var requestUserAndAdmin = { user: { user_roles: [ ], admin_roles: [ 12345 ] }, flash: flashFunction };
var requestSuperAdmin = { user: { superadmin: true }, flash: flashFunction };
var responseObject = { send: function(httpCode) {}, redirect: function() {} };
var responseSpy = sinon.spy(responseObject,'send');
var responseRedirectSpy = sinon.spy(responseObject,'redirect');
var nextSpy = sinon.spy();

var testNext = function(requireFunction, request, resultNext, resultRedirect) {
  requireFunction(request, responseObject, nextSpy);
  if(resultNext) {
    expect(nextSpy.calledOnce).to.be.ok();
  } else if (resultRedirect !== undefined && resultRedirect) {
    expect(responseRedirectSpy.calledOnce).to.be.ok();
  } else {
    expect(responseSpy.withArgs(403).calledOnce).to.be.ok();
  }
  responseSpy.reset();
  nextSpy.reset();
};

var testAll = function(roleRequired, testMatrix) {
  testMatrix.forEach(function(testToRun) {
    testNext(roleRequired, testToRun.request, testToRun.resultNext);
  });
};

describe('role require functions', function() {
  describe('requireNone', function() {
    it('should return true for all role levels', function(done) {
      testNext(roles.requireNone(), requestNone, true);
      testNext(roles.requireNone(), requestNone, true);
      testNext(roles.requireNone(), requestUser, true);
      testNext(roles.requireNone(), requestAdmin, true);
      testNext(roles.requireNone(), requestUserAndAdmin, true);
      testNext(roles.requireNone(), requestSuperAdmin, true);
      done();
    });
  });
  describe('requireUser', function() {
    it('should return true for user level', function(done) {
      testNext(roles.requireUser(), requestNone, false, true);
      testNext(roles.requireUser(), requestUser, true);
      testNext(roles.requireUser(), requestAdmin, true);
      testNext(roles.requireUser(), requestUserAndAdmin, true);
      testNext(roles.requireUser(), requestSuperAdmin, true);
      done();
    });
  });
  describe('requireAdmin', function() {
    it('should return true for admin level', function(done) {
      testNext(roles.requireAdmin(), requestNone, false);
      testNext(roles.requireAdmin(), requestUser, false);
      testNext(roles.requireAdmin(), requestAdmin, true);
      testNext(roles.requireAdmin(), requestUserAndAdmin, true);
      testNext(roles.requireAdmin(), requestSuperAdmin, true);
      done();
    });
  });
  describe('requireSuperAdmin', function() {
    it('should return true for superAdmin level', function(done) {
      var testMatrix =
        [ { request: requestNone, resultNext: false },
          { request: requestUser, resultNext: false },
          { request: requestAdmin, resultNext: false },
          { request: requestUserAndAdmin, resultNext: false },
          { request: requestSuperAdmin, resultNext: true } ];

      testAll(roles.requireSuperAdmin(), testMatrix);
      done();
    });
  });
});
