/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var expect = require('expect.js');
var Validator = require('../../../lib/models/validator');

describe('validator', function() {
  describe('addressValidator', function() {
    it('should not fail for undefined mailing address', function(done) {
      expect(Validator.addressValidator()).to.be.ok();
      done();
    });
    it('should not return any error for user with empty mailing address', function(done) {
      expect(Validator.addressValidator({})).to.be.ok();
      done();
    });
    it('should return true for a complete mailing address', function(done) {
      expect(Validator.addressValidator({ street1: 'test', city: 'test', state: 'CA', zip: '95120' })).to.be.ok();
      done();
    });
    it('should return false for incomplete mailing address', function(done) {
      [ 'street1', 'city', 'state', 'zip'].forEach(function(element) {
        var mailing_address = { street1: 'test', city: 'test', state: 'CA', zip: 95120 };
        mailing_address[element] = '';
        expect(Validator.addressValidator(mailing_address)).to.not.be.ok();
        delete mailing_address[element];
        expect(Validator.addressValidator(mailing_address)).to.not.be.ok();
      });
      done();
    });
  });

  describe('emailValidator', function() {
    it('should return true for correct email addresses, false otherwise', function(done) {
      expect(Validator.emailValidator("test@test.com")).to.be.ok();
      expect(Validator.emailValidator("test@test.test.com")).to.be.ok();
      expect(Validator.emailValidator("test@test")).to.not.be.ok();
      expect(Validator.emailValidator("test@test,com")).to.not.be.ok();
      expect(Validator.emailValidator("test.com")).to.not.be.ok();
      expect(Validator.emailValidator("&@test.com")).to.not.be.ok();
      expect(Validator.emailValidator(undefined)).to.not.be.ok();
      done();
    });
  });

  describe('stateValidator', function() {
    it('should return true for US states', function(done) {
      expect(Validator.stateValidator("CA")).to.be.ok();
      expect(Validator.stateValidator("MA")).to.be.ok();
      expect(Validator.stateValidator("WA")).to.be.ok();
      expect(Validator.stateValidator("TTT")).to.not.be.ok();
      expect(Validator.stateValidator(undefined)).to.be.ok();
      done();
    });
  });
});
