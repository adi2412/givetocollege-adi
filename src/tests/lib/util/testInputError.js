/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var expect = require('expect.js');
var InputError = require('../../../lib/util/InputError'),
    util = require('../../../lib/util/util');

describe('InputError', function() {
  describe('add', function() {
    it('should only allow general errors', function(done) {
      var errors = new InputError();
      expect(errors.getFields()).to.have.length(1);
      expect(errors.add('anykey', 'error1')).to.not.be.ok();
      expect(errors.add('general', 'error2')).to.be.ok();
      expect(errors.add('error3')).to.be.ok();
      expect(errors.get()).to.have.length(2);
      expect(errors.getFields()).to.have.length(1);
      expect(errors.get()[0]).to.be('error2');
      expect(errors.get()[1]).to.be('error3');
      done();
    });
    it('should only allows errors to be added to specific fields', function(done) {
      var errors = new InputError(['test']);
      expect(errors.getFields()).to.have.length(2);
      expect(errors.add('anykey', 'error1')).to.not.be.ok();
      expect(errors.add('general', 'error2')).to.be.ok();
      expect(errors.add('error3')).to.be.ok();
      expect(errors.add('test', 'error4')).to.be.ok();
      expect(errors.get('test')).to.have.length(1);
      expect(errors.get('test')[0]).to.be('error4');
      done();
    });
  });
  describe('clear', function() {
    it('should clear error messages', function(done) {
      var errors = new InputError(['test']);
      errors.add('test', 'error1');
      errors.add('test', 'error2');
      errors.add('test', 'error3');
      expect(errors.get('test')).to.have.length(3);
      errors.clear('test');
      expect(errors.get('test')).to.have.length(0);
      done();
    });
  });
  describe('toJSON', function() {
    it('Return errors in JSON format', function(done) {
      var errors = new InputError(['test']);
      errors.add('test', 'error1');
      errors.add('test', 'error2');
      errors.add('error3');
      var json = errors.toJSON();
      expect(json.test).to.have.length(2);
      expect(json.general).to.have.length(1);
      done();
    });
  });
  describe('get', function() {
    it('should return the errors', function(done) {
      var errors = new InputError(['test']);
      errors.add('test', 'error1');
      errors.add('test', 'error2');
      errors.add('test', 'error3');
      expect(errors.get('test')[0]).to.be('error1');
      expect(errors.get('test')[1]).to.be('error2');
      expect(errors.get('test')[2]).to.be('error3');
      done();
    });
  });
});
