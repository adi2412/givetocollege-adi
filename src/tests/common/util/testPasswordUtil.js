// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
* Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/
var expect = require('expect.js');
var passwordUtil = require ('../../../common/utils/password_util.js');

describe('passwordUtil', function() {
  describe('encryptPassword', function() {
    it('should return return true if password hashes are equals', function(done) {
      var hash = passwordUtil.encryptPassword("my password");
      expect(passwordUtil.validatePassword(hash,"my password")).to.be.ok();
      expect(passwordUtil.validatePassword(hash,"my wrong password")).to.not.be.ok();
      done();
    });
  });
});
