module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jade: {
      compile: {
        options: {
          compileDebug: false,  // No debug output (such as line numbers), reduced file size
          namespace: 'app.templates',
          client: true
        },
        files: {
          "views-precompiled.js": ["views/**/*.jade"]
        }
      }
    },
    cafemocha: {
      unittests: {
        src: 'tests/**/*.js',
        options: {
          ui: 'bdd',
          reporter: 'spec'
        }
      },
      unittests_jenkins: {
        src: 'tests/**/*.js',
        options: {
          reporter: 'xunit'
        }
      }
    }
  });

  // Load the plugin that provides the "jade" task.
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-cafe-mocha');

  // Default task(s).
  grunt.registerTask('default', ['jade']);
  grunt.registerTask('test', ['cafemocha:unittests']);
  grunt.registerTask('unittests_jenkins', ['cafemocha:unittests_jenkins']);
};
