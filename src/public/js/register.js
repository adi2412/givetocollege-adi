var GiveToCollege = GiveToCollege || {};

GiveToCollege.isValidEmail = function(email) {
  var em=/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
  return em.test(email);
}
GiveToCollege.isNameValid = function(name) {
  var nm=/[a-zA-Z]+ [a-zA-Z]+/;
  return nm.test(name);
}
GiveToCollege.setErrorMessage = function(sel,message) {
  $(sel).css({'color':'red','padding-left':'5px', 'font-style':'italic'});
  $(sel).html(message);
}

$(document).ready(function() {
  GiveToCollege.initializeOrganizationAutocomplete($('#school_name'));
  GiveToCollege.handleErrorMessages($('#errors'));
  // Enable client-side validation
  $('#email').blur(function() {
    var em=$('#email').val();
    if(!GiveToCollege.isValidEmail(em)) {
      GiveToCollege.setErrorMessage('#e-valid','Please enter a valid email address');
    } else {
      $('#e-valid').text('');
    }
  });
  $('#password').blur(function() {
    var pwd=$('#password').val();
    if(pwd.length<8) {
      GiveToCollege.setErrorMessage('#p-valid','Please enter at least 8 characters');
    } else {
      $('#p-valid').text('');
    }
  });
  $('#fullName').blur(function() {
    var fn=$('#fullName').val();
    if(fn.length<5 || !GiveToCollege.isNameValid(fn)) {
      GiveToCollege.setErrorMessage('#n-valid','Please enter you full name (ex: John Smith)');
    } else {
      $('#n-valid').text('');
    }
  });
  $('#registerForm').submit(function() {
    var ret=true;
    var em=$('#email').val();
    if(!GiveToCollege.isValidEmail(em)) {
      GiveToCollege.setErrorMessage('#e-valid','Please enter a valid email address');
      ret=false;
    }
    var pwd=$('#password').val();
    if(pwd.length<8) {
      GiveToCollege.setErrorMessage('#p-valid','Please enter at least 8 characters');
      ret=false;
    }
    var fn=$('#fullName').val();
    if(fn.length<5 || !isNameValid(fn)) {
      GiveToCollege.setErrorMessage('#n-valid','Please enter you full name (ex: John Smith)');
    } else {
      $('#n-valid').text('');
    }
    return ret;
  });
});
