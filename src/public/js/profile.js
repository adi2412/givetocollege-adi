$(function ($, _, Backbone) {

  "use strict";

  var Profile, ProfileView;

  var Profile = Backbone.Model.extend({
    idAttribute: "_id",
    url: function() {
      return '/user/info/' + this.get('id');
    },
    defaults: {
      email: '',
      first: '',
      last: '',
      dob: '',
      mailing_address: {
        street1: '',
        street2: '',
        city: '',
        state: '',
        zip: '',
      },
      user_roles: [],
    },
  });

  Profile = new Profile({ id: userId });

  ProfileView = Backbone.View.extend({
    tagName: 'div',
    el: $('#profile'),
    template: editable
        ? _.template($('#user-editable-template').html()) 
        : _.template($('#user-readonly-template').html()),

    role_template: editable
        ? _.template($('#role-editable-template').html()) 
        : _.template($('#role-readonly-template').html()),
    events: {
      "change .user-field .edit": 'update',
      "change .mailing-address-field .edit": 'updateMailingAddress',
      "mouseup #update-profile": "save",
      "mouseup #add-organization": 'startOrganizationEntry',
      "keypress #organization-entry": 'endOrganizationEntry',
      "click #organization-entry-button": 'endOrganizationEntry',
      "click .remove-organization": 'removeOrganizationEntry',
    },

    initialize: function () {
      Profile.bind('change', this.render, this);
      Profile.fetch();
    },

    startOrganizationEntry: function() {
      $('#add-organization').parent().remove();
      $('#organization-entry').slideDown();
      $('#organization-entry').focus();
      $('#organization-entry input').autocomplete('enable');
    },

    endOrganizationEntry: function(e) {
      if ((e.type == 'keypress' && e.keyCode === 13) || e.type == 'click') {
        var name = $('#organization-entry input').val();
        Profile.save('addToUser', name);
        Profile.unset('addToUser');
      }
    },

    removeOrganizationEntry: function(e) {
      var userRoles = []; 
      $(e.target)
          .parent().parent()
          .siblings('.role')
          .each(function(index, elem) {
        userRoles.push($(elem).attr('id').split('-')[1]);
      });
      Profile.save('user_roles', userRoles, {
        wait: true,
        success: $(e.target).parent().remove
      });
    },

    update: function(e) {
      Profile.set($(e.target).attr('name'), $(e.target).val());
    },

    updateMailingAddress: function(e) {
      Profile.get('mailing_address')[$(e.target).attr('name')] = $(e.target).val();
    },

    save: function() {
      Profile.save({ wait: true },
        { 
          success: function() { 
            $('#messages').children('div').hide();
            $('#success-messages')
            .addClass('alert')
            .addClass('alert-success')
            .text('Profile updated successfully')
            .slideDown();
          },
          error: function(model, response, options) {
            $('#messages').children('div').hide();
            var errorMessages = JSON.parse(response.responseText || '[]');
            $('#error-messages')
            .html('An error occurred while updating your profile.<br>' + errorMessages.join('<br>'))
            .addClass('alert')
            .addClass('alert-error')
            .slideDown();
          }
        });
    },

    render: function () {
      var profile = Profile.toJSON();
      if (profile.dob) {
        var d = new Date(profile.dob);
        var curr_date = d.getDate() + 1;
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        profile.dob = curr_month + "/" + curr_date + "/" + curr_year;
      }

      this.$el.html(this.template(profile));
      var hasOrganizations = false;
      $('#user-roles .role').remove();
      _.each(Profile.get('user_roles'), function(organization) {
        $('#user-roles').prepend(this.role_template(organization)); 
        hasOrganizations = true;
      }, this);

      var adminRoles = Profile.get('admin_roles')
      if (adminRoles && adminRoles.length) {
        _.each(adminRoles, function(organization) {
          $(_.template($('#admin-template').html())()).appendTo($('#roles'));
          $('#admin-roles tbody').prepend(_.template($('#role-readonly-template').html())(organization));
          hasOrganizations = true;
        }, this);
      }

      if (Profile.get('superadmin')) {
        $("<h3>You are Super Admin</h3>").appendTo($('#roles')); 
      }

      if (!hasOrganizations) {
        $('#no-organizations').css('display', 'block');
      }

      // Initialization of autocomplete needts to be done after profile views have been rendered
      GiveToCollege.initializeOrganizationAutocomplete($('#organization-entry input'));
      return this;
    },
  });

  ProfileView = new ProfileView();

}(jQuery, _, Backbone));