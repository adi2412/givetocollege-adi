$(function ($, _, Backbone) {

  "use strict";

  var Category, Categories, CategoryView, CategoriesView;
  var Category = Backbone.Model.extend({
    idAttribute: "name",
    defaults: {
      name: 'None',
      description: '',
    },
  });

  var Categories = Backbone.Collection.extend({
    model: Category
  });

  CategoryView = Backbone.View.extend({
    tagName: 'tr',
    template: _.template($('#category-template').html()),
    render: function () {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },
  });

  CategoriesView = Backbone.View.extend({
    el: $('#categories'),
    template: _.template($('#category-template').html()),
    events: { },

    initialize: function () {
      Categories.bind('change', this.render, this);
      this.categories = [];
      _.each(categories, function(category) {
        var category = new Category(category);
        this.categories.push(new CategoryView({ model: category }));
      }, this);
      this.render();
    },

    render: function () {
      _.each(this.categories, function(category) {
        this.$el.append(category.render().el);
      }, this);
    },
  });

  CategoriesView = new CategoriesView();

}(jQuery, _, Backbone));