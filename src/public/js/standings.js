$(function ($, _, Backbone) {

  "use strict";

  var College, CollegeList, Category, CategoryList, CategoryListView, StandingsView;
  var College = Backbone.Model.extend({
    defaults: {
      display_name: '',
      name: '',
      rank: -1,
      points: 0
    }

  });

  var CollegeList = Backbone.Collection.extend({
    model: College,
    initialize: function(options) {
      _.bindAll(this, 'url', 'gotoPage');
      this.userId = options.userId || '';
      this.maxPages = parseInt(options.maxPages);
      this.page = 1;
    },
    url: function() {
      if (!this.category || this.category == 'category-all') {
        return '/standings/college/' + this.page;
      }

      if (this.category == 'category-mine') {
        return '/standings/user/' + this.userId + '/universities';
      } else {
        return '/standings/college/category/' + this.category + '/' + this.page;
      }
    },
    gotoPage: function(page) {
      var newPage = page;
      if (newPage > this.maxPages) {
        newPage = this.maxPages
      } else if (newPage < 1) {
        newPage = 1;
      }

      if (newPage != this.page) {
        this.page = newPage;
        return this.fetch();
      }
    }
  });
  var userId = userId || '';
  CollegeList = new CollegeList({ userId: userId, maxPages: maxPages });

  var Category = Backbone.Model.extend({});
  var CategoryList = Backbone.Collection.extend({
    model: Category,
    url: function() {
      return '/category/list';
    }
  });
  CategoryList = new CategoryList();

  CategoryListView = Backbone.View.extend({
    el: $('#categories'),
    template: _.template($('#category-template').html()),
    events: {
      'click .category': 'updateCategory'
    },
    initialize: function() {
      CategoryList.bind('reset', this.render, this);
      CategoryList.fetch();
    },
    updateCategory: function(e) {
      var categoryId = $(e.target).attr('id');
      if (categoryId == 'category-all') {
        delete CollegeList.category;
      } else {
        CollegeList.category = categoryId;
      }
      $(e.target).parent().siblings().removeClass('active');
      $(e.target).parent().addClass('active');
      CollegeList.fetch();
    },
    render: function() {
      CategoryList.each(function(category) {
        this.$el.append(this.template(category.toJSON()));
      }, this);
    }
  });
  CategoryListView = new CategoryListView();

  StandingsView = Backbone.View.extend({
    el: $('#standings'),
    template: _.template($('#college-template').html()),

    events: {
      'click .page-button': 'gotoPage',
      'click #first-page': 'gotoFirstPage',
      'click #prev-page': 'gotoPrevPage',
      'click #next-page': 'gotoNextPage',
      'click #last-page': 'gotoLastPage',
    },

    initialize: function() {
      _.bindAll(this, 'gotoPage', 'gotoPrevPage', 'gotoNextPage');
      CollegeList.bind('reset', this.render, this);
      CollegeList.bind('add', this.render, this);
      CollegeList.bind('remove', this.render, this);
      CollegeList.bind('sort', this.render, this);
      CollegeList.fetch();
    },

    gotoPage: function(e) {
      var page = parseInt($(e.target).html());
      CollegeList.gotoPage(page);
    },

    gotoFirstPage: function() {
      CollegeList.gotoPage(1);
    },

    gotoPrevPage: function() {
      CollegeList.gotoPage(CollegeList.page - 1);
    },

    gotoNextPage: function() {
      CollegeList.gotoPage(CollegeList.page + 1);
    },

    gotoLastPage: function() {
      CollegeList.gotoPage(CollegeList.maxPages);
    },

    render: function() {
      this.$el.find('.content').empty();
      this.$el.find('.pagination ul').empty();

      CollegeList.each(function(college, index) {
        this.$el.find('.content').append(this.template(college.toJSON()));
      }, this);

      var startingPage = Math.max(1, CollegeList.page - 3);
      var lastPage = Math.min(CollegeList.maxPages, CollegeList.page + 3);

      var pagesRemaining = 6 - (lastPage - startingPage);
      if (pagesRemaining > 0) {
        if (lastPage == CollegeList.maxPages) {
          startingPage = Math.max(1, startingPage - pagesRemaining);
        } else if (startingPage == 1) {
          lastPage = Math.min(CollegeList.maxPages, lastPage + pagesRemaining);
        }
      }

      this.$el.find('.pagination ul').append('<li id="first-page"><a href="#">&lt;&lt;</a></li>');
      this.$el.find('.pagination ul').append('<li id="prev-page"><a href="#">&lt;</a></li>');
      for (var i = startingPage; i <= lastPage; i++) {
        var page = i;
        if (page == CollegeList.page) {
          this.$el.find('.pagination ul').append('<li class="page-button disabled"><a href="#">' + page + '</a></li>');
        } else {
          this.$el.find('.pagination ul').append('<li class="page-button"><a href="#">' + page + '</a></li>');
        }
      }
      this.$el.find('.pagination ul').append('<li><a href="#" id="next-page">&gt;</a></li>');
      this.$el.find('.pagination ul').append('<li><a href="#" id="last-page">&gt;&gt;</a></li>');
      return this;
    }
  });
  StandingsView = new StandingsView();

}(jQuery, _, Backbone));