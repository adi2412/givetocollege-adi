var Step1View, Step2View, Step3View, Step4View;

$(function ($, _, Backbone) {

  "use strict";

  var User, Organization;

  User = Backbone.Model.extend({
    idAttribute: "_id",
    url: function() {
      return '/user/info/' + this.get('id');
    },
    defaults: {
      email: '',
      first: '',
      last: '',
      dob: '',
      mailing_address: {
        street1: '',
        street2: '',
        city: '',
        state: '',
        zip: '',
      },
      user_roles: [],
    },
    parse: function(result) {
      var organizationList = [];
      _.each(result.user_roles, function(organization) {
        organizationList.push(new Organization(organization));
      });
      result.user_roles = organizationList;
      return result;
    }
  });

  User = new User({ id: userId });
  User.fetch();

  Organization = Backbone.Model.extend({
    defaults: {
      display_name: '',
      name: ''
    }
  });

  Step1View = Backbone.View.extend({
    el: $('#step1 .step-content'),
    template: _.template($('#step1-template').html()),
    events: {
      'change .user-field .edit': 'update'
    },

    initialize: function () {
      User.bind('change', this.render, this);
    },

    update: function(e) {
      User.set($(e.target).attr('name'), $(e.target).val());
    },

    save: function() {
      $('#step3-button')
      .attr('value', 'Please Wait')
      .addClass('disabled');

      User.save({ wait: true },
        { 
          success: function() { 
            $('#step1-status')
            .addClass('badge-success')
            .html('Completed');
            $('#step1-button').removeClass('disabled');
          },
          error: function(model, response, options) {
            $('#step1-status')
            .removeClass('badge-success')
            .html('Incomplete');
            $('#step1-button').removeClass('disabled');
          }
        });
    },

    render: function () {
      var user = User.toJSON();

      if (user.dob) {
        var d = new Date(user.dob);
        var curr_date = d.getDate() + 1;
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year = d.getFullYear();
        user.dob = curr_month + "/" + curr_date + "/" + curr_year;
      }

      if (user.email && user.first && user.last && user.dob) {
        $('#step1-status')
        .addClass('badge-success')
        .html('Completed');
        hideStep($('#step1'));

        this.complete = true;
        checkComplete();
      }

      this.$el.html(this.template(user));

      return this;
    },
  });

  Step2View = Backbone.View.extend({
    el: $('#step2 .step-content'),
    template: _.template($('#step2-template').html()),
    events: {
      'change .mailing-address-field .edit': 'update',
    },

    initialize: function () {
      User.bind('change', this.render, this);
    },

    update: function(e) {
      User.get('mailing_address')[$(e.target).attr('name')] = $(e.target).val();
    },

    save: function() {
      $('#step2-button')
      .attr('value', 'Please Wait')
      .addClass('disabled');

      User.save({ wait: true },
        { 
          success: function() { 
            $('#step2-status')
            .addClass('badge-success')
            .html('Completed');
            $('#step2-button').removeClass('disabled');
          },
          error: function(model, response, options) {
            $('#step2-status')
            .removeClass('badge-success')
            .html('Incomplete');
            $('#step2-button').removeClass('disabled');
          }
        });
    },

    render: function () {
      var user = User.toJSON();

      if (user.mailing_address.street1
          && user.mailing_address.city
          && user.mailing_address.state
          && user.mailing_address.zip) {
        $('#step2-status')
        .addClass('badge-success')
        .html('Completed');
        hideStep($('#step2'));

        this.complete = true;
        checkComplete();
      }

      this.$el.html(this.template(user));

      return this;
    },
  });

  Step3View = Backbone.View.extend({
    el: $('#organization-list tbody'),
    template: _.template($('#step3-template').html()),

    initialize: function () {
      User.bind('change', this.render, this);
    },

    save: function() {
      $('#step3-button')
      .attr('value', 'Please Wait')
      .addClass('disabled');

      var organization = $('#organization').val();

      User.save('addToUser', organization, { wait: true },
        { 
          success: function() { 
            $('#step3-status')
            .addClass('badge-success')
            .html('Completed');
            $('#step3-button').removeClass('disabled');
            User.unset('addToUser');
          },
          error: function(model, response, options) {
            $('#step3-status')
            .removeClass('badge-success')
            .html('Incomplete');
            $('#step3-button').removeClass('disabled');
            User.unset('addToUser');
          }
        });
    },

    render: function () {
      var user = User.toJSON();

      if (user.user_roles.length) {
        $('#organization-entry').hide();
        $('#organization-list').slideDown();
        $('#step3-status')
        .addClass('badge-success')
        .html('Completed');
        hideStep($('#step3'));

        this.$el.empty('.organization-entry');
        _.each(user.user_roles, function(organization) {
          this.$el.prepend(this.template(organization.toJSON()));
        }, this);

        this.complete = true;
        checkComplete();
      } else {
        $('#organization-entry').show();
        $('#organization-list').hide();
      }

      return this;
    },
  });

  Step4View = Backbone.View.extend({
    save: function() {
    },

    render: function () {
      var user = User.toJSON();

      if (user.channels.length) {
        $('#step4-status')
        .addClass('badge-success')
        .html('Completed');
        hideStep($('#step4'));

        _.each(user.channels, function(channel) {
          $('#'+channel.type).addClass('disabled');
        });

        this.complete = true;
        checkComplete();
      }

      return this;
    },
  });

  Step1View = new Step1View();
  Step2View = new Step2View();
  Step3View = new Step3View();
  Step4View = new Step4View();
}(jQuery, _, Backbone));