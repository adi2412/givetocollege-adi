$(function ($, _, Backbone) {

  "use strict";

  var User = Backbone.Model.extend({
    idAttribute: "_id",
    url: function() {
      return '/user/info/' + this.get('id');
    },
    defaults: {
      email: '',
      first: '',
      last: '',
      dob: '',
      mailing_address: {
        street1: '',
        street2: '',
        city: '',
        state: '',
        zip: '',
      },
      user_roles: [],
    },
  });

  User = new User({ id: GiveToCollege.userId });

  var UserAchievement = Backbone.Model.extend();
  var UserCampaign = Backbone.Model.extend();

  var UserAchievementList = Backbone.Collection.extend({
    model: UserAchievement,
    url: function() {
      return '/user/achievements';
    }
  });
  UserAchievementList = new UserAchievementList({});

  var UserCampaignList = Backbone.Collection.extend({
    model: UserCampaign,
    url: function() {
      return '/user/campaigns';
    }
  });
  UserCampaignList = new UserCampaignList({});

  var HomepageView = Backbone.View.extend({
    tagName: 'div',
    $el_userinfo: $('#sidebar-userinfo'),
    $el_badges: $('#sidebar-badges-content'),
    $el_campaigns: $('#campaigns'),
    $el_achievement_showcase: $('#achievement-showcase'),
    $el_achievements: $('#achievements'),

    userinfo_template: _.template($('#userinfo-template').html()),
    userbadge_template: _.template($('#userbadge-template').html()),
    campaign_template: _.template($('#campaign-template').html()),
    achievement_showcase_template: _.template($('#achievement-showcase-template').html()),
    achievement_template: _.template($('#achievement-template').html()),

    initialize: function () {
      User.bind('change', this.renderUserInfo, this);
      UserAchievementList.bind('reset', this.renderUserBadges, this);
      UserAchievementList.bind('reset', this.renderAchievements, this);
      UserCampaignList.bind('reset', this.renderCampaigns, this);

      User.fetch();
      UserCampaignList.fetch();
      UserAchievementList.fetch();
    },

    renderUserInfo: function() {
      var user = User.toJSON();
      this.$el_userinfo.html(this.userinfo_template(user));
    },

    renderUserBadges: function() {
      this.$el_badges.empty();
      var achievements = UserAchievementList
                    .filter(function(x) { return x.dateCompleted; })
                    .map(function(x) { return x.toJSON(); });

      if (achievements.length == 0) {
        this.$el_badges.html('None');
      } else {
        _.each(achievements, function(achievement) {
          if (achievement.badge) {
            this.$el_badges.append(this.userbadge_template(achievement));
          }
        }, this);
      }
    },

    renderCampaigns: function() {
      this.$el_campaigns.empty();
      var campaigns = UserCampaignList
                    .map(function(x) { return x.toJSON(); });
      if (campaigns.length == 0) {
        this.$el_campaigns.html('You have nothing to do!');
      } else {
        var $row = null;
        _.each(campaigns, function(campaign, index) {
          if (index % 4 == 0) {
            this.$el_campaigns.append('<div class="row-fluid">');
            $row = $(this.$el_campaigns.children('.row-fluid')).last();
          }
          $row.append(this.campaign_template(campaign));
        }, this);
      }
    },

    renderAchievements: function() {
      this.$el_achievements.empty();
      var achievements = UserAchievementList
                          .filter(function(x) { return x.get('achievement').shouldShow && !x.get('dateCompleted') })
                          .map(function(x) { return x.toJSON(); })
                          .sort(function(a, b) { return b.progress - a.progress; })
                          .slice(0, 6);
      if (achievements.length == 0) {
        this.$el_achievement_showcase.html('You have no achievements!');
      } else {
        this.$el_achievement_showcase.append(this.achievement_showcase_template(achievements[0]));
        _.each(achievements.slice(1), function(achievement, index) {
          this.$el_achievements.append(this.achievement_template(achievement));
        }, this);
      }
    }
  });

  HomepageView = new HomepageView();

}(jQuery, _, Backbone));