var GiveToCollege = GiveToCollege || {};

GiveToCollege.notificationsCheckInterval;
GiveToCollege.maxNotifications = 6;

GiveToCollege.getNotifications = function() {
  $.getJSON('/user/notifications', function(data) {
    $('#notifications .notification').remove();
    var unseen = data.unseen || [];
    var seen = data.seen || [];

    unseen = unseen.sort(function(a, b) { return b-a; });
    seen = seen.sort(function(a, b) { return b-a; });

    var total_unseen = 0;
    var total = 0;
    for (var i = 0; i < data.unseen.length && total < GiveToCollege.maxNotifications; i++) {
      var entry = data.unseen[i];
      $('#notifications').append('<li id="' + entry._id + '" class=" unseen-notification notification"><div class=notification-title>' + entry.title + '</div><div class=notification-description>' + entry.description + '</div>');
      total_unseen++;
      total++;
    }
    for (var i = 0; i < data.seen.length && total < GiveToCollege.maxNotifications; i++) {
      var entry = data.seen[i];
      $('#notifications').append('<li id="' + entry._id + '" class="notification"><div class=notification-title>' + entry.title + '</div><div class=notification-description>' + entry.description + '</div>');
      total++;
    }

    if (total_unseen > 0) {
      $('#notifications-toggle .badge').html(total_unseen).addClass('badge-important');
      $('#no-notifications').hide();
    } else if (total > 0) {
      $('#notifications-toggle .badge').html(0).removeClass('badge-important');
      $('#no-notifications').hide();
    } else {
      $('#notifications-toggle .badge').html(0).removeClass('badge-important');
      $('#no-notifications').show();
    }
  });
};

GiveToCollege.clearNotifications = function() {
  var entries = [];
  $('.unseen-notification')
  .each(function(index, entry) {
    entries.push($(entry).attr('id'));
  })

  $('#notifications-toggle .badge').html(0).removeClass('badge-important');

  $.post('/user/notifications/clear', { entries: entries });
}

$(document).ready(function() {
  GiveToCollege.notificationsCheckInterval = setInterval(GiveToCollege.getNotifications, 1000);
  $('#notifications-toggle').click(function() {
    GiveToCollege.clearNotifications();
  });
});