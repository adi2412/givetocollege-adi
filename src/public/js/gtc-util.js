var GiveToCollege = GiveToCollege || {};

GiveToCollege.handleErrorMessages = function($error_element) {
  GiveToCollege.handleMessages('error', $error_element || $('#error-messages'));
}

GiveToCollege.handleSuccessMessages = function($success_element) {
  GiveToCollege.handleMessages('success', $success_element || $('#error-messages'));
}

GiveToCollege.handleInfoMessages = function($info_element) {
  GiveToCollege.handleMessages('info', $info_element || $('#info-messages'));
}

GiveToCollege.handleMessages = function(type, $el) {
  var messages = GiveToCollege[type];

  console.log(messages);
  console.log($el);

  if (!$el) {
    return false;
  }

  var hasGeneralMessages = false;
  if (messages !== undefined && messages) {
    if (typeof(messages) == 'string') {
      $('<div class="alert">' + messages + '</div>')
      .appendTo($el)
      .addClass('alert-' + type);
      hasGeneralMessages = true;
    } else if (messages instanceof Array && messages.length) {
      var message = messages.join('<br>');
      $('<div class="alert">' + message + '</div>')
      .appendTo($el)
      .addClass('alert-' + type);
      hasGeneralMessages = true;
    } else {
      $.each(messages, function(key, value) {
        if (key == 'general') {
          $('<div class="alert">' + value + '</div>')
          .appendTo($el)
          .addClass('alert-' + type);
          hasGeneralMessages = true;
        } else {
          $('<div class="alert">' + value + '</div>')
          .insertAfter($('#'+key))
          .addClass('alert-' + type);
        }
      });
    }

    if (hasGeneralMessages) {
      $el.slideDown();
    }
  }
}



GiveToCollege.popupOAuthTimer = null;

GiveToCollege.popupOAuth = function(url, callback_url) {
  var thisWindow = window;
  var oauthPopup = window.open(url, "GTCOAuthPopup", 'location=1,menubar=0,scrollbars=0,status=0,toolbar=0', false);
  GiveToCollege.popupOAuthTimer = setInterval(function() {
    if (oauthPopup.closed) {
      thisWindow.location = callback_url;
      if (GiveToCollege.popupOAuthTimer != null) {
        clearInterval(GiveToCollege.popupOAuthTimer);
      }
    }
  }, 500);
}

GiveToCollege.isValidDonationAmount = function(amount) {
  return amount > 0;
}

GiveToCollege.universities = {};
GiveToCollege.initializeOrganizationAutocomplete = function(element) {
  element.autocomplete({
    enable: true,
    source: function (req, add) {
      $.getJSON('/autocomplete/organization', req, function(data) {
        var suggestions = [];  
        $.each(data, function(i, val) { suggestions.push(val.display_name); });
        add(suggestions);
      });
    }
  });
};
