// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Defines and registers the routes.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var controller_http = require('./controller.http');
var controller_io = require('./controller.io');

// Main controller, calls both http and socket.io controller.
module.exports = function(app) {
  controller_http(app);
  controller_io(app);
};
