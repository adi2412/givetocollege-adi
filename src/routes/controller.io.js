// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Defines and registers the routes.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

//var routes = require('./routes/io');
var roles = require('../roles');
var log4js = require('log4js');

var logger = log4js.getLogger('controller.io');

/*
  An array of JSON objects that denote the routes and the URL patterns that they should be registered to.
  A route entry should have the following:
    url: The url pattern to tie to this route.
    type: The type of HTTP request (e.g. get, post, etc.).  This should correspond with the express methods.
    role: The role needed to folow this route.
    route: The route function itself.
 */
var myRoutes = [
];

/*
  This function ties the routes to the appropriate URL patterns and HTTP type in express.
*/
module.exports = function(app) {
  myRoutes.forEach(function(route) {
    if (route && route.type && route.url && route.route && route.routeNext && route.role) {
      app[route.type](route.url, route.role, route.route, route.routeNext);
    } else if (route && route.type && route.url && route.route && route.role) {
      logger.info("Registered route: " + JSON.stringify(route));
      app[route.type](route.url, route.role, route.route);
    } else {
      logger.info("Could not register route: " + JSON.stringify(route));
    }
  });
};
