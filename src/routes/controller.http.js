// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Defines and registers the routes.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var routes = require('./http');
var roles = require('../roles');
var log4js = require('log4js');

var logger = log4js.getLogger('controller.http');

/*
  An array of JSON objects that denote the routes and the URL patterns that they should be registered to.
  A route entry should have the following:
    url: The url pattern to tie to this route.
    type: The type of HTTP request (e.g. get, post, etc.).  This should correspond with the express methods.
    role: The role needed to folow this route.
    route: The route function itself.
 */
var myRoutes = [
  { url: '/', type: 'get', role: roles.requireNone(), route: routes.index},
  // Login Routes
  { url: '/logout', type: 'get', role: roles.requireNone(), route: routes.login.logout },
  { url: '/login', type: 'get', role: roles.requireNone(), route: routes.login.login },
  { url: '/login/email/auth', type: 'post', role: roles.requireNone(), route: routes.login.loginEmail },
  { url: '/login/facebook', type: 'get', role: roles.requireNone(), route: routes.login.loginFacebook },
  { url: '/login/twitter', type: 'get', role: roles.requireNone(), route: routes.login.loginTwitter},
  { url: '/login', type: 'post', role: roles.requireNone(), route: routes.login.loginAuthentication, routeNext: routes.login.loginSuccess },
  { url: '/password/forgot', type: 'get', role: roles.requireNone(), route: routes.login.viewForgotPassword },
  { url: '/password/forgot', type: 'post', role: roles.requireNone(), route: routes.login.updateForgotPassword },
  { url: '/password/reset/:token', type: 'get', role: roles.requireNone(), route: routes.login.viewResetPassword },
  { url: '/password/reset', type: 'post', role: roles.requireNone(), route: routes.login.updateResetPassword },

  // OAuth Routes
  { url: '/user/facebook/callback', type: 'get', role: roles.requireNone(), route: routes.facebook.handleCallback },
  { url: '/user/twitter/callback', type: 'get', role: roles.requireNone(), route: routes.twitter.handleCallback },

  // User Routes
  { url: '/user/register', type: 'get', role: roles.requireNone(), route: routes.user.renderUserRegistration },
  { url: '/user/register/facebook/', type: 'get', role: roles.requireNone(), route: routes.facebook.initiateFacebookRegistration },
  { url: '/user/register/twitter/', type: 'get', role: roles.requireNone(), route: routes.twitter.initiateTwitterRegistration },
  { url: '/user/register/:invite', type: 'get', role: roles.requireNone(), route: routes.user.renderUserRegistration },
  { url: '/user/create', type: 'post', role: roles.requireNone(), route: routes.user.createUserFromEmail },
  { url: '/user/homepage', type: 'get', role: roles.requireUser(), route: routes.user.viewHomepage },
  { url: '/user/profile', type: 'get', role: roles.requireUser(), route: routes.user.viewUser },
  { url: '/user/profile/:id', type: 'get', role: roles.requireUser(), route: routes.user.viewUser },
  { url: '/user/settings', type: 'get', role: roles.requireUser(), route: routes.user.viewUserSettings },
  { url: '/user/list', type: 'get', role: roles.requireSuperAdmin(), route: routes.user.listUsers },
  { url: '/user/receipts', type: 'get', role: roles.requireUser(), route: routes.user.receipts },
  { url: '/user/info/:id', type: 'get', role: roles.requireUser(), route: routes.user.getInfo },
  { url: '/user/info/:id', type: 'put', role: roles.requireUser(), route: routes.user.putInfo },
  { url: '/user/campaigns', type: 'get', role: roles.requireUser(), route: routes.user.getCampaigns },
  { url: '/user/achievements', type: 'get', role: roles.requireUser(), route: routes.user.getAchievements },
  { url: '/user/roles/:id', type: 'get', role: roles.requireUser(), route: routes.user.getRoles },
  { url: '/user/changepassword', type: 'post', role: roles.requireUser(), route: routes.user.changePassword },

  // Invite routes
  { url: '/user/invite', type: 'get', role: roles.requireUser(), route: routes.invite.renderUserInvitation },
  { url: '/user/invite/email', type: 'post', role: roles.requireUser(), route: routes.invite.sendUserEmailInvitation },
  { url: '/user/invite/list', type: 'get', role: roles.requireUser(), route: routes.invite.listInvitations },
  { url: '/user/invite/gmail', type: 'get', role: roles.requireUser(), route: routes.invite.getGoogleContacts },
  { url: '/user/invite/gmail', type: 'post', role: roles.requireUser(), route: routes.invite.inviteGoogleContacts },
  { url: '/user/invite/facebook', type: 'get', role: roles.requireUser(), route: routes.invite.getFacebookFriends },
  { url: '/user/invite/facebook', type: 'post', role: roles.requireUser(), route: routes.invite.inviteFacebookFriends },
  { url: '/user/invite/twitter', type: 'get', role: roles.requireUser(), route: routes.invite.getTwitterFriends },
  { url: '/user/invite/twitter', type: 'post', role: roles.requireUser(), route: routes.invite.inviteTwitterFriends },
  { url: '/user/invite/linkedin', type: 'get', role: roles.requireUser(), route: routes.invite.getLinkedInConnections },
  { url: '/user/invite/linkedin', type: 'post', role: roles.requireUser(), route: routes.invite.inviteLinkedInConnections },
  { url: '/user/invite/existingusersbyemail', type: 'post', role: roles.requireUser(), route: routes.invite.getExistingUsersByEmail },
  { url: '/user/invite/existingusersbyfacebookid', type: 'post', role: roles.requireUser(), route: routes.invite.getExistingUsersByFacebookId },

  // Notification routes
  { url: '/user/notifications', type: 'get', role: roles.requireUser(), route: routes.notifications.getNotifications },
  { url: '/user/notifications/clear', type: 'post', role: roles.requireUser(), route: routes.notifications.clearNotifications },

  // Organization Routes
  { url: '/organization/list', type: 'get',  role: roles.requireSuperAdmin(), route: routes.organization.listOrganizations },
  { url: '/organization/list/:university', type: 'get', role: roles.requireNone(), route: routes.organization.listOrganization },
  { url: '/organization/create', type: 'get',  role: roles.requireSuperAdmin(), route: routes.organization.viewCreateOrganization },
  { url: '/organization/create', type: 'post', role: roles.requireSuperAdmin(), route: routes.organization.createOrganization },
  { url: '/organization/register', type: 'get',  role: roles.requireNone(), route: routes.organization.renderRegisterOrganization },
  { url: '/organization/register', type: 'post',  role: roles.requireNone(), route: routes.organization.registerOrganization },
  { url: '/organization/update/:id', type: 'get',  role: roles.requireSuperAdmin(), route: routes.organization.viewUpdateOrganization },
  { url: '/organization/update', type: 'post', role: roles.requireAdmin(), route: routes.organization.updateOrganization },
  { url: '/autocomplete/organization', type: 'get', role: roles.requireNone(), route: routes.autocomplete.organization },
  { url: '/organization/view', type: 'get',  role: roles.requireAdmin(), route: routes.organization.viewOrganization },
  { url: '/organization/users', type: 'get',  role: roles.requireAdmin(), route: routes.organization.viewUsersOrganization },
  { url: '/organization/addadmin', type: 'get',  role: roles.requireAdmin(), route: routes.organization.viewAddAdmins },
  { url: '/organization/addadmin', type: 'post',  role: roles.requireAdmin(), route: routes.organization.addAdmins },

  // University Routes
  { url: '/autocomplete/university', type: 'get', role: roles.requireNone(), route: routes.autocomplete.university },


  // Category Routes
  { url: '/category', type: 'get', role: roles.requireSuperAdmin(), route: routes.category.renderCategoriesPage },
  { url: '/category/list', type: 'get', role: roles.requireNone(), route: routes.category.getCategories },
  // TODO { url: '/category/info/:name', type: 'get', role: roles.requireSuperAdmin(), route: routes.category.get },
  // TODO { url: '/category/info/:name', type: 'delete', role: roles.requireSuperAdmin(), route: routes.category.delete },
  // TODO { url: '/category/info/:name', type: 'put', role: roles.requireSuperAdmin(), route: routes.category.put },
  { url: '/category/organizations/:name', type: 'put', role: roles.requireSuperAdmin(), route: routes.category.getOrganizations },

  // Standings Routes
  { url: '/standings/list', type: 'get', role: roles.requireNone(), route: routes.standings.renderMainStandingsPage },
  { url: '/standings/college/category/:category', type: 'get', role: roles.requireNone(), route: routes.standings.getUniversityStandingsForCategory },
  { url: '/standings/college/category/:category/:page', type: 'get', role: roles.requireNone(), route: routes.standings.getUniversityStandingsForCategory },
  { url: '/standings/college', type: 'get', role: roles.requireNone(), route: routes.standings.getUniversityStandingsForCategory },
  { url: '/standings/college/:page', type: 'get', role: roles.requireNone(), route: routes.standings.getUniversityStandingsForCategory },
  { url: '/standings/user/:id/college', type: 'get', role: roles.requireUser(), route: routes.standings.getUniversityStandingsForUser },

  // TODO
  //{ url: '/standings/user/:id/university', type: 'get', role: roles.requireUser(), route: routes.standings.getOrganizationStandingsForUser },

  // Campaign Routes
  // Widget
  { url: '/campaign/:slug/widget', type: 'get', role: roles.requireUser(), route: routes.campaigns.renderWidget },

  // Donate Campaign
  { url: '/campaign/donate', type: 'get', role: roles.requireUser(), route: routes.campaigns.donate.renderDonatePage },
  { url: '/campaign/donate/charge', type: 'post', role: roles.requireUser(), route: routes.campaigns.donate.chargeDonation },
  { url: '/campaign/donate/thankyou', type: 'get', role: roles.requireUser(), route: routes.campaigns.donate.renderThankYouPage },

  // Pledge Campaign
  { url: '/campaign/pledge', type: 'get', role: roles.requireUser(), route: routes.campaigns.pledge.renderPledgePage },
  { url: '/campaign/pledge/charge', type: 'post', role: roles.requireUser(), route: routes.campaigns.pledge.charge },
  { url: '/campaign/pledge/thankyou', type: 'get', role: roles.requireUser(), route: routes.campaigns.pledge.renderThankYouPage },

  // Complete Profile Campaign
  { url: '/campaign/complete_profile', type: 'get', role: roles.requireUser(), route: routes.campaigns.renderCompleteProfile },

  // Public Static Routes
  { url: '/howitworks', type: 'get',  role: roles.requireNone(), route: routes.howitworks },
  { url: '/colleges', type: 'get',  role: roles.requireNone(), route: routes.colleges },
  { url: '/faq', type: 'get',  role: roles.requireNone(), route: routes.faq },
  { url: '/aboutus', type: 'get',  role: roles.requireNone(), route: routes.aboutus },
  { url: '/privacy', type: 'get',  role: roles.requireNone(), route: routes.privacy },
  { url: '/termsandconditions', type: 'get',  role: roles.requireNone(), route: routes.termsandconditions },
  { url: '/test', type: 'get', role: roles.requireNone(), route: routes.test },
  { url: '/campaigns', type: 'get', role: roles.requireNone(), route: routes.campaign},
  { url: '/standing', type:'get', role:roles.requireNone(), route: routes.standing},
  { url: '/start_fundraiser', type:'get', role:roles.requireNone(), route: routes.startfundraiser},
  { url: '/start_fundraiser2', type:'get', role:roles.requireNone(), route: routes.startfundraiser2},
  { url: '/start_fundraiser3', type:'get', role:roles.requireNone(), route: routes.startfundraiser3},
  { url: '/start_fundraiser4', type:'get', role:roles.requireNone(), route: routes.startfundraiser4},
  { url: '/start_fundraiser5', type:'get', role:roles.requireNone(), route: routes.startfundraiser5},
  { url: '/start_fundraiser6', type:'get', role:roles.requireNone(), route: routes.startfundraiser6}



  // Rankings Routes
  //{ url: '/standings', type: 'get', role: roles.requireNone(), route: routes.standings }, 
  //{ url: '/rankings/', type: 'get', role: roles.requireNone(), route: routes.standings },
  //{ url: '/rankings/:category', type: 'get', role: roles.requireNone(), route: routes.standings },
  //{ url: '/rankings/:', type: 'get', role: roles.requireNone(), route: routes.standings }

  // Image Upload Routes
  // Removing this for simplicity's sake - we will only be using gravatar for now
  // { url: '/upload/profile', type: 'post', role: roles.requireUser(), route: routes.image_upload.uploadProfileImage }
];

/*
  This function ties the routes to the appropriate URL patterns and HTTP type in express.
*/
module.exports = function(app) {
  myRoutes.forEach(function(route) {
    if (route && route.type && route.url && route.route && route.routeNext && route.role) {
      app[route.type](route.url, route.role, route.route, route.routeNext);
    } else if (route && route.type && route.url && route.route && route.role) {
      logger.info("Registered route: " + JSON.stringify(route));
      app[route.type](route.url, route.role, route.route);
    } else {
      logger.info("Could not register route: " + JSON.stringify(route));
    }
  });
};
