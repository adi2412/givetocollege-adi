var configuration = require('../../configuration'),
    Invite = require('../../lib/models/invite'),
    User = require('../../lib/models/user'),
    UserLogin = require('../../lib/models/userLogin'),
    twitter = require('ntwitter'),
    logger = require('log4js').getLogger('invite'),
    async = require('async'),
    email = require('../../email');

exports.renderUserInvitation = function(request, response) {
  var userId = request.user._id;
  var inviteLink = configuration.get('app:protocol') + '://' + configuration.get('app:hostname') + '/user/register/' + userId;
  var fbapiid = configuration.get('facebook:clientID');
  response.render('user/invite', { title: 'Invite', inviteLink: inviteLink, fbapiid: fbapiid });
};

function inviteByEmails(user, emailArray, source, callback) {
  var userId = user._id;
  var userEmail = user.email;
  var inviteLink = configuration.get('app:protocol') + '://' + configuration.get('app:hostname') + '/user/register/' + userId;
  var insertInviteIfNoneFound = function(userId, inviteEmail, inviteLink) {
    return function(error, count) {
      if ( count === 0 ) {
        var name;
        if(inviteEmail.name != inviteEmail.id) {
          name = inviteEmail.name;
        }
        var newInvite = new Invite({ user: userId, recipientId: inviteEmail.id, recipientName: inviteEmail.name, source: source });
        newInvite.save(function(err) {
          if ( err ) {
            logger.error('Error when saving invite: ' + err);
          } else {
            email.sendEmailTemplate("support@givetocollege.com", inviteEmail, "You've been invited!", { invite_link: inviteLink }, 'invite_user');
          }
        });
      }
    };
  };
  for(var i in emailArray) {
    var inviteEmail = emailArray[i];
    if ( inviteEmail === userEmail ) {
      continue;
    }

    Invite.count({ 'recipientId': inviteEmail.id, 'user': userId }, insertInviteIfNoneFound(userId, inviteEmail, inviteLink));
  }
  if(callback !== undefined) {
    callback(null);
  }
}

exports.sendUserEmailInvitation = function(request, response) {
  var emails = request.body.emails;
  var user = request.user;
  async.series([
    function(callback) {
      if(emails !== undefined) {
        var contacts = [];
        var emailArray = emails.split(",");
        for(var i in emailArray) {
          contacts.push({ id: emailArray[i] });
        }
        inviteByEmails(user, contacts, 'email', callback);
      }
    }
  ],
  function(err, results) {
    listInvitations(request, response);
  });
};

exports.getFacebookFriends = function(request, response) {
  var fbapiid = configuration.get('facebook:clientID');
  var userId = request.user._id;
  var inviteLink = configuration.get('app:protocol') + '://' + configuration.get('app:hostname') + '/user/register/' + userId;
  response.render('invites/facebook_invites',{ title: 'Facebook Friends', fbapiid: fbapiid, userId: userId, inviteLink: inviteLink });
};

exports.inviteFacebookFriends = function(request, response) {
  var contacts = JSON.parse(request.body.contacts);
  var user = request.user;
  var facebookContacts = [];
  for(var i in contacts) {
    var contact = {};
    var contactid = contacts[i].id;
    var contactids = contactid.split("|");
    var email = contactids[1] + "@facebook.com";
    contact.id = email;
    contact.name = contacts[i].name;
    facebookContacts.push(contact);
  }
  async.series([
    function(callback) {
      inviteByEmails(user, facebookContacts, 'facebook', callback);
    }
    ],
    function(err, results) {
      listInvitations(request, response);
    });
};

exports.getTwitterFriends = function(request, response) {
  var twit = new twitter({
    consumer_key: configuration.get('twitter:consumerKey'),
    consumer_secret: configuration.get('twitter:consumerSecret'),
    access_token_key: 'keys',
    access_token_secret: 'go here'
  });
  var userId = request.user._id;
  response.render('invites/twitter_invites',{ title: 'Twitter Friends', twitterapiid: twitterapiid, userId: userId });
};

exports.inviteTwitterFriends = function(request, response) {
  response.render('invites/invitelist', { title: 'Invitation sent!' });
};

exports.getGoogleContacts = function(request, response) {
  var googleapiid = configuration.get('google:consumerKey');
  var userId = request.user._id;
  response.render('invites/gmail_invites', { title: 'GMail Contacts', googleapiid: googleapiid, userId: userId });
};

exports.inviteGoogleContacts = function(request, response) {
  var contacts = JSON.parse(request.body.contacts);
  var user = request.user;
  async.series([
    function(callback) {
      if(contacts !== undefined) {
        inviteByEmails(user, contacts, 'gmail', callback);
      }
    }],
    function(err, results) {
      listInvitations(request, response);
    }
  );
};

exports.getLinkedInConnections = function(request, response) {
  var linkedinapiid = configuration.get('linkedin:apiKey');
  var userId = request.user._id;
  response.render('invites/linkedin_invites', { title: 'LinkedIn Connections', linkedinapiid: linkedinapiid, userId: userId });
};

exports.inviteLinkedInConnections = function(request, response) {
  response.render('invites/invitelist', { title: 'Invitation sent!' });
};

exports.getExistingUsersByEmail = function(request, response) {
  var emails = request.body.emails;
  var query = User.find({});
  var orQuery = [];
  for(var i in emails) {
    orQuery.push({ email: emails[i].id });
  }
  query.or(orQuery);
  query.select('_id first last email');
  query.exec(function(err, data) {
    var users = [];
    if(err) {
      logger.error(err);
    } else {
      users = data;
    }
    response.json(users);
  });
};

exports.getExistingUsersByFacebookId = function(request, response) {
  var facebookids = request.body.facebookids;
  var query = UserLogin.find({});
  var orQuery = [];
  for(var i in facebookids) {
    orQuery.push({ userKey: "facebook|" + facebookids[i].id });
  }
  query.or(orQuery);
  query.exec(function(err,data) {
    var users = [];
    if(err) {
      logger.error(err);
    } else {
      for(var i in data) {
        users.push(data[i].userRef);
      }
    }

    response.json(users);
  });
};

var queryInvites =
  function(status, userId) {
    return function(callback) {
      Invite.find({ user: userId, status: status }, function(err, invites) {
        if(err) {
          logger.error(err);
          callback(err, undefined);
        } else {
          callback(null, invites);
        }
      });
    };
  };

var listInvitations = function(request, response) {
  var userId = request.user._id;
  async.parallel([
    queryInvites('sent', userId),
    queryInvites('accepted', userId)
  ],function(err, results) {
    if(err) {
      logger.error(err);
      response.render('500', { status:500, error: err});
    } else {
      var sentInvites = results[0];
      var sentInviteCount = sentInvites.length;
      var acceptedInvites = results[1];
      var acceptedInvitesCount = acceptedInvites.length;
      response.render('invites/invitelist', { title: 'Invitation Dashboard', sentInvites: sentInvites, acceptedInvites: acceptedInvites });
    }
  });
};

exports.listInvitations = listInvitations;
