// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/**
 * A User is defined simply as a user of the GiveToCollege application
 * Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
 **/

var events = require('../../../lib/achievements').events,
    eventQueue = require('../../../event_queue');

exports.renderPledgePage = function(request, response) {
  return response.render('campaigns/pledge.jade', { title: 'Pledge' });
};

exports.renderThankYouPage = function(request, response) {
  return response.render('campaigns/pledge_thankyou.jade', { title: 'Thank You!', amount: request.query.amount, points: request.query.points });
};

exports.charge = function(request, response) {
  // TODO Make sure this is an integer
  var amount = request.body.amount;
  var months = request.body.months;
  eventQueue.publish(events.PLEDGE, request.user._id, { organization: request.body.organization, amount: amount, months: months });
  return response.redirect('/campaign/pledge/thankyou?amount=' + amount + '&points=' + amount*5 + '&months=' + months);
}
