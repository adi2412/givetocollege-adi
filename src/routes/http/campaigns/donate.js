// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/**
 * A User is defined simply as a user of the GiveToCollege application
 * Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
 **/

var async = require('async'),
  configuration = require('../../../configuration'),
  stripe = require('stripe'),
  nconf = require('nconf'),
  sanitize = require('validator').sanitize;

Validator = require('validator').Validator;
Validator.prototype.error = function (type, msg) {
  this._errors.push({ type: type, msg: msg });
  return this;
};

Validator.prototype.getErrors = function () {
  return this._errors;
};

// Local imports
var events = require('../../../lib/achievements').events,
    eventQueue = require('../../../event_queue');

// Schema imports
var Organization = require('../../../lib/models/organization');

// Error types
var errorTypes = {
  CHARGE_TYPE: 'chargeType',
  AMOUNT: 'amount',
  ORGANIZATION: 'organization',
  STRIPE: 'stripe',
  OTHER: 'other'
};


// Initialize stripe object
stripe = stripe(configuration.get('stripe:apiKey'));

exports.renderDonatePage = function(request, response) {
  return response.render('campaigns/donate.jade', { title: 'Donate', publicKey: configuration.get("stripe:publicKey") });
};

exports.renderThankYouPage = function(request, response) {
  return response.render('campaigns/donate_thankyou.jade', { title: 'Thank You!', amount: request.query.amount, points: request.query.points });
};

exports.chargeDonation = function(request, response) {
  var validator = new Validator();

  // Check that organization name is not empty
  validator.check(request.body.organization, errorTypes.ORGANIZATION, nconf.get("err_donate:invalidOrganization"));

  // Check that amount is not empty and not negative
  validator.check(request.body.amount, errorTypes.AMOUNT, nconf.get("err_donate:amountNAN")).isInt();
  validator.check(request.body.amount, errorTypes.AMOUNT, nconf.get("err_donate:invalidAmount")).min(0);

  // Check that the stripe token is attached
  validator.check(request.body.stripeToken, errorTypes.STRIPE, nconf.get("err_donate:stripeMissing")).notNull();

  // TODO Check page expiry

  // If errors exist, then return errors right now.
  var errors = validator.getErrors();
  if (errors.length) {
    response.send({ errors: errors });
  }

  var amount = sanitize(request.body.amount).toInt();
  var organization = sanitize(request.body.organization).trim().toLowerCase();
  var stripeToken = request.body.stripeToken;

  async.waterfall([
    function(callback) {
      // Ensure that organization name exists
      return Organization.find({ name: organization }, function(err, organization) {
        if (!organization) {
          validator.error(errorType.ORGANIZATION, nconf.get("err_donate:orgNotFound"));
          return callback(nconf.get("err_donate:orgNotFound"));
        } else {
          return callback(err, organization);
        }
      });
    },
    function(organization, callback) {
      // XXX When the backend system is in place, we will probably want to move this to the
      // backend system to take care of, at which point, this step disappears.
      return stripe.charges.create({
        amount: amount * 100,
        currency: 'usd',
        description: organization._id,
        card: stripeToken
      }, callback);
    }
  ], function(err) {
    if (err) {
      logger.error('An error occurred during payment processing: ' + JSON.stringify(err));
      validator.error(errorType.OTHER, nconf.get("err_donate:payment"));
      eventQueue.publish(events.FAILED_DONATION, request.user._id, { organization: request.body.organization, amount: amount });
      return response.send({ errors: errors });
    } else {
      eventQueue.publish(events.DONATION, request.user._id, { organization: request.body.organization, amount: amount });
      return response.redirect('/campaign/donate/thankyou?amount=' + amount + '&points=' + amount*5);
    }
  });
}
