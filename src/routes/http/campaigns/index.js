// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Contains all campaign routes.
  Index or root routes will be declared here.  All other subroutes (e.g. autocomplete, login, etc)
  will imported into the appropriate vars.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var logger = require('log4js').getLogger('campaigns'),
    sanitize = require('validator').sanitize;

var Campaign = require('../../../lib/models/campaign');

exports.donate = require('./donate');
exports.pledge = require('./pledge');

exports.renderWidget = function(request, result) {
  var slug = request.params.slug || null;
  slug = sanitize(slug).xss();

  Campaign.findOne({ slug: slug }, function(err, campaign) {
    if (err) {
      logger.error('An error occurred while looking up campaign slug ' + slug + ': ' + err);
      return result.send(500);
    } else if (!campaign) {
      return result.send(404);
    } else {
      return result.render('campaigns/' + slug + '/widget', { campaign: campaign, user: request.user});
    }
  });
}

exports.renderCompleteProfile = function(request, result) {
  result.render('campaigns/complete_profile', { title: 'Complete Your Profile', userId: request.user._id });
};
