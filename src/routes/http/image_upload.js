var async = require('async'),
    configuration = require('../../configuration'),
    fs = require('fs'),
    InputErrors = require('../../lib/util/InputError'),
    logger = require('log4js').getLogger('image_upload'),
    nconf = require('nconf'),
    uuid = require('node-uuid');

var User = require('../../lib/models/user');

exports.uploadProfileImage = function(request, response) {
  var errors = new InputErrors(['profileImage']);
  var user = request.user;

  // If the user isn't logged in, return forbidden response
  if (user === undefined || !user) {
    logger.warn('Attempting to upload a profile image with a null or undefined user! ' + JSON.stringify(request));
    response.send(403);
  }

  if (request.files.profileImage === undefined) {
    logger.warn('Attempting to upload an empty image! ' + JSON.stringify(request));
    errors.add('profileImage', nconf.get('err_image_upload:invalidImage'));
    response.send(500);
  }

  // Create filename
  var profileImageName = uuid.v1() + '.' + request.files.profileImage.type.split('/')[1];

  // TODO Limit the size of the profileImages array
  fs.readFile(request.files.profileImage.path, function (err, data) {
    var fsPath = configuration.get("profile:upload") + "/" + profileImageName;
    var urlPath = profileImageName;
    async.series([
      function(callback) { fs.writeFile(fsPath, data, callback); },
      function(callback) {
        User.update(
          { _id: user._id },
          { $push: { profileImages: urlPath } },
          callback);
      }
    ], function(err, result) {
      if (err) {
        logger.error('An error occurred while trying to upload image: ' + err);
        errors.add('profileImage', nconf.get('err_image_upload:other'));
        return response.send(500);
      } else {
        logger.debug('Image upload successful!' + fsPath);
        request.flash('info', nconf.get('err_image_upload:success'));
        response.redirect('/user/profile');
      }
    });
  });
};
