// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Contains all standings-related routes.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var async = require('async');
var logger = require('log4js').getLogger('routes/standings');

var Organization = require('../../lib/models/organization');
var University = require('../../lib/models/university');
var User = require('../../lib/models/user');

// Arbitrarily chosen
var MAX_ENTRIES_PER_PAGE = 20;

exports.renderMainStandingsPage = function(request, response) {
  University.count({}, function(err, count) {
    if (err) {
      logger.error('An error occurred while retrieving the number of universities: ' + err);
    }
    return response.render('standings', { title: 'Give To College', maxPages: Math.ceil(count / MAX_ENTRIES_PER_PAGE) });
  });
};

exports.getUniversityStandingsForCategory = function(request, response) {
  var page = (request.params.page || 1) - 1;
  page = page >= 0 ? page : 0;

  var query = {
    // XXX Temporarily removed status enabled filter status: 'enabled'
  };
  if (request.params.category) {
    query.category = request.params.category;
  }
  University.find(query)
  .skip(page*MAX_ENTRIES_PER_PAGE)
  .limit(MAX_ENTRIES_PER_PAGE)
  .sort({ points: 'desc' })
  .exec(function(err, universities) {
    if (err) {
      logger.error('An error occurred while retrieving unviersity standings for category: ' + err);
    } else if (!universities) {
      response.send([]);
    } else {
      var startingRank = page*MAX_ENTRIES_PER_PAGE + 1;
      universities = universities.map(function(x, i) {
        var university = x.toObject();
        university.rank = startingRank + i;
        return university;
      });
      response.send(universities);
    }
  });
};

exports.getUniversityStandingsForUser = function(request, response) {
  var userId = request.params.id || '';
  if (!userId) {
    response.send([]);
  }

  async.waterfall([
    function(callback) {
      User.findById(userId)
      .populate('user_roles')
      .populate('admin_roles')
      .exec(callback);
    },
    function(user, callback) {
      var universities = []
      if (user) {
        universities = 
          universities.concat(user.user_roles, user.admin_roles)
          .filter(function(element) {
            return element.status != 'disabled';
          });
        universities.sort(function(a,b) { return b - a; });
      }
      return callback(null, universities);
    }
  ], function(err, universities) {
      if (err) {
        logger.error('An error occurred while retrieving univerisity standings for user: ' + err);
        response.send(500);
      }

      return response.send(universities || []);
  });
};
