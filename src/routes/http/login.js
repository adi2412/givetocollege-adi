// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Contains all login-related routes.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var InputError = require('../../lib/util/InputError');
var nconf = require('nconf');

var bcrypt = require('bcrypt'),
    crypto = require('crypto'),
    configuration = require('../../configuration'),
    emailService = require('../../email'),
    log4js = require('log4js'),
    passport = require('passport'),
    password_util = require('../../common/utils/password_util'),
    validator = require ('../../lib/models/validator');

// Logger
var logger = log4js.getLogger('login');

var User = require('../../lib/models/user');

/**
 * Returns the login key used to search for the user login information.
 **/
var getLoginKey = exports.getLoginKey = function(type, id) {
  return type + "|" + id;
};

exports.loginEmail= passport.authenticate('local', 
  {
    successRedirect: '/user/homepage',
    failureRedirect: '/login',
    failureFlash: true
  }
);

var MAX_TOKEN_TIME = 3*60*60*1000; // three hours

exports.login = function(request, response) {
  var error = request.flash('error').pop();
  var errors = new InputError();
  errors.add(error);
  response.render('login', { title: 'Login', errors: errors.getErrors() });
};

exports.loginFacebook = passport.authenticate('facebook',
  {
    scope: [ 'user_about_me', 'email' ],
    successRedirect: '/user/homepage',
    failureRedirect: '/user/login'
  }
);

exports.loginTwitter = passport.authenticate('twitter',
  {
    successRedirect: '/user/homepage',
    failureRedirect: '/user/login'
  }
);

exports.loginSuccess = function (request, response) {
  var email = request.user.email;
  User.update({ email: email }, { lastLoggedIn: Date.now() }, function(error) {
    if(error) {
      logger.warn("Error while updating the last logged in field: " + error);
    }
  });
  response.redirect('/user/homepage');
};

exports.logout = function(request, response) {
  request.logOut();
  response.redirect('/');
};

exports.viewForgotPassword = function(request, response) {
  response.render('forgot_password', { title: 'Forgot Password' });
};

exports.updateForgotPassword = function(request, response) {
  var email = request.body.email;
  if (validator.emailValidator(email)) {
    // Check that the supplied email is known.
    User.findOne({ email: email }, function(error, user) {
      if (!error && user) {
        if(!user.lastReset || ((Date.now() - user.lastReset) > MAX_TOKEN_TIME)) {
          // Generate a token unique to the user.
          var token = encodeURIComponent(getResetPasswordToken(user.email));
          // Send an email with the generated token
          emailService.sendEmailTemplate(configuration.get("support:email"),email,"Password reset information",
            { user: user, app: configuration.get("app"), token: token }, 'password_reset');
          // Update the time of the sent token
          User.update({ email: email }, { lastReset: Date.now() }, function(error) {
            if (error) {
              logger.warn("An error occurred while updating the lastReset field: " + error);
            }
          });
        }
      }
    });
  }

  // No matter what the result of the logic is, always redirect to the next step message.
  response.render('update_forgot_password', { title: 'Forgot Password' });
};

exports.viewResetPassword = function(request, response) {
  var token = request.params.token;
  response.render('reset_password', { title: 'Reset Password', token: token });
};

exports.updateResetPassword = function(request, response) {
  var token = request.body.token;
  var newPassword = request.body.password;
  var success = false;
  var message = nconf.get('err_login:resetError');
  var email;
  var timeDifference;
  var validationPassed = false;

  try {
    if(token && newPassword) {
      var decryptedToken = parseResetPasswordToken(token);
      email = decryptedToken.email;
      timeDifference = Date.now() - decryptedToken.date;
      validationPassed = (validator.emailValidator(email) && timeDifference<=MAX_TOKEN_TIME);
    }
  } catch(error) {
    // An error occured, send an error message back to the user.
    validationPassed = false;
  }

  if (validationPassed) {
    var newPasswordHash = password_util.encryptPassword(newPassword);
    User.update({ email: email }, { passwordHash: newPasswordHash }, function(error) {
      if(!error) {
        success = true;
        message = nconf.get('err_login:resetSuccess');
        response.render('login', { title: 'Login', result: { success: success, message: message } });
      } else {
        logger.warn("Error while accessing DB: " + error);
        response.render('update_reset_password', { title: 'Reset Password', result: { success: success, message: message }});
      }
    });
  } else {
    response.render('update_reset_password', { title: 'Reset Password', result: { success: success, message: message }});
  }
};

function getResetPasswordToken(email) {
  var salt = bcrypt.genSaltSync();
  var toEncrypt = salt + "|" + JSON.stringify({ email: email, date: Date.now() });
  var cipher = crypto.createCipher('aes-256-cbc',configuration.get("app:reset_password_key"));
  var token = cipher.update(toEncrypt,'utf8','base64');
  token += cipher.final('base64');
  return token;
}

function parseResetPasswordToken(token) {
  var decipher = crypto.createDecipher('aes-256-cbc',configuration.get("app:reset_password_key"));
  var decrypted = decipher.update(token,'base64','utf8');
  decrypted += decipher.final('base64');
  var dec = decrypted.split("|");
  return JSON.parse(dec[1]);
}
