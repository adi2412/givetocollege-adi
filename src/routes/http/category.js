// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  All the category-based routes
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var Category = require ('../../lib/models/category');
var Organization = require('../../lib/models/organization');
var async = require('async')
  , logger = require('log4js').getLogger('routes/category');

exports.getCategories = function(request, response) {
  Category.find()
  .select({ _id: 1, name: 1})
  .exec(function(err, categories) {
    if (err) {
      logger.error('An error occurred while getting categories: ' + err);
      return response.send(500);
    }
    return response.send(categories);
  });
};

exports.getUniversities = function(request, response) {
  var name = request.params.name || '';
  if (!name) {
    return response.send(400);
  }

  Organization.find({ name: name }, function(err, organizations) {
    if (err) {
      logger.error('An error occurred while getting organizations: ' + err);
      return response.send(500);
    }

    if (!organizations || organizations.length == 0) {
      return response.send(404);
    } else {
      return organizations;
    }
  });
};

exports.renderCategoriesPage = function(request, response) {
  Category.find({}, function(err, categories) {
    if (err) {
      logger.error('An error occurred while rendering categories page: ' + err);
      return response.send(500);
    }
    return response.render('categories', { categories: categories, title: 'Categories' });
  });
}
