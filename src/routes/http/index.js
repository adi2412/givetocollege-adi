// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Contains all routes.
  Index or root routes will be declared here.  All other subroutes (e.g. autocomplete, login, etc)
  will imported into the appropriate vars.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

exports.login = require('./login');
exports.autocomplete = require('./autocomplete');
exports.category = require('./category');
exports.campaigns = require('./campaigns');
exports.user = require('./user'); 
exports.invite = require('./invite'); 
exports.organization = require('./organization'); 
exports.facebook = require('./facebook');
exports.twitter = require('./twitter');
exports.standings = require('./standings');
exports.notifications = require('./notifications');

exports.image_upload = require('./image_upload');

exports.index = function(req, res) {
  res.render('index', { title: 'Give To College' });
};

exports.howitworks = function(req, res) {
  res.render('howitworks', { title: 'How It Works: Give To College' });
};

exports.colleges = function(req, res) {
  res.render('colleges', { title: 'Colleges: Give To College' });
};

exports.faq = function(req, res) {
  res.render('faq', { title: 'Frequently Asked Questions: Give To College' });
};

exports.aboutus = function(req, res) {
  res.render('aboutus', { title: 'About Us: Give To College' });
};

exports.privacy = function(req, res) {
  res.render('privacy', { title: 'Privacy Policy: Give To College' });
};

exports.termsandconditions = function(req, res) {
  res.render('termsandconditions', { title: 'Terms and Conditions: Give To College' });
};

exports.test = function(req,res){
  res.render('test', {title:'Test: GTC'});
};

exports.campaign = function(req,res){
  res.render('campaign', {title:'Test: Campaigns'});
};

exports.standing = function(req,res){
  res.render('standing', {title:'Test: Standings'});
};

exports.startfundraiser = function(req,res){
  res.render('start_fundraiser', {title:'Test: Start Fundraiser'});
};

exports.startfundraiser2 = function(req,res){
  res.render('start_fundraiser2', {title:'Test: Start Fundraiser'});
};

exports.startfundraiser3 = function(req,res){
  res.render('start_fundraiser3', {title: 'Test: Start a Fundraiser'});
};

exports.startfundraiser4 = function(req,res){
  res.render('start_fundraiser4', {title: 'Test: Start a Fundraiser'});
};

exports.startfundraiser5 = function(req,res){
  res.render('start_fundraiser5', {title: 'Test: Start a Fundraiser'});
};

exports.startfundraiser6 = function(req,res){
  res.render('start_fundraiser6', {title: 'Test: Start a Fundraiser'});
};