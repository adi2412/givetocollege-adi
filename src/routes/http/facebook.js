// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Facebook-related routes
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var userRoute = require('./user'),
    passport = require('passport'),
    log4js = require('log4js'),
    nconf = require('nconf'),
    login = require('./login'),
    User = require('../../lib/models/user'),
    UserLogin = require('../../lib/models/userLogin'),
    InputErrors = require('../../lib/util/InputError');

var logger = log4js.getLogger('facebook');

var FACEBOOK_MESSAGES = exports.facebookMessages = {
  serverError: "An error occurred while authenticating your Facebook account.  Please try again later.",
  insufficientInfo: "We could not get sufficient information from your Facebook account to create your profile.  Please use another registration method."
};

exports.initiateFacebookRegistration = passport.authenticate('facebook', 
  { 
    state: JSON.stringify({ action: 'register' }),
    scope: [ 'user_about_me', 'email' ],
    display: 'popup',
    failureRedirect: '/user/register/',
    failureFlash: nconf.get('err_facebook:serverError')
  }
);

exports.getOrCreateUser = function(profile, done) {
  var errors = new InputErrors();

  // Do some validation here:
  // If the profile doesn't exist, then we have a problem.
  if (!profile) {
    logger.trace('Attempting to get or create user with empty profile');
    errors.add(nconf.get('err_facebook:serverError'));
  } else if (!profile.id || profile.id === '') {
    logger.warn('Facebook profile came back with no id');
    errors.add(nconf.get('err_facebook:insufficientInfo'));
  }

  if (!errors.isEmpty()) {
    return done(null, false, errors.getErrors());
  }

  var userKey = login.getLoginKey('facebook', profile.id);
  logger.debug('Looking for user login: ' + userKey);
  UserLogin.findOne({ userKey: userKey }, function(err, userLogin) {
    if (err) {
      logger.error('An error occurred while looking for facebook user by key ' + userKey + ': ' + err);
      errors.add(nconf.get('err_facebook:serverError'));
      return done(err, false, errors.getErrors());
    }

    if (userLogin) {
      User.findById(userLogin.userRef, function(err, user) {
        if (err) {
          logger.error('An error accurred while looking facebook user ' + userKey + ': ' + err);
          return done(err, false, errors.getErrors());
        }

        if (!user) {
          errors.add(nconf.get('err_facebook:serverError'));
        } else {
          user.loginType = 'facebook';
          user.userKey = userLogin.userKey;
        }
        return done(null, user, errors.getErrors());
      });
    } else {
      var input = {
        request: { body: { } },
        id: profile.id,
        fullName: profile.name.givenName + ' ' + profile.name.familyName,
        email: profile.emails[0].value || '', // For now, use the first email address
        register_type: 'facebook'
      };
      logger.debug('Attempting to create user from facebook account: ' + JSON.stringify(input));
      userRoute.createUser(input, function(err, userData) {
        if (err) {
          logger.error('An error occured when creating the user ' + err);
          return done(err);
        } else {
          var user = userData.user;
          if (user) {
            user.loginType = 'facebook';
            user.userKey = userData.userLogin.userKey;
          }
          return done(null, user);
        }
      });
    }
  });
};

exports.handleCallback = passport.authenticate('facebook',
{
  successRedirect: '/oauth/close.html',
  failureRedirect: '/login',
  failureFlash: nconf.get('err_facebook:serverError')
});
