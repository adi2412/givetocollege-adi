// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  A Organization is defined as an organization that users can donate to.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var Organization = require('../../lib/models/organization'),
    User = require('../../lib/models/user'),
    Validator = require('../../lib/models/validator'),
    emailService = require('../../email'),
    nconf = require('nconf'),
    PagingManager = require('../../lib/logic/pagingManager');
var configuration = require('../../configuration'),
    logger = require('log4js').getLogger('routes/organization');

exports.viewCreateOrganization = function(request, response) {
  var organization = new Organization();
  response.render('organization', { title: 'Organization Page', type: "create", organization: organization, result: false });
};

exports.createOrganization = function(request, response) {
  var display_name = request.body.display_name || "";
  var acronym = request.body.acronym || undefined;
  var domain = request.body.domain || undefined;
  var address1 = request.body.address1 || undefined;
  var address2 = request.body.address2 || undefined;
  var city = request.body.city || undefined;
  var state = request.body.state || undefined;
  var zip = request.body.zip || undefined;
  var taxid = request.body.taxid || undefined;
  var twitterhandle = request.body.twitterhandle || undefined;
  var facebookpage = request.body.facebookpage || undefined;
  var webpage = request.body.webpage || undefined;
  var isEnabled = request.body.isEnabled==='true'? true: false;
  var isPaymentEnabled = request.body.isPaymentEnabled==='true'? true: false;

  var organization = new Organization();
  organization.display_name = display_name;
  organization.acronym = acronym;
  organization.domain = domain;
  organization.address1 = address1;
  organization.address2 = address2;
  organization.city = city;
  organization.state = state;
  organization.zip = zip;
  organization.taxid = taxid;
  organization.twitterhandle = twitterhandle;
  organization.facebookpage = facebookpage;
  organization.webpage = webpage;
  organization.isEnabled = isEnabled;
  organization.isPaymentEnabled = isPaymentEnabled;

  logger.debug(JSON.stringify(organization,null,4));

  organization.save(function(error) {
    console.log(error);
    if(error) {
      success = false;
      message = nconf.get('err_organization:errors:saveError');
    } else {
      success = true;
      message = nconf.get('err_organization:errors:saveSuccess');
    }
    console.log("success " + success + " - " + message);
    // Create an empty organization for the view.
    organization = new Organization();
    response.render('organization', { title: 'Organization Page', type: "create", organization: organization, result: { success: success, message: message }});
  });
};

exports.listOrganizations = PagingManager.createPagingManager(Organization,'name','organizationlist','Organization List');

exports.viewUpdateOrganization = function(request, response) {
  var user = request.user;
  if (user.roleLevel != 3) {
    response.render('401');
  } else {
    var id = request.params.id;
    Organization.findOne({ _id: id }, function(error, organization) {
      response.render('organization', { title: 'Edit Organization', type: "update", organization: organization, result: false });
    });
  }
};

exports.viewOrganization = function(request, response) {
  var user = request.user;
  if (user.roleLevel != 2) {
    response.render('401');
  } else {
    var organization = user.admin_roles[0];
  }
};

exports.updateOrganization = function(request, response) {
  var id = request.body.id || "";
  var name = request.body.display_name || "";
  var acronym = request.body.acronym || "";
  var domain = request.body.domain || "";
  var address1 = request.body.address1 || "";
  var address2 = request.body.address2 || "";
  var city = request.body.city || "";
  var state = request.body.state || "";
  var zip = request.body.zip || "";
  var twitterhandle = request.body.twitterhandle || "";
  var facebookpage = request.body.facebookpage || "";
  var webpage = request.body.webpage || "";
  var isEnabled = request.body.isEnabled==='true'? true: false;
  var isPaymentEnabled = request.body.isPaymentEnabled==='true'? true: false;

  var updateList = { display_name: name, name: name, acronym: acronym, domain: domain, address1: address1, address2: address2, city: city, state: state, zip: zip, twitterhandle: twitterhandle, facebookpage: facebookpage, webpage: webpage, isEnabled:isEnabled, isPaymentEnabled: isPaymentEnabled };

  Organization.update({ _id: id }, updateList, { multi: false }, function(error, numAffected) {
    var success = false;
    if (!error) {
      success = true;
    }
    Organization.findOne({ _id: id }, function(error, organization) {
      if(error || !success) {
        success = false;
        message = nconf.get('err_organization:errors:updateError');
      } else {
        success = true;
        message = nconf.get('err_organization:errors:updateSuccess');
      }
      response.render('organization', { title: 'Edit Organization', organization: organization, type: "update", result: { success: success, message: message } });
    });
  });
};

exports.renderRegisterOrganization = function(request, response) {
  var errors = request.flash('error');
  response.render('register_organization', { title: 'Register Organization', errors: errors });
};

exports.registerOrganization = function(request, response) {
  // Validate input
  var Validator = require('validator').Validator;
  var validator = new Validator();
  validator.error = function (msg) {
    this._errors.push(msg);
    return this;
  };
  validator.getErrors = function () {
    return this._errors;
  };

  validator.check(request.body.organization, nconf.get('err_organization:validation:emptyOrg')).notEmpty();
  validator.check(request.body.email, nconf.get('err_organization:validation:invalidEmail')).isEmail();
  validator.check(request.body.fullname, nconf.get('err_organization:validation:emptyName')).notEmpty();
  validator.check(request.body.title, nconf.get('err_organization:validation:emptyTitle')).notEmpty();

  var errors = validator.getErrors();
  if (errors.length) {
    errors.forEach(function(error) {
      request.flash('error', error);
    });
    return response.redirect('/organization/register');
  }

  var organization = {};
  organization.organization = validator.sanitize(request.body.name).xss();
  organization.fullName = validator.sanitize(request.body.fullname).xss();
  organization.title = validator.sanitize(request.body.title).xss();
  organization.email = validator.sanitize(request.body.email).xss();
  organization.phone = validator.sanitize(request.body.phone).xss();

  // Send two emails:
  // 1. To the user who registered.
  // 2. To the super admin.
  emailService.sendEmailTemplate(configuration.get("support:email"),configuration.get("support:email"),"Organization registration",
       { organization: organization }, 'organization_register');

  // Finally, redirect to the thank you page.
  response.render('register_organization_end', { title: 'Thank You'});
};

exports.viewAddAdmins = function(request, response) {
  var user = new User();
  response.render('organization/add_admin', { title: 'Add Administrator', type: 'create', result: undefined, admin: {} });
};

exports.addAdmins = function(request, response) {
  var name = request.body.name;
  response.render('organization/add_admin', { title: 'Add Administrator' });
};
