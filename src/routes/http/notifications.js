// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var async = require('async')
var Notification = require('../../lib/models/notification');
var logger = require('log4js').getLogger('notifications');

var MAX_SEEN_NOTIFICATIONS = 5;

exports.getNotifications = function(request, response) {
  if (!request.user || !request.user._id) {
    return response.send([]);
  }

  async.parallel({
    unseen: function(callback) {
      Notification.find({ userRef: request.user._id, seen: false })
      .exec(callback);
    },
    seen: function(callback) {
      Notification.find({ userRef: request.user._id, seen: true })
      .sort({ created: 'desc'})
      .limit(MAX_SEEN_NOTIFICATIONS)
      .exec(callback);
    },
  }, function(err, result) {
    if (err) {
      logger.error('An error occurred while retrieving notifications: ' + err);
      return response.send(500);
    } else if (!result) {
      response.send({});
    } else {
      response.send(result);
    }
  });
}

exports.clearNotifications = function(request, response) {
  // TODO instanceof array check?
  if (request.body.entries) {
    Notification.update({ _id: { '$in': request.body.entries } }, { '$set': { seen: true } }, { multi: true }, function(err) {
      if (err) {
        logger.error('An error occurred while clearing notifications: ' + err);
      }

      response.send(200);
    });
  } else {
    response.send(200);
  }
}
