// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Twitter-related routes
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var userRoute = require('./user'),
    passport = require('passport'),
    log4js = require('log4js'),
    nconf = require('nconf'),
    login = require('./login'),
    User = require('../../lib/models/user'),
    UserLogin = require('../../lib/models/userLogin'),
    InputErrors = require('../../lib/util/InputError');

var logger = log4js.getLogger('twitter');

// TODO encrypt using some kind of key/nonce system
exports.initiateTwitterRegistration = passport.authenticate('twitter',
  {
    state: JSON.stringify({ action: 'register' }),
    failureRedirect: '/user/register/',
    failureFlash: nconf.get('err_twitter:serverError')
  }
);

exports.getOrCreateUser = function(profile, done) {
  var errors = new InputErrors();

  // Do some validation here:
  // If the profile doesn't exist, then we have a problem.
  if (!profile) {
    logger.trace('Attempting to get or create user with empty profile');
    errors.add(TWITTER_MESSAGES.serverError);
  } else if (!profile.id || profile.id === '') {
    logger.warn('Twitter profile came back with no id');
    errors.add(nconf.get('err_twitter:insufficientInfo'));
  }

  if (!errors.isEmpty()) {
    return done(null, false, errors.getErrors());
  }

  var userKey = login.getLoginKey('twitter', profile.id);
  logger.debug('Looking for user login: ' + userKey);
  UserLogin.findOne({ userKey: userKey }, function(err, userLogin) {
    if (err) {
      logger.error('An error occurred while looking for twitter user by key ' + userKey + ': ' + err);
      errors.add(nconf.get('err_twitter:serverError'));
      return done(err, false, errors.getErrors());
    }

    if (userLogin) {
      User.findById(userLogin.userRef, function(err, user) {
        if (err) {
          logger.error('An error accurred while looking twitter user ' + userKey + ': ' + err);
          return done(err, false, errors.getErrors());
        }

        if (!user) {
          errors.add(nconf.get('err_twitter:serverError'));
        } else {
          user.loginType = 'twitter';
          user.userKey = userLogin.userKey;
        }
        return done(null, user, errors.getErrors());
      });
    } else {
      var input = {
        request: { body: { } },
        id: profile.id,
        fullName: profile.displayName,
        register_type: 'twitter'
      };
      logger.debug('Attempting to create user from twitter account: ' + JSON.stringify(input));
      userRoute.createUser(input, function(err, userData) {
        if (err) {
          logger.error('An error occured when creating the user ' + err);
          return done(err);
        } else {
          var user = userData.user;
          if (user) {
            user.loginType = 'twitter';
            user.userKey = userData.userLogin.userKey;
          }
          return done(null, user);
        }
      });
    }
  });
};

exports.handleCallback = passport.authenticate('twitter',
{
  successRedirect: '/oath/close.html',
  failureRedirect: '/login',
  failureFlash: nconf.get('err_twitter:serverError')
});

