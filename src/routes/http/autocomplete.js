var autocomplete = require('../../lib/autocomplete/autocomplete');

exports.organization = function (req, res) {
  var query = req.param('term');
  autocomplete.getOrganizationSuggestions(query, function(err, doc) {
    if (err) res.json({ error: err });
    else res.json(doc);
  });
};

exports.university = function (req, res) {
  var query = req.param('term');
  autocomplete.getUniversitySuggestions(query, function(err, doc) {
    if (err) res.json({ error: err });
    else res.json(doc);
  });
};
