// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/**
 * A User is defined simply as a user of the GiveToCollege application
 * Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
 **/
// Library imports
var async = require('async'),
    bcrypt = require('bcrypt'),
    check = require('validator').check,
    crypto = require('crypto'),
    logger = require('log4js').getLogger('user'),
    passport = require('passport'),
    mongoose = require('mongoose'),
    passwordUtils = require('../../common/utils/password_util'),
    sanitize = require('validator').sanitize,
    nconf = require('nconf'),
    util = require('util');

// GTC local imports
var events = require('../../lib/achievements').events,
    eventQueue = require('../../event_queue'),
    login = require('./login'),
    InputErrors = require('../../lib/util/InputError'),
    Validator = require('../../lib/models/validator'),
    userHelpers = require('../../lib/user'),
    roles = require('../../roles'),
    configuration = require('../../configuration'),
    email = require('../../email');

// Schema related imports
var User = require('../../lib/models/user'),
    Campaign = require('../../lib/models/campaign'),
    Organization = require('../../lib/models/organization'),
    UserLogin = require('../../lib/models/userLogin'),
    UserCampaign = require('../../lib/models/userCampaign'),
    UserAchievement = require('../../lib/models/userAchievement'),
    ObjectId = mongoose.Types.ObjectId;

exports.viewUser = function(request, response) {
  var user = request.user;
  var info = request.flash('info');
  var error = request.flash('error');
  if (user) {
    var user_id = request.params.id || user._id;
    var editable = user_id == user._id || roles.getRoleLevel(user) >= 3;
    var loginType = editable ? user.loginType : '';

    User.findOne({ _id: user_id}, function(err, user) {
      if (user) {
        response.render('user_profile', { 
          title: 'User Information', 
          loginType: loginType, 
          userId: user_id,
          editable: editable,
          error: error,
          info: info
        });
      } else {
        response.send(404);
      }
    });
  } else {
    response.redirect('/login');
  }
};


exports.viewHomepage = function(request, response) {
  var userRole = request.user.roleLevel;
  var homepage;
  switch(userRole) {
    case 1:
      homepage = 'user';
      break;
    case 2:
      homepage = 'admin';
      break;
    case 3:
      homepage = 'superadmin';
      break;
  }
  response.render('homepage/' + homepage, { title: 'Homepage', userId: request.user._id });
};

exports.register = function(request, response) {
  response.render('register', { title: 'Register' });
};


/**
 * Render the registration page
 **/
exports.renderUserRegistration = function(request, result) {
  var input = request.flash('info').pop() || {};
  var errors = request.flash('error').pop();
  result.render('user_register', { title: 'Register', input: input, errors: errors });
};

exports.createUserFromEmail = function(request, result, next) {
  var input = {
    id: request.body.email,
    email: request.body.email,
    fullName: request.body.fullName,
    password: request.body.password || "",
    school_name: request.body.school_name || "",
    register_type: 'email'
  };
  createUser(input, function(err, userData) {
    // If an error has occurred, then go back to the registration page
    // Otherwise, try to login the user.
    if (err) {
      request.flash('error', err);
      result.redirect('/user/register');
    } else {
      request.email = userData.email,
      request.password = userData.password,
      logger.trace("Redirecting user " + userData.email);
      passport.authenticate('local',
        {
          successRedirect: '/user/homepage',
          failureRedirect: '/user/register'
        }
      )(request, result, next);
    }
  });
};

/**
 * Create a user.  The goal of this function is to either create a user or to give
 * as much feedback as possible back in the response.  As a result, we try everything
 **/
var createUser = exports.createUser = function(input, callback) {
  async.waterfall([
    function(callback) { sanitizeAndValidateUser(input, callback); },
    function(user, callback) { saveUserData(input, user, callback); },
    function(userData, callback) { addGiveToCollegeCampaigns(input, userData, callback); }
  ], callback);
};

/**
 * Validates that the user object is indeed valid.
 * Returns the sanitized user object or null if the 
 **/
var sanitizeAndValidateUser = exports.sanitizeAndValidateUser = function(input, done) {
  logger.trace('Sanitizing and validating user');
  // FIXME Think of a better way to ensure that all user-related errors get logged
  var errors = new InputErrors(['fullName', 'email', 'school_name']);

  var user = {
    fullName: input.fullName,
    user_roles: []
  };

  // Check that the either the fullName or first and last name exist
  if (user.fullName) {
    if (user.fullName.split(/\s+/).length >= 2) {
      user.fullName = sanitize(user.fullName).trim();
    } else {
      errors.add('fullName', nconf.get('err_user:validation:invalidName'));
    }
  } else {
    errors.add('fullName', nconf.get('err_user:validation:emptyName'));
  }

  // Email, if provided, must be a valid email
  try {
    if (input.email) {
      check(input.email).isEmail();
      input.email = user.email = sanitize(input.email).trim().toLowerCase();
    } else if (input.register_type == 'email') {
      errors.add('email', nconf.get('err_user:validation:emptyEmail'));
    }
  } catch (e) {
    errors.add('email', nconf.get('err_user:validation:invalidEmail'));
  }
  
  // Finally, validate school names
  // school_name should only ever show up during registration, where a 
  // user does not have any roles.
  if (input.school_name) {
    // If a School Name is provided, then validate and add to User role
    var name = sanitize(input.school_name || "").trim().toLowerCase();
    logger.debug("looking up organization: " + name);
    Organization.findOne({ name: name },function(err, organization) {
      if (err) {
        logger.error(util.format('An error occurred while validating organization name %s: %s', name, err));
        errors.add(nconf.get('err_user:validation:couldNotAccess'));
      } else if (organization) {
        logger.debug(util.format('Found organization %s, adding user role to organization', organization.name));
        user.user_roles.push(organization._id);
      } else {
        logger.debug(util.format('Organization %s not found', name));
      }
      return done(errors.getErrors(), user);
    });
  } else {
    return done(errors.getErrors(), user);
  }
};

/**
 * Validate input.  If input is valid, then callback with no error.
 * Otherwise, callback returning the error JSON object.
 **/
var saveUserData = function(input, user, callback) {
  logger.trace('Saving user data');
  var errors = new InputErrors(['password']);
  var user = new User(user, { strict: true, _id: true });
  var userLogin = new UserLogin({}, { strict: true, _id: true });

  userLogin.userRef = user;

  if (input.register_type && input.id) {
    userLogin.userKey = login.getLoginKey(input.register_type, input.id);
  } else {
    logger.warn("User Login information was not present during save: " + JSON.stringify(input));
    errors.add(nconf.get('err_user:registration:couldNotCreateUser'));
  }

  // Password must exist only for email accounts
  if (input.password && input.register_type == 'email') {
    userLogin.passwordHash = passwordUtils.encryptPassword(input.password);
  } else if (input.register_type == 'email') {
    errors.add('password',nconf.get('err_user:registration:emptyPassword'));
  }

  logger.debug(util.format('Saving user %s with login %s', JSON.stringify(user), JSON.stringify(userLogin)));
  async.series([
    function(callback) { user.save(callback); },
    function(callback) { userLogin.save(callback); }
  ], function(err) {
    if (err) {
      logger.error("An error occurred while trying to save the user: " + err);
      errors.add(nconf.get('err_user:registration:couldNotAccess'));
      logger.debug("User creation unsuccessful: " + JSON.stringify(user));
      logger.debug("User Login creation unsuccessful: " + JSON.stringify(userLogin));
    } else {
      logger.debug("User creation successful: " + JSON.stringify(user));
      logger.debug("User Login creation successful: " + JSON.stringify(userLogin));
    }
    return callback(errors.getErrors(), userLogin);
  });
};

var addGiveToCollegeCampaigns = function(input, userLogin, callback) {
  logger.trace('Adding default campaigns');
  var errors = new InputErrors();
  eventQueue.publish(events.ADD_CAMPAIGN, userLogin.userRef, { campaigns: ['gtc_complete_profile', 'gtc_invite', 'gtc_donate', 'gtc_pledge'] });
  return callback(errors.getErrors(), userLogin);
};

exports.getInfo = function(request, response) {
  var userId = request.params.id || request.user._id;
  User.findById(userId)
      .populate('user_roles')
      .populate('admin_roles')
      .exec(function(err, user) {
    if (err) {
      logger.error('An error occurred while retrieving user ' + userId + ': ' + err);
      return response.send(500);
    } else if (!user) {
      logger.error('Could not find user ' + userId);
      return response.send(404);
    }

    // Do some post-processing
    var userObj = user.toObject();

    // ProfileImage is a virtual and is not passed along in the toObject() call
    userObj.profileImage = user.profileImage;

    // If the admin field is empty, then we do not want to reveal to the user
    // that an admin field exists, so remove it from the response
    if (userObj.admin_roles.length === 0) {
      delete userObj.admin_roles;
    }

    return response.send(userObj);
  });
};

exports.getCampaigns = function(request, response) {
  var userId = request.user._id;
  UserCampaign.find({ user: userId })
  .populate('campaign')
  .exec(function(err, campaigns) {
    if (err) {
      logger.error('An error occurred while retrieving user campaigns: ' + userId + ': ' + err);
      return response.send(500);
    } else {
      return response.send(campaigns.map(function(x) { return x.toObject(); }));
    }
  });
};

exports.getAchievements = function(request, response) {
  var userId = request.user._id;
  UserAchievement.find({ user: userId })
  .populate('achievement')
  .exec(function(err, achievements) {
    if (err) {
      logger.error('An error occurred while retrieving user achievements: ' + userId + ': ' + err);
      return response.send(500);
    } else {
      return response.send(achievements.map(function(x) { return x.toObject(); }));
    }
  });
};

Validator = require('validator').Validator;

exports.putInfo = function(request, response) {
  var currUser = request.user;
  var userId = request.params.id || currUser._id;
  var context = request.query.context || '';

  // Check to see if the user has privileges
  if (userId != currUser._id && roles.getRoleLevel(currUser.user) < 3) {
    return response.send(403);
  }

  // Validate input
  var validator = new Validator();
  validator.error = function (msg) {
    this._errors.push(msg);
    return this;
  };

  validator.getErrors = function () {
    return this._errors;
  };

  validator.check(request.body.email, nconf.get('err_user:validation:invalidEmail2')).isEmail();
  validator.check(request.body.first, nconf.get('err_user:validation:invalidFirstName')).notEmpty();
  validator.check(request.body.last, nconf.get('err_user:validation:invalidLastName')).notEmpty();

  if (request.body.dob) {
    validator.check(request.body.dob, nconf.get('err_user:validation:invalidBirthDate')).isDate();
    validator.check(new Date(request.body.dob), nconf.get('err_user:validation:timeTravelBirthDate')).isBefore();
  }

  if (request.body.mailing_address.zip) {
    validator.check(request.body.mailing_address.zip, nconf.get('err_user:validation:invalidZip')).len(4,5).isInt().min(0);
  }

  var errors = validator.getErrors();
  if (errors.length) {
    return response.send(400, errors);
  }

  // Sanitize input
  request.body.first = sanitize(request.body.first).xss();
  request.body.last = sanitize(request.body.last).xss();
  request.body.mailing_address.street1 = sanitize(request.body.mailing_address.street1).xss();
  request.body.mailing_address.street2 = sanitize(request.body.mailing_address.street2).xss();
  request.body.mailing_address.city = sanitize(request.body.mailing_address.city).xss();
  request.body.mailing_address.state = sanitize(request.body.mailing_address.state).xss();
  request.body.mailing_address.zip = sanitize(request.body.mailing_address.zip).toInt();

  // If DOB was not supplied, then delete dob field so that mongoose update does not fail
  if (!request.body.dob) {
    delete request.body.dob;
  }

  // If zip was not supplied, then delete zip field so that mongoose update does not fail
  if (!request.body.mailing_address.zip) {
    delete request.body.mailing_address.zip;
  }

  delete request.body._id;
  // You cannot update admin roles or superadmin through this interface
  delete request.body.admin_roles;
  delete request.body.superadmin;

  // Create a plan of action.
  var plan = [];

  // If there is a request to add an organization, then add it to
  // the plan to lookup the organization name and add the id to the
  // user role.
  if (request.body.addToUser) {
    var organizationName = request.body.addToUser;
    delete request.body.addToUser;

    plan.push(function(callback) {
      Organization.findOne({ name: organizationName.toLowerCase() })
        .exec(function(err, organization) {
          if (err) {
            logger.error('An error occurred while trying to validate organization: ' + err);
          } else if (organization) {
            request.body.user_roles.push(organization);
          }
          return callback();
        });
    });
  }

  // Add Deduplication of User Role onto the plan of action.
  plan.push(function(callback) {
    var userOrganizations = {};
    request.body.user_roles.forEach(function (entry) {
      if (typeof(entry) == 'object' && entry._id) {
        userOrganizations[entry._id] = true;
      } else if (typeof(entry) == 'string') {
        userOrganizations[entry] = true;
      }
    });
    request.body.user_roles = Object.keys(userOrganizations);
    return callback();
  });

  // Add the update user action to the plan.
  plan.push(function(callback) {
    // If all was good, go ahead and update the user
    User.findByIdAndUpdate(userId, { '$set': request.body })
        .populate('user_roles')
        .populate('admin_roles')
        .exec(function(err, user) {
      if (err) {
        logger.error('Could not update user ' + userId + ': ' + err);
        response.send(500);
        return callback(true);
      } else if (!user) {
        logger.error('Could not find user ' + userId);
        response.send(404);
        return callback(true);
      }
      return callback(null, user);
    });
  });

  // If all goes well, we want to publish that the user's profile has been updated
  plan.push(function(callback) {
    eventQueue.publish(events.PROFILE_UPDATE, userId);
    return callback();
  });

  // Finally, execute the plan
  async.series(plan, function(err, result) {
    if (err) {
      // If we haven't sent it already, send a 500
      return response.send(500);
    }
    return response.send(result.slice(-2)[0]);
  });
};

exports.viewUserSettings = function(request, response) {
  var info = request.flash('info');
  var error = request.flash('error');
  var success = request.flash('success');

  response.render('user_settings', { 
    title: 'Settings', user: request.user, 
    info: info, 
    error: error, 
    success: success });
};

exports.changePassword = function(request, response) {
  if (!request.user || request.user.loginType != 'email') {
    return response.send(400);
  }

    // Validate input
  var validator = new Validator();
  validator.error = function (msg) {
    this._errors.push(msg);
    return this;
  };

  validator.getErrors = function () {
    return this._errors;
  };
  validator.check(request.body.password, nconf.get('err_user:validation:passwordEmpty')).notEmpty();
  validator.check(request.body.new_password, nconf.get('err_user:validation:newPasswordEmpty')).notEmpty();
  validator.check(request.body.retype_password, nconf.get('err_user:validation:retypePasswordEmpty')).notEmpty();
  validator.check(request.body.new_password, nconf.get('err_user:validation:passwordsDontMatch')).equals(request.body.retype_password);

  var userKey = login.getLoginKey(request.user.loginType, request.user.email);

  UserLogin.findOne({ userKey: userKey }, function(err, userLogin) {
    if (err) {
      logger.error('An error occurred while looking up user login');
      return response.send(500);
    }

    var validated = passwordUtils.validatePassword(userLogin.passwordHash, request.body.password);
    validator.check(validated, nconf.get('err_user:validation:incorrectPassword')).is();

    var errors = validator.getErrors();
    if (errors.length) {
      errors.forEach(function(e) { request.flash('error', e); });
      logger.debug('Failed change password request: ' + JSON.stringify(errors));
      return response.redirect('user/settings');
    }

    userLogin.passwordHash = passwordUtils.encryptPassword(request.body.new_password);
    userLogin.save(function(err) {
      if (err) {
        logger.error('An error occurred while updating user\'s password: ' + err);
        errors.error('An unexpected error occured.  Please try again later');
        return response.send(500);
      }

      errors.forEach(function(e) { request.flash('error', e); });
      request.flash('success', nconf.get('err_user:validation:passwordSuccess'));
      return response.redirect('user/settings');
    });

  });
};

exports.listUsers = function(request, response) {
  var page = request.query.page;
  if ( !page ) {
    page = 0;
  } else {
    page = parseInt(page, 10);
    if ( !page || isNaN(page) ) {
      page = 0;
    }
  }
  var search = request.query.search;
  var query = {};
  if (search) {
    queryRegex = new RegExp(".*" + search + ".*", "i");
    query = { "email": queryRegex };
  }
  User.count(query, function(error, count) {
    var numberOfPages = Math.floor( count / 10 );
    if ( page < 0 || page > numberOfPages ) {
      page = 0;
    }
    var nextPage;
    if (page<numberOfPages) {
      nextPage = page + 1;
    }
    var previousPage = page - 1;
    logger.info("page = " + nextPage + "," + previousPage);
    User.find(query, {}, { skip: page*10, limit: 10 }, function(error, users) {
      response.render('user_list', { title: 'User List', users: users, page: page, numberOfPages: numberOfPages, nextPage: nextPage, previousPage: previousPage});
    });
  });
};

exports.receipts = function(request, response) {
  response.render('user/receipts', { title: 'Receipts' });
};
