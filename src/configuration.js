// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Defines the roles.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/
var nconf = require('nconf'),
    util = require('util'),
    log4js = require('log4js');
var logger = log4js.getLogger('configuration');
var messages = require('./messages'),
    msgConfig = messages.configList,
    msgDir = messages.configDir;

/* Helper */
function configureList(configDir, configList) {
  /** 
   * Assuming :
   * nconf.use('memory');
   * nconf.env().argv();
   * Already done.
   **/
  for(var i = 0; i < configList.length; i++) {
    var configName = configList[i].name;
    var configRequired = configList[i].required;
    var configFile = configDir + '/' + configName + '.conf';
    logger.info('Adding configuration ' + configName + ' from file ' + configFile + " required: " + configRequired);
    try {
      nconf.add(configName, { type: 'file', file: configFile });
    } catch( e ) {
      if (configRequired) {
        logger.error('Exception caught : ' + e);
        logger.error('On File : ' + configName);
        throw new Error(e);
      } else {
        logger.info("The configuration file '" + configFile.name + "' wasn't added to the configurations.");
      }
    }
  }
}
exports.configureApplication = function(configDir) {
  /*
    Configuration hierarchy:
      1. app.conf (application configuration, should not be changed in production)
      2. global.conf (contains services configuration like DB, mail server...)
      3. an override config file for testing.
      4. arguments
      5. environment variables
  */
  nconf.use('memory');

  nconf.env().argv();

  var configList = [  { name: 'override', required: false },
                      { name: 'global', required: true },
                      { name: 'app', required: true } ];
  configureList(configDir, configList);
  configureList(msgDir, msgConfig);
};
/**
 * Return the server URI, e.g. http://www.givetocollege.com:3000
 * if isHttps is true, then the server URI returned is https
 **/
exports.getServerURI = function(isHttps) {
  if (!isHttps) {
    var uri = nconf.get('http:server');
    var port = nconf.get('http:port');
    port = port == '80' ? '' : ":" + port;
    return util.format("http://www.%s%s", uri, port);
  } else {
    // TODO return HTTPS server domain
    return undefined;
  }

};

exports.get = function(key) {
  return nconf.get(key);
};
