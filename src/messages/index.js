var configList = [ 
  {name: "err_authorization", required: true},
  {name: "err_donate", required: true},
  {name: "err_facebook", required: true},
  {name: "err_image_upload", required: true},
  {name: "err_login", required: true},
  {name: "err_organization", required: true},
  {name: "err_roles", required: true},
  {name: "err_twitter", required: true},
  {name: "err_user", required: true}
  ];
var configDir = __dirname;

exports.configList = configList;
exports.configDir = configDir;