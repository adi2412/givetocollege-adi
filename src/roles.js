// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Defines the roles.
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/
var nconf = require('nconf');
var roleMap =
  {
    none: 0,
    user: 1,
    admin: 2,
    superadmin: 3
  };

var getRoleLevel = function(user) {
  if (user === undefined) {
    return roleMap.none;
  } else if (user.superadmin) {
    return roleMap.superadmin;
  } else if ('admin_roles' in user && user.admin_roles.length > 0) {
    return roleMap.admin;
  } else {
    return roleMap.user;
  }
};

var requireRole = function(requiredRole, redirect) {
  return function(request, response, next) {
    if (requiredRole == 'none') {
      next();
    } else if ('user' in request) {
      if (getRoleLevel(request.user) >= roleMap[requiredRole]) {
        next();
      } else {
        response.send(403);
      }
    } else if (redirect !== undefined && redirect) {
      request.flash('error', nconf.get('err_roles:auth'));
      response.redirect(redirect);
    } else {
      response.send(403);
    }
  };
};

module.exports.getRoleLevel = getRoleLevel;

module.exports.requireNone = function() { return requireRole('none'); };
module.exports.requireUser = function() { return requireRole('user', '/login'); };
module.exports.requireAdmin = function() { return requireRole('admin'); };
module.exports.requireSuperAdmin = function() { return requireRole('superadmin'); };
