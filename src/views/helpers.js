exports.helpers = {
  renderSource: function (source) {
    switch (source) {
      case "email":
        return "Email";
      case "facebook":
        return "Facebook";
      case "gmail":
        return "GMail";
      case "linkedin":
        return "LinkedIn";
    }
  }
};
