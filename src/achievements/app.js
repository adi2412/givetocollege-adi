// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var achievements = require('../lib/achievements'),
    async = require('async'),
    express = require('express'),
    http = require('http'),
    MongoStore = require('connect-mongo')(express),
    mongoose = require('mongoose'),
    eventQueue = require('../event_queue'),
    configuration = require('../configuration'),
    log4js = require('log4js'),
    achievementEventManager = achievements.achievementEventManager;

var Achievement = require('../lib/models/achievement');

var app = express();

app.configure(function() {
  configuration.configureApplication(__dirname + '/config');
  app.set('port', configuration.get('http:port'));
  app.use(express.favicon());
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

app.configure('development', function() {
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  app.use(log4js.connectLogger(logger, { level: log4js.levels.DEBUG }));
  mongoose.set('debug', true);
});

app.configure('production', function() {
  mongoose.set('debug', false);
  app.use(log4js.connectLogger(logger, { level: log4js.levels.WARN }));
});

// log4js configuration.
log4js.configure(__dirname + '/config/log4js_configuration.json');
var logger = log4js.getLogger('Achievement Server');
logger.info('Starting Achievement Manager Server');

// Initialization Steps
async.series([
  function(callback) {
    logger.info('Initializing connection to DB...');
    var connection = 'mongodb://' + configuration.get('mongodb:server') + '/' + configuration.get('mongodb:db_name');
    logger.info('Database connection = ' + connection);
    var db = mongoose.connect(connection, callback);
  },
  function(callback) {
    // Subscribe the Achievements Event listener to the event queue
    logger.info('Initializing Event Logger service...');
    async.series([
      function(callback) { eventQueue.initialize(callback); },
      function(callback) { 
        var err = eventQueue.subscribe('event.#', achievements.listeners.eventListener);
        return callback(err);
      }
    ], callback);
  },
  function(callback) {
    // Upsert existing achievements
    async.parallel(
      achievements.achievements.map(function(achievement) {
        return function(callback) {
          console.info('Creating/Upserting achievement ' + achievement.slug);
          return Achievement.update({ slug: achievement.slug }, achievement, { upsert: true }, callback);
        };
      }), callback);
  },
  function(callback) {
    // Start the Achievement Manager
    logger.info('Initializing Achievement Event Manager...');
    achievementEventManager.initialize(achievements.emitter, achievements.achievements, callback);
  },
  function(callback) {
    // Configure the routes.
    return callback();
  }
], function(err, result) {
  if (err) {
    console.error('An error occurred during initialization: %s', err);
    process.exit(-1);
  } else {
    // Start the server
    logger.info("Initialization successful!  Starting server...");
    var server = http.createServer(app);
    server.listen(app.get('port'), function() {
      logger.info("Achievement Server listening on port " + app.get('port'));
    });
  }
});

process.on('exit', function() {
  logger.info("Stopping Achievement Manager...");
  achievementEventManager.destroy();
  logger.info("Destroying Event Logger service...");
  eventQueue.destroy();
  logger.info("Disconnecting from DB...");
  db.close();
});

process.on('SIGINT', function() {
  logger.info( "gracefully shutting down from  SIGINT (Crtl-C)" );
  process.exit();
});
