// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var bcrypt = require ('bcrypt'),
    crypto = require('crypto');

exports.encryptPassword = function(password) {
  return bcrypt.hashSync(password,8);
};

exports.validatePassword = function(passwordHash, password) {
  if(!passwordHash) {
    return false;
  }
  return bcrypt.compareSync(password, passwordHash);
};

exports.generateTemporaryPassword = function() {
  return crypto.randomBytes(24).toString('hex');
};
