// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var amqp = require('amqp'),
    async = require('async'),
    config = require('./configuration'),
    logger = require('log4js').getLogger('eventQueue'),
    util = require("util");

var eventExchangeName = "events";
var eventQueueName = "events";

var connectionConfiguration = null;
var connection = null;
var exchange = null;
var queue = null;

/**
 * Initializes the event queue.  This needs to be called before
 * anything can be done with the event queue.
 **/
exports.initialize = function(callback) {
  // If callback is not specified, then just create a dummy callback
  if (!callback) {
    callback = function() { };
  }

  if (connection) {
    logger.warn("Connection already initialized, ignoring second call to initialize()");
    return callback();
  }

  connectionConfiguration = config.get("rabbitmq:protocol") + "://" +
    config.get("rabbitmq:login") + ":" + config.get("rabbitmq:password") + "@" +
    config.get("rabbitmq:host") + ":" + config.get("rabbitmq:port") + config.get("rabbitmq:vhost");

  logger.info("Using configuration: %s", JSON.stringify(connectionConfiguration));
  connection = amqp.createConnection({ url: connectionConfiguration });

  connection.on('ready', function () {
    async.parallel({
      'eventExchange': function(callback) { 
        connection.exchange(eventExchangeName, { type: 'topic', durable: true, autoDelete: false }, function(exchange) {
          if (exchange) {
            return callback(undefined, exchange);
          } else {
           return callback('Error while initializing Event Exchange');
          }
        }); 
      },
      'eventQueue': function(callback) { 
        connection.queue(eventQueueName, { durable: true, autoDelete: false }, function(queue) {
          if (queue) {
            return callback(undefined, queue);
          } else {
            return callback('Error while initializating Event Queue');
          }
        });
      }
    }, function(err, result) {
      if (err) {
        logger.error("An error occurred while trying to initialize Event Logging service: %s", err);
      } else {
        logger.info("Successfully initialized Event Logging service!");
        exchange = result.eventExchange;
        queue = result.eventQueue;
        queue.bind(eventExchangeName, '#');
      }
      return callback(err);
    });
  });
};

/**
 * Tears down the event queue.
 **/
module.exports.destroy = function() {
  logger.info("Tearing down Event Logging service");
  if(connection) {
    connection.end();
  }
};

/**
* Subscribes to an event queue.
* Subscribes with routingKeyInPayload option set to true
**/
module.exports.subscribe = function(topic, listener) {
  if (!connection) {
    logger.warn("Ignoring request to subscribe to topic %s before connection is ready", topic);
    return 'Connection is not ready';
  }

  if (topic === undefined || !topic) {
    logger.warn("Ignoring request to subscribe to undefined or empty topic");
    return 'Topic not defined';
  } else if (listener === undefined || !listener) {
    logger.warn("Ignoring request to subscribe using an undefined or empty listener");
    return 'Listener not defined';
  }

  logger.debug("Adding listener to topic %s", topic);
  queue.subscribe({ routingKeyInPayload: true }, listener);
};

/**
 * Publish an event to the event queue.
 * Fire and forget.
 **/
module.exports.publish = function(type, user, context) {
  var message = { 
    type: type,
    date: Date.now(),
    user: user._id || user,
    context: context || {}
  };

  if (exchange) {
    exchange.publish("event." + type, JSON.stringify(message));
  } else {
    logger.error("Could not publish event because event exchange has not been initialized!");
  }
};
