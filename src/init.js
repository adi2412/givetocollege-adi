// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var check = require('validator').check,
    requiredCampaigns = require('./lib/campaigns/required_campaigns'),
    configuration = require('./configuration'),
    logger = require('log4js').getLogger('init'),
    mongoose = require('mongoose'),
    util = require('util');

// Schema related imports
var Campaign = require("./lib/models/campaign")
  ;

/**
 * There are some things that we need to make sure exist prior to startup.
 * This function is responsible for setting up these things.
 * TODO Fix this so that its run one time during an env setup phase rather than @ every startup.
 **/
module.exports = function(context) {
  loadGiveToCollegeCampaigns();
};

var loadGiveToCollegeCampaigns = function() {
  logger.info('Loading required campaigns...');
  requiredCampaigns.forEach(function(campaign) {
    Campaign.update({ slug: campaign.slug }, campaign, { upsert: true }, function(err) {
      if (err) {
        logger.error(util.format('Could not store required campaign %s: %s',
          JSON.stringify(campaign), err));
      } else {
        logger.info('Successfully loaded campaign %s', JSON.stringify(campaign.name));
      }
    });
  });
};
