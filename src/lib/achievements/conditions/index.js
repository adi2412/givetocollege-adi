// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

/**
 * All possible conditions.  If you are planning to add a condition, make sure
 * it also gets added here.
 **/
exports.PROFILE_COMPLETED = 'ProfileCompleted';
exports.INVITATION_MILESTONE = 'InvitationMilestone';
exports.ADDED_ORGANIZATION = 'AddedOrganization';
exports.MAILING_ADDRESS_COMPLETED = 'MailingAddressCompleted';

exports.ProfileCompleted = require('./ProfileCompleted');
exports.InvitationMilestone = require('./InvitationMilestone');
exports.AddedOrganization = require('./AddedOrganization');
exports.MailingAddressCompleted = require('./MailingAddressCompleted');
