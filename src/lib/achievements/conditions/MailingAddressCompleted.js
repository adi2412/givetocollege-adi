// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var logger = require('log4js').getLogger('ProfileCompleted');

// TODO dougkang Turn this into an object

/**
 *  Given a user object, check to see if mailing address has been filled out
 **/
module.exports = function(user, context) {
  if (context === undefined || !context) {
    logger.debug('Context was not passed in arguments');
    return -1;
  } else if (user === undefined || !user) {
    logger.debug('User was not provided');
    return -1;
  }

  return user.mailing_address !== undefined && user.mailing_address ? 100 : 0;
};
