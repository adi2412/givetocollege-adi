// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var logger = require('log4js').getLogger('AddedOrganization');

// TODO dougkang Turn this into an object

/**
 *  Given a user object, check to see if user roles have been added
 **/
module.exports = function(user, context) {
  if (context === undefined || !context) {
    logger.debug('Context was not passed in arguments');
    return -1;
  } else if (user === undefined || !user) {
    logger.debug('User was not provided');
    return -1;
  }

  return user.user_roles !== undefined && user.user_roles.length ? 100 : 0;
};