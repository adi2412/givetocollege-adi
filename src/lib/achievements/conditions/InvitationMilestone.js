// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var step = require('step');
var logger = require('log4js').getLogger('AchievementMilestone');

var Invite = require('../../models/invite');

/**
 *  Given a user object, check to see how many people have accepted his invitations
 *  Important Note:  Notice that this function actually returns a function for that particular milestone.
 **/
module.exports = function(milestone) {
  var threshold = milestone;
  return function(user, context) {
    if (context === undefined || !context) {
      logger.debug('Context was not passed in arguments');
      return -1;
    } else if (user === undefined || !user) {
      logger.debug('User was not provided');
      return -1;
    }

    step(
      function() { Invite.count({ user: user._id, status: 'invited'}, this); },
      function(err, count) {
        if (err) {
          logger.error('An error occurred while looking up invitation achievement status: ' + err);
          return -1;
        } else if (!count) {
          return 0;
        }
        return Math.floor(count / threshold);
      });
  };
};
