// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var logger = require('log4js').getLogger('ProfileCompleted');

// TODO dougkang Turn this into an object

/**
 *  Given a user object, check to see if the profile is complete
 **/
module.exports = function(user, context) {
  if (context === undefined || !context) {
    logger.debug('Context was not passed in arguments');
    return -1;
  } else if (user === undefined || !user) {
    logger.debug('User was not provided');
    return -1;
  }

  return user.email !== undefined && user.email
    && user.first !== undefined && user.first
    && user.last !== undefined && user.last
    && user.dob !== undefined && user.dob ? 100 : 0;
};
