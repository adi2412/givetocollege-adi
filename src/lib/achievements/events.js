// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

/**
 * Listed below are the types of updates that we listen for when dealing with
 * achievements.
 **/

module.exports = {
  PROFILE_UPDATE: 'profileUpdate',
  ADD_ORGANIZATION: 'addOrganization',
  ADD_CAMPAIGN: 'addCampaign',
  INVITE_ACCEPTED: 'inviteAccepted',
  FAILED_DONATION: 'donation',
  DONATION: 'donation',
  FAILED_PLEDGE: 'pledge',
  PLEDGE: 'pledge'
};
