// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var EligibleAchievements = function(user) {
  this._user = user;
  this._user.eligibleAchievements = this._user.eligibleAchievements || {};
}

EligibleAchievements.prototype.contains = function(achievement) {
  return this._user.eligibleAchievements[achievement] !== undefined;
}

EligibleAchievements.prototype.updateAchievement = function(achievement, score) {
  if (this.contains(achievement)) {
    this._user.eligibleAchievements[achievement] = score;
    this._user.markModified('eligibleAchievements');
  }
}

EligibleAchievements.prototype.addAchievement = function(achievement) {
  if (!this.contains(achievement)) {
    this._user.eligibleAchievements[achievement] = 0;
    this._user.markModified('eligibleAchievements');
  }
}

EligibleAchievements.prototype.removeAchievement = function(achievement) {
  if (this.contains(a)) {
    delete this._user.eligibleAchievements[achievement];
    this._user.markModified('eligibleAchievements');
  }
}

module.exports = EligibleAchievements;