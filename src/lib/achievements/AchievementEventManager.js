// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var async = require('async'),
  events = require('./events'),
  AchievementEventHandler = require('./AchievementEventHandler'),
  logger = require('log4js').getLogger('AchievementEventManager');

var Achievement = require('../models/achievement');
var Campaign = require('../models/campaign');
var UserCampaign = require('../models/userCampaign');
var UserAchievement = require('../models/userAchievement');

var instance = null;

exports.initialize = function(emitter, achievements, callback) {
  if (instance) {
    logger.warn('Achievement Manager already initialized, ignoring second call to initialize()');
    return callback();
  }

  instance = new AchievementEventManager({
    emitter: emitter,
    achievements: achievements
  });
  instance.start();
  return callback();
};

exports.destroy = function() {
  if (instance) {
    instance.stop();
  }
};

/**
 * The AchievementEventManager is a class responsible for managing Achievement Event Handlers, 
 * starting and stopping the event listeners.
 **/
var AchievementEventManager = function(options) {
  this._emitter = options.emitter;
  this._achievementEvents = [];

  if (options.achievements !== undefined) {
    options.achievements.forEach(function(achievement) {
      var slug = achievement.slug;
      var handler = new AchievementEventHandler({
        slug: slug,
        events: achievement.events,
        conditions: achievement.conditions,
        scoreFunction: achievement.scoreFunction
      });
      this.addEventHandler(handler);
    }, this);
  }

  // Additionally, we want to create an event handler for new campaigns being added
  // to users.
  this._campaignEventHandler = function(user, context, callback) {
    if (!callback) {
      callback = function() { };
    }

    var campaigns = context.campaigns;

    async.waterfall([
      function(callback) {
        return Campaign.find({ slug: { '$in': campaigns }}, callback);
      },
      function(campaigns, callback) {
        var achievements = campaigns.map(function(x) { return x.achievements; });
        achievements = achievements.reduce(function(prev, curr) { return prev.concat(curr); }, []);
        console.log(achievements);
        Achievement.find({ slug: { '$in': achievements }}, function(err, achievements) {
          return callback(err, { campaigns: campaigns, achievements: achievements });
        });
      },
      function(result, callback) {
        var userAchievements = result.achievements.map(function(x) { return { user: user, achievement: x }; });
        var userCampaigns = result.campaigns.map(function(x) { return { user: user, campaign: x }; });
        async.parallel([
          function(callback) { UserCampaign.create(userCampaigns, callback); },
          function(callback) { UserAchievement.create(userAchievements, callback); }
        ], callback);
      }
    ], function(err, result) {
      logger.debug('Completed Campaign Handler ' + campaigns + ': ' + err);
      return callback(err, result);
    });
  };
};

/**
 * Return the event emitter that we will be listening to
 **/
AchievementEventManager.prototype.getEventEmitter = function() {
  return this._emitter;
};

/**
 * Stop listening for events and swap out the event emiter
 **/
AchievementEventManager.prototype.setEventEmitter = function(emitter) {
  this.stop();
  this._emitter = emitter;
};

/**
 * Add an Achievement event handler to the Event Manager
 **/
AchievementEventManager.prototype.addEventHandler = function(eventHandler) {
  if (eventHandler.events === undefined || !(eventHandler.events instanceof Array) || !eventHandler.events.length) {
    logger.warn('Skipping AchievementEventHandler with no/invalid event array: ' + JSON.stringify(eventHandler));
  } else {
    this._achievementEvents.push(eventHandler);
  }
  return this;
};

/**
 * Start listening for events.
 **/
AchievementEventManager.prototype.start = function() {
  logger.info('Starting AchievementEventManager');
  this._achievementEvents.forEach(function(eventHandler) {
    eventHandler.events.forEach(function(eventType) {
      logger.debug("%s: Listening for %s events", eventHandler.achievement, eventType);
      this._emitter.on(eventType, eventHandler.getInstance());
    }, this);
  }, this);

  logger.debug("campaignEventHandler: Listening for %s events", events.ADD_CAMPAIGN);
  this._emitter.on(events.ADD_CAMPAIGN, this._campaignEventHandler);

  return this;
};

/**
 * Stop listening for events.
 **/
AchievementEventManager.prototype.stop = function() {
  logger.info('Stopping AchievementEventManager');
  this._achievementEvents.forEach(function(eventHandler) {
    eventHandler.events.forEach(function(eventType) {
      this._emitter.removeListener(eventType, eventHandler.getInstance());
    }, this);
  }, this);
  this._emitter.removeListener(events.ADD_CAMPAIGN, this._campaignEventHandler);
};
