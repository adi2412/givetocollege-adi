/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

exports.fivePoints = function() {
  return 5; 
};

exports.fiftyPoints = function() {
  return 50;
};

exports.onefiftyPoints = function() {
  return 150;
};

exports.fourHundredPoints = function() {
  return 400;
};

exports.oneThousandPoints = function() {
  return 1000;
};

exports.donate = function(context) {
  return 5*context.amount;
};
