// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var achievements = require('./')
  , async = require('async')
  , logger = require('log4js').getLogger('eventListener')
  ;

var User = require('../../lib/models/user');

/**
 * The Achievement Event listener parses the incoming message and 
 * fires off the appropriate events.  Effectively, you can call this a
 * message to event translator.
 *
 * An event contains at least the following fields:
 * type: the type of event. this type is effectively the event type.
 * user: the user that this event pertains to.
 * context: any other required data needed
 **/
module.exports.eventListener = function(message) {
  try {
    var data = JSON.parse(message.data);
    logger.debug("Received message: %s", JSON.stringify(data));
    
    async.parallel({
      type: function(callback) { 
        if (data.type === undefined || !data.type) {
          return callback('No type in event');
        } else {
          return callback(null, data.type);
        }
      },
      user: function(callback) { 
        if (data.user === undefined || !data.user) {
          return callback('No user in event');
        } else {
          return User.findById(data.user, callback);
        }
      }, 
      context: function(callback) {
        if (data.context === undefined || !data.context) {
          return callback('No context in event');
        } else {
          return callback(null, data.context);
        }
      },
    }, function(err, result) {
      if (err) {
        logger.error("An error occurred while creating event %s: %s", data.type, err);
        return false;
      }
      logger.debug("Valid event found!  Emitting event: %s", JSON.stringify(result));
      achievements.emitter.emit(result.type, result.user, result.context);
    });
  } catch (e) {
    logger.warn("An error occurred while parsing event %s, %s", message.data, e);
  }
}