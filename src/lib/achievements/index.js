// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var EventEmitter = require('events').EventEmitter;

exports.emitter = new EventEmitter();

exports.listeners = require('./listeners');

exports.events = require('./events');

exports.achievements = require('./achievements');

exports.achievementEventManager = require('./AchievementEventManager');
