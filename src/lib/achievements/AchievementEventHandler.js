// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var async = require('async'),
    logger = require('log4js').getLogger('AchievementEventHandler'),
    util = require('util');

var Organization = require('../models/organization');
var Notification = require('../models/notification');
var UserAchievement = require('../models/userAchievement');

var conditions = require('../achievements/conditions');
var score_functions = require('../achievements/score_functions');

/**
 * An Achievement Event Handler checks for achievement completion upon pertinent event emission.
 * Achievement Event Handlers compose of a couple of things:
 *  - which achievement this event handler is checking for completion.
 *  - which events would fire off this check.
 *  - the conditions that need to be satisifed before an achievement is completed.
 *  - the score function used to determine the points awarded to a user.
 */
var AchievementEventHandler = function(context) {
  this.slug = context.slug;
  this.events = context.events;
};

/**
 * The meat of this class, the result of this function should be called when a pertinent event is fired.  The
 * returned checks to see that the conditions have been satisified and, if they have been, calls achievementGet 
 * to award the user this achievement.  The same function (e.g. same reference) is always returned no matter how
 * many times you call this.
 *
 * Event handling is a bit weird in that the 'this' variable is not actually what you expect it to
 * be.  As such, getInstance actually packs the appropriate variables via lambda whilst returning a function.
 **/
AchievementEventHandler.prototype.getInstance = function() {
  var self = this;
  var slug = this.slug;

  // The handler is effectively a singleton object.  We do this because we want to be able to return the same
  // handler reference every time we call getInstance() so we don't have to re-construct it every time.
  // This makes things like removing listeners really easy.
  if (this._handler === undefined) {
    this._handler = function (user, context, callback) {
      if (!callback) {
        callback = function() { };
      }

      logger.debug(util.format('Checking for user [%s] achievement [%s] eligibility', user.fullName, slug));

      async.waterfall([
        function(callback) {
          UserAchievement.find({ user: user._id, dateCompleted: { '$exists': false } })
          .populate('achievement', null, { slug: slug })
          .exec(function(err, userAchievements) {
            var userAchievement = null;
            userAchievements = userAchievements.filter(function(x) { return x.achievement; });
            if (err) {
              logger.error('An error occurred while looking up user achievements: ' + err);
            } else if (userAchievements.length == 0) {
              logger.debug(util.format('Did not meet criteria for achievement %s: User not eligible for achievement', slug));
            } else if (userAchievements.length != 1) {
              logger.warn('A query for an achievement slug %s yielded more than one result: %s', slug, JSON.stringify(userAchievements));
            } else {
              userAchievement = userAchievements[0];
              logger.debug('Found achievement ' + JSON.stringify(userAchievement.achievement));
            }
            callback(err, userAchievement);
          });
        },
        function(userAchievement, callback) {
          if (!userAchievement) { 
            return callback();
          }

          var achievement = userAchievement.achievement;

          // Make sure that all conditions are satisfied.
          var parallelProgressUpdate = [];
          achievement.conditions.forEach(function(condition) {
            parallelProgressUpdate.push(function (callback) {
              return callback(null, conditions[condition](user, context));
            });
          });

          // Check all conditions in parallel.  
          async.parallel(parallelProgressUpdate, function(err, result) {
            var progress = 0;
            if (err) {
              logger.debug(util.format('An error occurred while updating progress for achievement %s: %s', achievement, err));
            } else {
              progress = result.length == 0 ? 100 : result.reduce(function (prev, curr) { return prev + curr; }) / result.length;
              if (progress != 100) {
                logger.debug(util.format('Did not meet criteria for achievement %s: conditions not met.  Progress %d\%', achievement, progress));
              } else {
                logger.debug(util.format('Achievement %s gotten! %s', achievement.slug, JSON.stringify(context)));
                var points = score_functions[achievement.scoreFunction](context);
                self.achievementGet(userAchievement, user, points, context, function(err) {
                  if (err) {
                    logger.error('An error occurred while updating achievements: ' + err);
                  }
                });
              }
            }
            return userAchievement.update({ '$set': { progress: progress } }, callback);
          });
        }
      ], callback);
    }
  }

  return this._handler;
};

/**
 * achievementGet() is the function that awards the user with an achievement.  Given the context,
 * it calculates the score to award the user and updates/adds the appropriate mongoose entries.
 **/
AchievementEventHandler.prototype.achievementGet = function(userAchievement, user, points, context, callback) {

  var achievement = userAchievement.achievement;

  logger.debug(util.format('User "%s" got achievement "%s" worth %d points', 
    user.fullName, 
    achievement,
    points));

  var query = {
    '$inc' : { points: points },
  };

  // Remove the achievement from the list of eligible achievements,
  // Add this achievement to the list of achievements for this user,
  // Update the total points.
  async.parallel([
    function(callback) {
      user.update(query, function(err) {
        if (err) {
          logger.error('An error occurred while giving user %s achievement %s: %s', user._id, achievement, err);
        }
        if (callback !== undefined) {
          return callback(err);
        }
      });
    },
    function(callback) {
      if (context.organization) {
        Organization.findOneAndUpdate({ display_name: context.organization }, { '$inc': { points: points } }, callback);
      }
    },
    function(callback) {
      Notification.create({ userRef: user._id, title: achievement.name, description: achievement.description }, callback);
    },
    function(callback) {
      if (achievement.isOneTimeAchievement) {
        logger.debug('Setting One Time Achievement %s to done', achievement.slug);
        userAchievement.update({ '$set': { dateCompleted: Date.now() } }, callback);
      }
    }
  ], callback);
};

module.exports = AchievementEventHandler;
