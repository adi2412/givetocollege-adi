// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var events = require('./events'),
  conditions = require('./conditions');

/**
 * Listed below are the types of achievements that can be won.
 * ACHIEVEMENTS!
 **/
module.exports = [
  {
    slug: 'profileComplete',
    name: 'Completed your Profile',
    description: 'You have completed your profile!',
    badge:  '/img/badges/badge-completeprofile.png',
    events:  [ events.PROFILE_UPDATE ],
    conditions: [ conditions.PROFILE_COMPLETED ],
    scoreFunction: 'fivePoints',
    isOneTimeAchievement: true,
    shouldShow: true
  },
  {
    slug: 'addedCollege',
    name: 'Added a College',
    description: 'You have added a college!',
    image:  '/img/badges/badge-schoolspirit.png',
    events:  [ events.ADD_ORGANIZATION ],
    conditions: [ conditions.ADDED_ORGANIZATION ],
    scoreFunction: 'fivePoints',
    isOneTimeAchievement: true,
    shouldShow: true
  },
  {
    slug: 'mailingAddressComplete',
    name: 'Compled Mailing Address',
    description: 'You have completed your mailing address!',
    badge:  '/img/badges/badge-leaf.png',
    events:  [ events.PROFILE_UPDATE ],
    conditions: [ conditions.MAILING_ADDRESS_COMPLETED ],
    scoreFunction: 'fivePoints',
    isOneTimeAchievement: true,
    shouldShow: true
  },
  { 
    slug: 'donation',
    name: 'Donation',
    description: 'You made a Donation!',
    badge:  '/img/badges/badge-pledge.png',
    events:  [ events.DONATION ],
    conditions: [ ],
    scoreFunction: 'donate',
    isOneTimeAchievement: false,
    shouldShow: false
  },
  { 
    slug: 'pledge',
    name: 'Pledge',
    description: 'You made a Pledge!',
    badge:  '/img/badges/badge-pledge.png',
    events:  [ events.PLEDGE ],
    conditions: [ ],
    scoreFunction: 'pledge',
    isOneTimeAchievement: false,
    shouldShow: false
  },
  { 
    slug: 'invitationAccepted',
    name: 'Invitation accepted!',
    description: 'Someone has accepted your invitation!',
    badge:  '/img/badges/badge-generic.png',
    events:  [ events.INVITE_ACCEPTED ],
    conditions: [ ],
    scoreFunction: 'fivePoints',
    isOneTimeAchievement: false,
    shouldShow: false
  },
  /* XXX Remove for now
  {
    slug: 'tenInvitationsAccepted',
    name: 'Invitation Milestone 1: 10 Invitations accepted',
    description: '10 people have accepted your invitations',
    image:  'img/achievements/generic.jpg',
    events:  [ events.INVITE_ACCEPTED ],
    conditions: [ conditions. ],
    scoreFunction: 'fiftyPoints',
    isOneTimeAchievement: true,
    shouldShow: true
  },
  {
    slug: 'twentyInvitationsAccepted',
    name: 'Invitation Milestone 2: 20 Invitations accepted',
    description: '20 people have accepted your invitations',
    image:  'img/achievements/generic.jpg',
    events:  [ events.INVITE_ACCEPTED ],
    conditions: [ conditions.InvitationMilestone(20) ],
    scoreFunction: 'onefiftyPoints',
    isOneTimeAchievement: true,
    shouldShow: true
  },
  {
    slug: 'thirtyInvitationsAccepted',
    name: 'Invitation Milestone 3: 30 Invitations accepted',
    description: '30 people have accepted your invitations',
    image:  'img/achievements/generic.jpg',
    events:  [ events.INVITE_ACCEPTED ],
    conditions: [ conditions.InvitationMilestone(30) ],
    scoreFunction: 'fourHundredPoints',
    isOneTimeAchievement: true,
    shouldShow: true
  },
  {
    slug: 'fortyInvitationsAccepted',
    name: 'Invitation Milestone 4: 40 Invitations accepted',
    description: '40 people have accepted your invitations',
    image:  'img/achievements/generic.jpg',
    events:  [ events.INVITE_ACCEPTED ],
    conditions: [ conditions.InvitationMilestone(40) ],
    scoreFunction: 'oneThousandPoints',
    isOneTimeAchievement: true,
    shouldShow: true
  } */
];