/**
 * The InputError object is used to pass error information between the server and the client.
 * Because this object is designed to give feedback on user input, settable error fields will
 * coincide with the inputData object provided + an additional general field for any other general
 * errors that might occur.
 *
 * The concept is fairly straightforward at the moment:
 * - the constructor takes in an array of field names to be 
 * - setting an error for particular field(s) involves creating an object w/ the key set to the field
 *   desired and the value set to the error.  This appends the error to the list of errors pertaining to
 *   this field.
 * - getting an error is as simple as doing a get() on the fieldname.  This returns a list of errors
 *   for that particular field.
 * - currently, there is no "delete" operation for errors (we don't have a strong reason to have such a thing).
 **/

var util = require('./util');

var GENERAL_ERRORS_KEY = 'general';

/**
 * Default constructor for a GTCInputError object.
 **/
var GTCInputError = module.exports = function(inputKeys) {
  this.errors_ = {};
  if (inputKeys && inputKeys instanceof Array) {
    inputKeys.forEach(function (key) {
      if (key) this.errors_[key] = [];
    }, this);
  }
  // The list of general errors will go here.
  this.errors_[GENERAL_ERRORS_KEY]= [];
};

/**
 * Returns true if there are no error messages for any field,
 * false if otherwise.
 **/
GTCInputError.prototype.isEmpty = function() {
  for(var field in this.errors_) {
    if (this.errors_[field].length) return false;
  }
  return true;
};

/**
 * Clear all messages for this object.
 * If a field is not specified, the error messages in the general errors
 * are cleared.
 **/
GTCInputError.prototype.clear = function(field) {
  if (field === undefined) {
    this.errors_[GENERAL_ERRORS_KEY] = [];  
  } else if (field && this.errors_[field] !== undefined) {
    this.errors_[field] = [];
  }
};

/**
 * Add an error message to the corresponding field.
 * If a field is not specified, the error message is added to the list of
 * general errors.
 * Returns true if field exists, false if otherwise.
 **/
GTCInputError.prototype.add = function(field, errorMsg) {
  if (field !== undefined && errorMsg === undefined) {
    // In the case that errorMsg was not specified, the
    // errorMsg is actually the first argument and user wants to
    // add this to the list of general errors.
    var myErrorMsg = field;
    this.errors_[GENERAL_ERRORS_KEY].push(myErrorMsg);
    return true;
  } else if (field && this.errors_[field] !== undefined) {
    this.errors_[field].push(errorMsg);
    return true;
  }

  return false;
};

/**
 * Get a list of the error messages pertaining to the
 * input field.
 * If an input field is not specified, then the error message is added to
 * the list of general errors.
 * Returns a list of the errors added to the input field.
 * If input field does not exist, then list returned will be empty.
 **/
GTCInputError.prototype.get = function(field) {
  if (field === undefined) {
    return this.errors_[GENERAL_ERRORS_KEY];
  } else if (field && this.errors_[field] !== undefined) {
    return this.errors_[field];
  } else {
    return [];
  }
};

/**
 * Get a list of all possible fields.
 **/
GTCInputError.prototype.getFields = function() {
  return Object.keys(this.errors_);
};

/**
 * Copies the GTCInputError to a JSON object.  Only fields with at at least
 * one error message will be included in this object.
 * JSON object will take the following form:
 * { field1: [ errorMsgs ], field2: [ errorMsgs ], GENERAL_ERRORS_KEY: [ errorMsgs ] }
 **/
GTCInputError.prototype.toJSON = function() {
  var result = {};
  for (var key in this.errors_) {
    if (this.errors_[key].length) {
      result[key] = this.errors_[key];
    }
  }

  return result;
};

/**
 * Returns an error object suitable for passing in async and other node standard
 * functions.  That is, if there are no errors, return null, otherwise, return
 * an object.
 **/
GTCInputError.prototype.getErrors = function() {
  var result = this.toJSON();
  return util.isObjectEmpty(result) ? null : result;
};
