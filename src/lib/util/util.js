var configuration = require('../../configuration');

/**
 * Determines if input object argument is empty or not (e.g. an object === {});
 **/
exports.isObjectEmpty = function(obj) {
  for(var i in obj) { 
    if (obj.hasOwnProperty(i)) {
      return false;
    }
  }
  return true;
};

exports.getServerUrl = function() {
  var server = configuration.get('http:server');
  var port = configuration.get('http:port');
  var rootUrl = server;
  if (port != '80') {
    rootUrl += ':' + port;
  }
  // TODO If https, use https
  return 'http://' + rootUrl + '/';
};
