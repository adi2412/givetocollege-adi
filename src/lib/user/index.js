// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var async = require('async'),
    achievements = require('../achievements').achievements,
    logger = require('log4js').getLogger('user');

// Schema imports
var User = require('../models/user'),
    Campaign = require('../models/campaign'),
    UserCampaign = require('../models/userCampaign');
