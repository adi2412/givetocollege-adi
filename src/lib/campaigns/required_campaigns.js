// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var achievements = require('../achievements').achievements;

// Note: Registration automatically picks up slugs that begin with gtc

module.exports = [
  {
    slug: 'gtc_complete_profile',
    name: 'Complete Your Profile Campaign',
    description: 'Complete Your Profile to get Easy Points!',
    active: true,
    achievements: [ 'profileComplete', 'addedOrganization', 'mailingAddressComplete' ]
  },
  {
    slug: 'gtc_invite',
    name: 'Invite Your Friends Campaign',
    description: 'Invite others to get points!',
    active: true,
    achievements: [ 'invitationAccepted', 'tenInvitationsAccepted', 'twentyInvitationsAccepted', 'thirtyInvitationsAccepted', 'fortyInvitationsAccepted' ]
  },
  {
    slug: 'gtc_donate',
    name: 'Donate Campaign',
    description: 'Donate to your favorite colleges to get you and your college serious points!',
    active: true,
    achievements: [ 'donation' ]
  },
  {
    slug: 'gtc_pledge',
    name: 'Pledge Campaign',
    description: 'Pledge to your favorite colleges to get you and your college serious points!',
    active: true,
    achievements: [ 'donation' ]
  },
];
