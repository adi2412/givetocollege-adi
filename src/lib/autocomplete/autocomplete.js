var mongoose = require('mongoose'),
    Organization = require('../models/organization'),
    University = require('../models/university');

exports.getOrganizationSuggestions = function(query, callback) {
  return exports.getSuggestions_({
     field: 'name',
     selectField: 'display_name',
     pattern: query.toLowerCase(),
     model: Organization,
     maxSuggestions: 10}, callback);
};

exports.getUniversitySuggestions = function(query, callback) {
  return exports.getSuggestions_({
     field: 'name',
     selectField: 'display_name',
     pattern: query.toLowerCase(),
     model: University,
     maxSuggestions: 10}, callback);
};

/**
 * Given a term and a mongoose model, executes the callback function 
 * 
 * The options object has the following keys:
 *  query: query to use to search the mongoose model
 *  model: the model to run the query on
 *  maxSuggestions: the maximum number of suggestions to provide.  
 *                  0 (default) means no maximum.
 */

function queryDB(model, field, pattern, selectField, maxSuggestions, callback) {
  var query = model.find({})
                   .regex(field, new RegExp('.*'+pattern+'.*', 'i'))
                   .select(selectField);

  if (maxSuggestions > 0) {
    query.limit(maxSuggestions);
  }

  // Finally exec the query
  return query.exec(callback);
}

exports.getSuggestions_ = function(options, callback) {
  var params = options || {};
  var field = params.field || "";
  var pattern = params.pattern || "";
  var maxSuggestions = params.maxSuggestions || 0;
  var selectField = params.selectField || "";

  // If any of these cases hold, there is no reason to actually make the query.
  // Immediately call the callback function if available and return undefined
  if (!field || !selectField || !params.model || maxSuggestions < 0) {
    if (callback) { callback("Invalid options", []); }
    return undefined;
  }

  return queryDB(params.model, field, pattern, selectField, maxSuggestions, callback);
};
