// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

exports.createExtendedPagingManager = function(Model, searchKey, searchCallback, callback, pageItemNumber) {
  if (pageItemNumber === undefined || pageItemNumber<0) {
    pageItemNumber = 10;
  }
  return function(request, response) {
    var page = request.query.page;
    if ( !page ) {
      page = 0;
    } else {
     page = parseInt(page, pageItemNumber);
     if ( !page || isNaN(page) ) {
       page = 0;
     }
    }
    var search = request.query.search;
    var query = {};
    if (search) {
      queryRegex = new RegExp(".*" + search + ".*", "i");
      query[searchKey] = queryRegex;
    }
    searchCallback(request,query);
    Model.count(query, function(error, count) {
      var numberOfPages = Math.floor( count / pageItemNumber );
      if ( page < 0 || page > numberOfPages ) {
        page = 0;
      }
      var nextPage;
      if (page<numberOfPages) {
        nextPage = page + 1;
      }
      var previousPage = page - 1;
      Model.find(query, {}, { skip: page*pageItemNumber, limit: pageItemNumber }, function(error, collection) {
        callback(response, collection, page, numberOfPages, nextPage, previousPage);
      });
    });
  };
};

exports.createPagingManager = function(Model,key,viewName,title) {
  return function(request, response) {
    var page = request.query.page;
    if ( !page ) {
      page = 0;
    } else {
     page = parseInt(page, 10);
     if ( !page || isNaN(page) ) {
       page = 0;
     }
    }
    var search = request.query.search;
    var query = {};
    if (search) {
      queryRegex = new RegExp(".*" + search + ".*", "i");
      query[key] = queryRegex;
    }
    Model.count(query, function(error, count) {
      var numberOfPages = Math.floor( count / 10 );
      if ( page < 0 || page > numberOfPages ) {
        page = 0;
      }
      var nextPage;
      if (page<numberOfPages) {
        nextPage = page + 1;
      }
      var previousPage = page - 1;
      Model.find(query, {}, { skip: page*10, limit: 10 }, function(error, collection) {
        response.render(viewName,
          { title: title, collection: collection, page: page, numberOfPages: numberOfPages, nextPage: nextPage, previousPage: previousPage});
      });
    });
  };
};
