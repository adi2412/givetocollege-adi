// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var configuration = require('../../configuration')
  , mongoose = require('mongoose')
  , Schema = mongoose.Schema
  ;

var AchievementSchema = new Schema({
  slug: { type: String, unique: true, required: true },
  name: { type: String, required: true },
  description: { type: String, required: true },
  badge: { type: String, required: true },
  events: { type: [ String ], required: true },
  conditions: { type: [ String ], required: true },
  scoreFunction: { type: String, required: true },
  isOneTimeAchievement: { type: Boolean, required: true },
  shouldShow: { type: Boolean, required: true }
});

module.exports = mongoose.model('Achievement', AchievementSchema);