// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    states = require('../../common/states'),
    Validator = require('./validator'),
    Schema = mongoose.Schema;

var CategorySchema = new Schema({
  slug:          { type: String, unique: true },
  name:          { type: String, unique: false },
  description:   { type: String, required: true, default: '' }
});

CategorySchema.index({ slug: 1 });
CategorySchema.index({ name: 1 });

module.exports = mongoose.model('Category', CategorySchema);