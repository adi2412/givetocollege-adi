// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 *
 **/
var NotificationSchema = new Schema({
  userRef: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  title: { type: String, required: true },
  description: { type: String, required: true },
  created: { type: Date, required: true, default: Date.now },
  seen: { type: Boolean, required: true, default: false }
});

module.exports = mongoose.model('Notification', NotificationSchema);

