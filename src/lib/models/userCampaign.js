// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserCampaignSchema = new Schema({
  user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  campaign: { type: Schema.Types.ObjectId, required: true, ref: 'Campaign' },
  startDate: { type: Date, required: true, default: Date.now },
  endDate: { type: Date, required: false }
});

UserCampaignSchema.index({ user: 1, campaign: 1}, { unique: true });

module.exports = mongoose.model('UserCampaign', UserCampaignSchema);
