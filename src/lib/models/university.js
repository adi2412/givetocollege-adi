// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  A university is a group of organizations
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    states = require('../../common/states'),
    Validator = require('./validator'),
    Schema = mongoose.Schema;

var UniversitySchema = new Schema({
  display_name: { type: String, required: true },
  name:       { type: String, required: true, set: lower },
  acronym:    { type: String, required: false },
  domain:     { type: String, required: false, set: lower },
  logo:       { type: String, required: false },
  status:     { type: String, required: true, enum: ['enabled', 'disabled'], default: 'disabled' },
  category:   { type: Schema.Types.ObjectId, required: false, ref: 'Category' }
});

UniversitySchema.index({ display_name: 1 });
UniversitySchema.index({ name: 1 });

function lower(v) {
  return v.toLowerCase();
}

module.exports = mongoose.model('University', UniversitySchema);

