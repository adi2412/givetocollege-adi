// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  A university is a group of organizations
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    states = require('../../common/states'),
    Validator = require('./validator'),
    Schema = mongoose.Schema;

var UniversityOrganizationSchema = new Schema({
  university:   { type: Schema.Types.ObjectId, required: false, ref: 'University' },
  organization:   { type: Schema.Types.ObjectId, required: false, ref: 'Organization' }
});

// TODO create indexes

module.exports = mongoose.model('UniversityOrganization', UniversityOrganizationSchema);

