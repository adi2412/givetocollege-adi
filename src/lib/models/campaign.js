// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CampaignSchema = new Schema({
  slug: { type: String, unique: true, required: true },
  name: { type: String, required: true },
  description: { type: String, required: true },
  active: { type: Boolean, required: true, 'default': false },
  sponsor: { type: Schema.Types.ObjectId, required: false, ref: 'Organization' },
  createdBy: { type: Schema.Types.ObjectId, required: false, ref: 'User' },
  achievements: { type: [ String ], required: true, 'default': [] }
});

module.exports = mongoose.model('Campaign', CampaignSchema);
