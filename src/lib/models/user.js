// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var configuration = require('../../configuration'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    states = require('../../common/states'),
    Validator = require('./validator'),
    roles = require('../../roles'),
    gravatar_util = require('../util/gravatar'),
    util = require('util');

/**
 * A User is a person who has logged onto our site.  If the user has completed his registration,
 * the info field will be defined.
 **/
var UserSchema = new Schema({
  email: { type: String, required: false },
  created: { type: Date, default: Date.now },
  channels: [ { type: Schema.Types.ObjectId, required: true, ref: 'UserLogin' } ],
  user_roles: [
    { type: Schema.Types.ObjectId, required: true, ref: 'Organization' }
    // TODO
    //affiliation: { type: String, enum: [ 'Alumni', 'Parent', 'Student', 'Other' ], required: false },
    //year: { type: Number, required: false }
  ],
  admin_roles: [
    { type: Schema.Types.ObjectId, required: true, ref: 'Organization' }
  ],
  superadmin: { type: Boolean, required: false },
  first: { type: String, required: true },
  last: { type: String, required: true },
  dob: { type: Date, required: false },
  mailing_address: {
    street1: { type: String, required: false },
    street2: { type: String, required: false },
    city: { type: String, required: false },
    state: { type: String, required: false },
    zip: { type: Number, required: false }
  },
  gravatar: { type: Boolean, required: true, default: true },
  profileImages: { type: [ String ], required: false, default: [] },
  points: { type: Number, required: true, default: 0 }
});

UserSchema.virtual("roleLevel")
  .get(function() {
    return roles.getRoleLevel(this);
  });

UserSchema.virtual("fullName")
  .get(function() {
    return util.format("%s %s", this.first, this.last);
  })
  .set(function(fullName) {
    if (!fullName) { return; }

    var names = fullName.split(/\s+/);
    if (names.length > 0) {
      this.first = names[0];
    } 
    if (names.length >= 2) {
      this.last = names.slice(1).join(' ');
    }
  });

UserSchema.virtual('roles')
  .get(function() {
    return {
      user: this.user_roles,
      admin: this.admin_roles,
      superadmin: this.superadmin
    };
  });

UserSchema.virtual('roles.user')
  .get(function() {
    return this.user_roles;
  })
  .set(function(roles) {
    this.user_roles = roles;
  });

UserSchema.virtual('roles.admin')
  .get(function() {
    return this.admin_roles;
  })
  .set(function(roles) {
    this.admin_roles = roles;
  });

UserSchema.virtual('roles.superadmin')
  .get(function() {
    return this.superadmin;
  })
  .set(function(superadmin) {
    this.superadmin = superadmin;
  });
 
UserSchema.pre('save', function(next) {
  if (!Validator.addressValidator(this.mailing_address)) {
    return next(new Error('Incomplete or invalid mailing address'));
  }
  next();
});

UserSchema.virtual('profileImage')
  .get(function() {
    if (this.gravatar) {
      return gravatar_util.getHash(this.email);
    } else if (this.profileImages.length === 0) {
      return "/img/uploads/profile/default.gif";
    } else {
      return configuration.get("profile:path_root") + "/" + this.profileImages[this.profileImages.length-1];
    }
  });

module.exports = mongoose.model('User', UserSchema);

