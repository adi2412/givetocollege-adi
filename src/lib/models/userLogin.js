// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Validator = require('./validator'),
    roles = require ("../../roles");

var UserLoginSchema = new Schema({
  userKey: { type: String, unique: true },
  passwordHash: { type: String, required: false },
  userRef: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('UserLogin', UserLoginSchema);
