// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/
var states = require('../../common/states');

var validate = {
  "user": function(user) {
    if (!user.universities) {
      return false;
    }
    return true;
  },
  "admin": function(admin) {
    if (!admin.universities) {
      return false;
    }
    return true;
  },
  "superadmin": function(superadmin) {
    if (superadmin.universities) {
      return false;
    }
    return true;
  }
};

exports.roleValidator = function(value) {
  if ( !(value instanceof Object) ) {
    return false;
  }

  var valid = true;
  Object.keys(value).forEach(function(role) {
    var roleInstance = value[role];
    if( roleInstance instanceof Object ) {
      // Basically checking if the role exists and has a property within
      // the validate object.
      if (role &&
          Object.keys(validate).indexOf(role) >= 0) {
        valid &= validate[role](roleInstance);
      } else {
        valid = false;
      }
    } else {
      valid = false;
    }
  });
  return valid;
};

exports.stateValidator = function stateValidator(value) {
  return value === undefined || (value in states.stateCodeToName);
};

exports.emailValidator = function emailValidator(value) {
  return (/^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,4}$/).test(value);
};

exports.addressValidator = function addressValidator(value) {
    // Basically, mailing_address is an all or nothing thing
    // TODO add zip validation, add city validation
    return value === undefined ||
      (value.street1 && value.city && value.state && value.zip) ||
      (!value.street1 && !value.street2 && !value.city && !value.city && !value.zip);
};

exports.EINValidator = function EINValidator(value) {
  return value === undefined || (/^[0-9][0-9]-[0-9]{7}/).test(value);
};
