// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  An organization is... an organization.  This is the main entity that
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    Validator = require('./validator'),
    Schema = mongoose.Schema;

var OrganizationSchema = new Schema({
  display_name:     { type: String, required: true },
  name:             { type: String, required: true, set: lower },
  acronym:          { type: String, required: false },
  domain:           { type: String, required: false, set: lower },
  address1:         { type: String, required: false },
  address2:         { type: String, required: false },
  city:             { type: String, required: false },
  state:            { type: String, validate: Validator.stateValidator, required: false },
  zip:              { type: String, required: false },
  logo:             { type: String, required: false },
  taxid:            { type: String, validate: Validator.EINValidator, required: false },
  twitterhandle:    { type: String, required: false },
  facebookpage:     { type: String, required: false },
  webpage:          { type: String, required: false },
  isEnabled:        { type: Boolean, required: true, 'default': true},
  isPaymentEnabled: { type: Boolean, required: true, 'default': false},
  type:             { type: String, 'enum': ['General', 'Medical School', 'Business School', 'Law School', 'Engineering School'] }
});

OrganizationSchema.pre('validate', function (next) {
  if(this.display_name !== undefined) {
    this.name = this.display_name.toLowerCase();
  }
  next();
});

OrganizationSchema.index({ display_name: 1 });
OrganizationSchema.index({ name: 1 });

function lower(v) {
  return v.toLowerCase();
}

module.exports = mongoose.model('Organization', OrganizationSchema);
