// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose');
var Schema = mongoose.Schema ,
    User = Schema.User;

var RelationshipSchema = new Schema({
  userFrom: User ,
  userTo:   User ,
});
