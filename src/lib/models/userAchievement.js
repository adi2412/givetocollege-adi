// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UserAchievementSchema = new Schema({
  user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  achievement: { type: Schema.Types.ObjectId, required: true, ref: 'Achievement' },
  progress: { type: Number, min: 0, max: 100, required: true, default: 0 },
  dateCompleted: { type: Date, required: false }
});

UserAchievementSchema.index({ "user": 1, "achievement": 1 }, { unique: true });

module.exports = mongoose.model('UserAchievement', UserAchievementSchema);
