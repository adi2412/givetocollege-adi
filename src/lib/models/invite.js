// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var mongoose = require('mongoose'),
    dateUtils = require('../../common/utils/date');
    Schema = mongoose.Schema;

var inviteStatus = [ 'sent', 'accepted' ];
var inviteSources = [ 'email', 'facebook', 'linkedin', 'gmail', 'twitter' ];
var InviteSchema = new Schema({
  user:             { type: Schema.Types.ObjectId, required: true },
  recipientId:      { type: String, required: true },
  recipientName:    { type: String, required: false },
  source:           { type: String, enum: inviteSources, required: true },
  dateSent:         { type: Date, default: Date.now() },
  status:           { type: String, enum: inviteStatus, default: 'sent' }
});

InviteSchema.index({ user: 1, recipientId: 1 }, { unique: true });

InviteSchema.virtual('formattedDateSent').get(function() {
  return dateUtils.formatDate(this.dateSent);
});

module.exports = mongoose.model('Invite', InviteSchema);
