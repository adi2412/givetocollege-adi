var nodeunit = require('nodeunit'),
    async = require('async'),
    bcrypt = require('bcrypt'),
    mongoose = require('mongoose'),
    Validator = require('../lib/models/validator'),
    login = require('../routes/http/login'),
    userRoute = require('../routes/http/user');

var User = require('../lib/models/user'),
    UserLogin = require('../lib/models/userLogin'),
    Organization = require('../lib/models/organization');

exports["Test User Validation"] = nodeunit.testCase({
  setUp: function(callback) {
    mongoose.connect('mongodb://localhost/test', function() {
      new Organization({
        name: "testu",
        acronym: "tu",
        domain: "testu.edu",
        status: "enabled"
      }, { id: true }).save(callback);
    });
  },
  tearDown: function(callback) {
    mongoose.connection.model('Organization').remove(function() {
      mongoose.connection.close();
      return callback();
    });
  },
  'Empty name is bad': function(test) {
    async.series([
      function(callback) {
        // Having NOTHING should fail, of course
        var user = { };
        userRoute.sanitizeAndValidateUser(user, function(err, result) {
          test.ok(err, "Expecting errors for an empty user");
          return callback();
        });
      },
      function(callback) {
        // Just an empty fullname should fail
        var user = { fullName: "" };
        userRoute.sanitizeAndValidateUser(user, function(err, result) {
          test.ok(err, "Expecting errors for an empty fullName");
          return callback();
        });
      },
      function(callback) {
      // Single word fullnames should fail
        var user = { fullName: "firstnameonly" };
        userRoute.sanitizeAndValidateUser(user, function(err, result) {
          test.ok(err, "Expecting errors for a single word fullName");
          return callback();
        });
      },
      function(callback) {
        // Multi-word fullnames should not fail
        var user = { fullName: "firstname lastname" };
        userRoute.sanitizeAndValidateUser(user, function(err, result) {
          test.ok(!err, "NOT expecting errors for a multi-word fullName");
          return callback();
        });
      }
    ], test.done);
  },
  'Empty or invalid email is bad': function(test) {
    async.series([
      function(callback) {
        // Just an empty email should not fail
        var user = { id: '123', fullName: "test test", email: "" };
        userRoute.sanitizeAndValidateUser(user, function(err, result) {
          test.ok(!err, "NOT expecting errors for empty email");
          return callback();
        });
      },
      function(callback) {
        // Invalid email should fail
        var user = { id: '123', fullName: "test test", email: "invalidemail" };
        userRoute.sanitizeAndValidateUser(user, function(err, result) {
          test.ok(err, "Expecting errors for invalid email");
          return callback();
        });
      },
      function(callback) {
        // Valid email should not fail
        var user = { id: '123', fullName: "test test", email: "valid@valid.com" };
        userRoute.sanitizeAndValidateUser(user, function(err, result) {
          test.ok(!err, "NOT expecting errors for valid email");
          return callback();
        });
      }
    ], test.done);
  },
  'Invalid schools are ignored': function(test) {
    // Invalid school names should fail
    var user = { fullName: "test test", school_name: 'invalid' };
    userRoute.sanitizeAndValidateUser(user, function(err, result) {
      test.ok(!err, "Not expecting errors for invalid school");
      return test.done();
    });
  },
  'Empty set of roles should still return': function(test) {
    // Empty roles should be ok
    var user = { id: '123', fullName: "test test", user_roles: [ ] };
    userRoute.sanitizeAndValidateUser(user, function(err, result) {
      test.ok(!err, "NOT expecting errors for invalid role");
      return test.done();
    });
  },
  'Valid user check using user_name': function(test) {
    var user = { id: '123', fullName: "test test", school_name: 'TESTU   ' };
    userRoute.sanitizeAndValidateUser(user, function(err, user) {
      test.ok(!err, "NOT expecting errors for valid user");
      test.equals(1, user.user_roles.length, "Expecting at least one user role");
      return test.done();
    });
  },
  'Valid user check': function(test) {
    Organization.findOne({}, function(err, organization) {
      var user = { id: '123', fullName: "test test", user_roles: [ ""+organization._id ] };
      userRoute.sanitizeAndValidateUser(user, function(err) {
        test.ok(!err, "NOT expecting errors for valid user");
        return test.done();
      });
    });
  }
});
