// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var async = require('async'),
    mongoose = require('mongoose'),
    nodeunit = require('nodeunit');

// Local imports
var userLib = require('../../../lib/user');

// Schema imports
var User = require('../../../lib/models/user'),
    Campaign = require('../../../lib/models/campaign'),
    UserCampaign = require('../../../lib/models/userCampaign');

/*
module.exports['Test Adding Campaign to User'] = nodeunit.testCase({
  setUp: function(callback) {
    mongoose.connect('mongodb://localhost/test', function() {
    async.parallel([
        function(callback) {
          User.create({
              email: "test@test.edu",
              first: "first",
              last: "last"
          }, callback);
        },
        function(callback) {
          Campaign.create({
            slug: "testcampaign",
            name: "Test Campaign",
            description: "This is a test campaign",
            widget: "test",
            active: true
          }, callback);
        }
      ], callback);
    });
  },
  tearDown: function(callback) {
    async.series([
      function(callback) { UserCampaign.remove({}, callback); },
      function(callback) { User.remove({}, callback); },
      function(callback) { Campaign.remove({}, callback); },
      function(callback) { mongoose.connection.close(callback); }
    ], callback);
  },

  'simple test': function(test) {
    test.expect(3);
    var user;
    var campaign;
    async.series([
      function(callback) {
        User.findOne({}, function(err, result) {
          user = result;
          return callback(err);
        });
      },
      function(callback) {
        Campaign.findOne({}, function(err, result) {
          campaign = result;
          return callback(err);
        });
      },
      function(callback) {
        userLib.addCampaignToUser(user, campaign, function(err) {
          test.ok(!err);
          UserCampaign.findOne({ user: user._id, campaign: campaign._id }, function(err, result) {
            test.ok(!err);
            test.ok(result);
            return callback(err);
          });
        });
      }
    ], test.done);
  }
});
*/
