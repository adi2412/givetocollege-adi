var async = require('async'),
    nodeunit = require('nodeunit');

var autocomplete = require('../../../lib/autocomplete/autocomplete');

module.exports['Test Get Suggestions'] = nodeunit.testCase({
  'undefined input arguments': function(test) {
    test.expect(1);
    test.ok(!autocomplete.getSuggestions_());
    test.done();
  },

  'empty input arguments': function(test) {
    test.expect(1);
    test.equal(undefined, autocomplete.getSuggestions_({}));
    test.done();
  },

  'no field argument': function(test) {
    test.expect(1);
    autocomplete.getSuggestions_(
      {
        model: 'something',
        maxSuggestions: 2
      },
      function (err, doc) {
        test.ok(err);
        test.done();
      }
    );
  },

  'negative maxSuggestions': function(test) {
    test.expect(1);
    autocomplete.getSuggestions_(
      {
        model: 'something',
        field: 'field',
        maxSuggestions: -1
      },
      function (err, doc) {
        test.ok(err);
        test.done();
      }
    );
  },

  'empty model input argument': function(test) {
    test.expect(1);
    autocomplete.getSuggestions_(
      {query: {}}, 
      function (err, doc) {
        test.ok(err);
        test.done();
      }
    );
  }

});

