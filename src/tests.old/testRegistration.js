var nodeunit = require('nodeunit'),
    register = require('../routes/http/user'),
    mongoose = require('mongoose'),
    async = require('async'),
    configuration = require('../configuration');

configuration.configureApplication();

var User = require('../lib/models/user'),
  UserLogin = require('../lib/models/userLogin'),
  Organization = require('../lib/models/organization');

exports['Input Validation Test'] = nodeunit.testCase({
  setUp: function(callback) {
    mongoose.set('debug', true);
    mongoose.connect('mongodb://localhost/test', callback);
  },

  tearDown: function(callback) {
    async.series([
      function(callback) { UserLogin.remove(callback); },
      function(callback) { User.remove(callback); },
      function(callback) { mongoose.connection.close(); return callback(); }
    ], callback);
  },
  'undefined passwords with email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: "test@test.edu",
      fullName: "first last",
      password: undefined, 
      register_type: 'email'
    };
    register.createUser(input, function(err) {
      test.ok(err);
      test.equals(configuration.get('err_user:registration:emptyPassword'), err.password[0]);
      return test.done();
    });
  },
  'undefined passwords with non-email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: "test@test.edu",
      fullName: "first last",
      password: undefined, 
      register_type: 'facebook'
    };
    register.createUser(input, function(err, result) {
      test.ok(!err);
      test.ok(result);
      return test.done();
    });
  },
  'valid passwords': function(test) {
    var input = { 
      id: "test@test.edu",
      email: "test@test.edu",
      fullName: "first last",
      password: 'password123', 
      register_type: 'email'
    };
    register.createUser(input, function(err, userLogin) {
      test.ok(!err);
      test.ok(userLogin.passwordHash);
      return test.done();
    });
  },
  'valid passwords with non-email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: "test@test.edu",
      fullName: "first last",
      password: 'password123', 
      register_type: 'facebook'
    };
    register.createUser(input, function(err, userLogin) {
      test.ok(!err);
      test.ok(!userLogin.passwordHash);
      return test.done();
    });
  },
  'empty email with email account': function(test) {
    var input = { 
      id: "test@test.edu",
      fullName: "first last",
      register_type: 'email' 
    };
    register.createUser(input, function(err, result) {
      test.ok(err.email[0]);
      return test.done();
    });
  },
  'empty email with non-email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: undefined, 
      fullName: "first last",
      register_type: 'facebook'
    };
    register.createUser(input, function(err, result) {
      test.ok(!err);
      test.ok(result);
      return test.done();
    });
  },
  'invalid email with email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: 'this_email_is_invalid', 
      fullName: "first last",
      register_type: 'email'
    };
    register.createUser(input, function(err) {
      test.ok(err.email[0]);
      return test.done();
    });
  },
  'invalid email with non-email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: 'this_email_is_invalid', 
      fullName: "first last",
      register_type: 'facebook'
    };
    register.createUser(input, function(err) {
      test.ok(err.email[0]);
      return test.done();
    });
  },
  'valid email with email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: 'test@test.edu', 
      fullName: "first last",
      password: 'password123', 
      register_type: 'email'
    };
    register.createUser(input, function(err, userLogin) {
      test.ok(!err);
      User.findById(userLogin.userRef, function(err, user) {
        test.equals(input.email, user.email);
        return test.done();
      });
    });
  },
  'valid email with non-email account': function(test) {
    var input = { 
      id: "test@test.edu",
      email: 'test@test.edu', 
      fullName: "first last",
      register_type: 'facebook'
    };
    register.createUser(input, function(err, userLogin) {
      test.ok(!err);
      User.findById(userLogin.userRef, function(err, user) {
        test.equals(input.email, user.email);
        return test.done();
      });
    });
  }
});

exports['Create User Test'] = nodeunit.testCase({
  setUp: function(done) {
    // Define and export Test Schema for test use.
    async.series({
      closeOpenConnection: function(callback) { mongoose.connection.close(callback); },
      connectToMongoose: function(callback) { mongoose.connect('mongodb://localhost/test', callback); },
      clearUserLogins: function(callback) { UserLogin.remove(callback); },
      clearUsers: function(callback) { User.remove(callback); },
      clearOrganizations: function(callback) { Organization.remove(callback); },
      createOrganizations: function(callback) {
        return Organization.create({
          display_name: "University of Test",
          name: "University of Test",
          acronym: "UoT",
          domain: "test.edu",
          logo: "logo",
          status: "enabled",
          points: 0
        }, callback);
      }
    }, done);
  },
  tearDown: function(done) {
    async.parallel({
      removeUserLogin: function(callback) { UserLogin.remove(callback); },
      removeUser: function(callback) { User.remove(callback); },
      removeOrganization: function(callback) { Organization.remove(callback); }
    }, function () {
      return mongoose.connection.close(done);
    });
  },
  'crazy last name check': function(test) {
    var User = mongoose.connection.model('User');
    var UserLogin = mongoose.connection.model('UserLogin');
    var Organization = mongoose.connection.model('Organization');
    var input = {
      id: '123',
      fullName: 'first this is the last name',
      email: 'crazy_last_name@test.com',
      register_type: 'facebook'
    };
    register.createUser(input, function(err, userData) {
      test.ok(!err, 'Expecting no errors');
      test.ok(userData, 'Expecting user data');
      console.log(JSON.stringify(userData));
      UserLogin.findOne({ userKey: 'facebook|123' })
        .populate('userRef')
        .exec(function (err, userLogin) {
          var user = userLogin.userRef;
          console.log(JSON.stringify(userLogin));
          test.ok(!err, 'Expecting no errors');
          test.ok(user, 'Expecting non-null user field');
          test.equals('first', user.first);
          test.equals('this is the last name', user.last);
          return test.done();
        });
    });
  },
  'valid email registration': function(test) {
    var input = {
      id: 'email2@test.com',
      email: 'email2@test.com',
      fullName: 'first last',
      password: '123',
      school_name: 'University of Test',
      register_type: 'email'
    };
    register.createUser(input, function(err, userLogin) {
      test.ok(!err, 'Expecting no errors');
      test.ok(userLogin, 'Expecting user data');
      UserLogin.findOne({ userKey: 'email|email2@test.com' })
        .populate('userRef')
        .exec(function (err, userLogin) {
          var user = userLogin.userRef;
          test.ok(!err, 'Expecting no errors');
          test.ok(user, 'Expecting non-null user field');
          test.equals('first', user.first);
          test.equals('last', user.last);
          test.ok(userLogin.passwordHash, 'Expecting password hash to be filled');
          test.equals(1, user.user_roles.length, 'Expecting 1 role');
          test.equals(user.email, 'email2@test.com', 'Expecting email');
          Organization.findById(user.user_roles[0], function(err, organization) {
            test.ok(!err, 'Not expecting any errors');
            test.ok(organization, 'Expecting a single organization');
            test.equals('university of test', organization.name, 'Expecting organization to be populated');
            return test.done();
          });
        });
    });
  },
  'valid facebook registration': function(test) {
    var input = {
      id: '456',
      fullName: 'first last',
      email: 'facebook@test.com',
      register_type: 'facebook'
    };
    register.createUser(input, function(err, userData) {
      test.ok(!err, 'Expecting no errors');
      test.ok(userData, 'Expecting user data');
      UserLogin.findOne({ userKey: 'facebook|456' })
        .populate('userRef')
        .exec(function (err, userLogin) {
          console.log(userLogin);
          var user = userLogin.userRef;
          test.ok(!err, 'Expecting no errors');
          test.ok(user, 'Expecting non-null user field');
          test.equals('first', user.first);
          test.equals('last', user.last);
          test.ok(!user.passwordHash, 'Expecting password hash to be empty');
          test.equals(user.email, 'facebook@test.com', 'Expecting email');
          return test.done();
        });
    });
  }
});
