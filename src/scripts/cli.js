/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var parser = require('nomnom');
var findit = require('findit');
var mongoose = require('mongoose'),
    configuration = require('../configuration');
var when = require('when');

configuration.configureApplication(__dirname + '/../config');
var connection = 'mongodb://' + configuration.get('mongodb:server') + '/' + configuration.get('mongodb:db_name');
var db = mongoose.connect(connection);

// Creation of the promise which needs to be resolved 
var defer = when.defer();

// Register the commands.
var commandsDirectory = __dirname + '/commands';
var fileList = findit.sync(commandsDirectory);
fileList.forEach(function(file) {
  var Command = require(file);
  var command = new Command(db, defer);
  parser
    .command(command.getCommand())
    .options(command.getOptions())
    .callback(command.getCallback())
    .help(command.getHelp());
});

process.nextTick(function() { parser.parse(); });

process.nextTick(function() { defer.promise.ensure(function() { mongoose.disconnect(); }); });
