// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var Category = require ('../../lib/models/category');

function Command(db, defer) {
  this.db = db;
  this.defer = defer;

  this.getCommand = function() {
    return "addcategory";
  };

  this.getHelp = function() {
    return "Add a category to the database";
  };

  this.getOptions = function() {
    return {
      name:        { abbr: "n",
                     required: true,
                     help: "Category Name" },
      description: { abbr: "d",
                     required: true,
                     help: "Category description" }
    };
  };

  this.getCallback = function() {
    return function(opts) {
      var categoryData = { name: opts.name, description: opts.description };

      var category = new Category(categoryData);
      category.save(function(error) {
        if(error) {
          console.log("Error: " + error);
          defer.reject();
        } else {
          console.log("Category saved successfully");
          defer.resolve();
        }
      });
    };
  };
}

module.exports = Command;
