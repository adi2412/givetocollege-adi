// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var User = require("../../lib/models/user");

function Command(db, defer) {
  this.db = db;
  this.defer = defer;

  this.getCommand = function() {
    return "finduser";
  };

  this.getOptions = function() {
    return {
      email:      { abbr: "e",
                    help: "User email" }
    };
  };

  this.getCallback = function() {
    return function(opts) {
      var query = {};
      if (opts.email) {
        var queryRegex = new RegExp(".*" + opts.email + ".*", "i");
        query = { "email": queryRegex };
      }

      User.find(query, function(error, users) {
        users.forEach(function(user,index,array) {
          console.log(index + ": " + user);
        });
        defer.resolve();
      });
    };
  };

  this.getHelp = function() {
    return "Find users in the database";
  };
}

module.exports = Command;
