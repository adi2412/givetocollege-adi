// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var User = require ('../../lib/models/user');
var UserLogin = require('../../lib/models/userLogin');
var bcrypt = require ('bcrypt');

function Command(db, defer) {
  this.db = db;
  this.defer = defer;

  this.getCommand = function() {
    return 'adduser';
  };

  this.getHelp = function() {
    return 'Add a user in the database';
  };

  this.getOptions = function() {
    return {
      email:      { abbr: 'e',
                    required: true,
                    help: 'User email' },
      password:   { abbr: 'p',
                    required: true,
                    type: 'string',
                    help: 'Password' },
      first:      { abbr: 'f',
                    required: true,
                    help: 'First Name' },
      last:       { abbr: 'l',
                    required: true,
                    help: 'Last Name' },
      role:       { abbr: 'r',
                    required: true,
                    help: 'User role' },
      organization: { abbr: 'u',
                    required: false,
                    help: 'Organization' }
    };
  };

  this.getCallback = function() {
    return function(opts) {
      console.log(opts);
      bcrypt.hash(opts.password, 8, function(err, hash) {
        if(err) {
          console.log('Error while computing the hash');
          console.log(err);
          defer.reject();
        }
        var userData = { email: opts.email, passwordHash: hash, first: opts.first, last: opts.last, user_roles: [], admin_roles: [] };

        if (opts.role=='superadmin') {
          userData.superadmin = true;
        } else if (opts.role=='admin') {
          userData.admin_roles.push(opts.organization);
        } else if (opts.role=='user') {
          userData.user_roles.push(opts.organization);
        }

        console.log('Adding user: ' + userData);

        var user = new User(userData);
        user.save(function(error) {
          if(error) {
            console.log('Error:');
            console.log(error);
            defer.reject();
          } else {
            console.log('User saved successfully');
            var userLogin = new UserLogin({ userKey: 'email|' + opts.email, userRef: user._id, passwordHash: hash });
            userLogin.save(function(error) {
              if(error) {
                console.log('Error:');
                console.log(error);
                defer.reject();
              } else {
                console.log('UserInfo saved successfully');
                defer.resolve();
              }
            });
          }
        });
      });
    };
  };
}

module.exports = Command;
