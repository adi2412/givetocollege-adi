var Category = require('../../lib/models/category'),
    University = require('../../lib/models/university'),
    Organization = require('../../lib/models/organization'),
    UniversityOrganization = require('../../lib/models/universityOrganization'),
    mongoose = require('mongoose'),
    states = require('../../common/states'),
    fs = require('fs'),
    csv = require('csv'),
    when = require('when'),
    logger = require('log4js').getLogger('import_universities');

function Command(db, defer) {
  this.db = db;
  this.defer = defer;

  this.getCommand = function() {
    return 'import_universities';
  };

  this.getHelp = function() {
    return 'Import universities from a csv file';
  };

  this.getOptions = function() {
    return {
      file: {
        abbr: 'f',
        required: true,
        help: 'university file'
      }
    };
  };

  this.getCallback = function() {
    return function(opts) {
      var total_saved = 0;
      /** A promiseArray object stores all promises for saving the university.  
        * Once a university is saved, its promise gets resolved.
        * when.all(promiseArray) returns a promise which is resolved once all of promiseArray promises 
        * get resolved.
        */
      var uniPromises = [];
      var categories = {};

      csv()
        .from.stream(fs.createReadStream(opts.file))
        .on('record', function(data, index) {
          var state = states.stateNameToCode[data[1]];
          if( state === undefined ) {
            logger.error("State " + data[1] + " not found for " + data[0]);
            return;
          }

          if (data[5] && !categories[data[5]]) {
            var slug = data[5].toLowerCase().replace(/ /g, '_');
            categories[data[5]] = new Category({ slug: slug, name: data[5] });
          }

          // Create the overarching university model
          logger.trace('Creating university: ' + data[0]);
          var university = new University({ display_name: data[0], name: data[0], state: state });
          if (categories[data[5]]) {
            university.category = categories[data[5]];
          }
          uniPromises.push(defer.promise);
          return university.save(function(err, university) {
            if (err) {
              logger.error('An error occurred while creating university %s: %s', data[0], err);
              defer.resolve('couldn\'t save university: '+data[0]);
              return;
            }

          total_saved++;
          logger.debug('Creating organizations for university: ' + JSON.stringify(data[0]));
          var organizations = [];
          var orgPromises = [];
          organizations.push({display_name: data[0], name: data[0], state: state, type: 'General' });

          if (data[8]) {
            organizations.push({display_name: data[8], name: data[8], state: state, type: 'Medical School' });
          }

          if (data[9]) {
            organizations.push({display_name: data[9], name: data[9], state: state, type: 'Law School' });
          }

          if (data[10]) {
            organizations.push({display_name: data[10], name: data[10], state: state, type: 'Business School' });
          }

          if (data[11]) {
            organizations.push({display_name: data[11], name: data[11], state: state, type: 'Business School' });
          }

          logger.trace('Found %d organizations for %s', organizations.length, university.display_name);
          organizations.forEach(function(org) {
            logger.trace('Saving organization: ' + JSON.stringify(org));
            var organization = new Organization(org);
            var def = when.defer();
            orgPromises.push(def.promise);
            organization.save(function(err) {
              def.resolve('saved organization: ' + JSON.stringify(org));
              if (err) {
                logger.error('An error occurred while creating organization %s: %s', JSON.stringify(organization), err);
                return callback(err);
              }
              UniversityOrganization.create({
                university: university,
                organization: organization
              }, function(err) {
                if (err) {
                  logger.error('An error occurred while linking organization: ', err);
                }
              });
            });
          });
          defer.resolve(when.all(orgPromises));
        });
      })
      .on('end',function(count){
        logger.info('Number of lines: '+count);
        logger.debug('Saving categories...');
        var catPromises = [];
        Object.keys(categories).map(function(x) {
          var def = when.defer();
          catPromises.push(def.promise);
          categories[x].save(function(err, result) {
            def.resolve();
            if (err) {
              logger.warn(err);
            }
          });
        });
        uniPromises.push(when.all(catPromises));
        when(when.all(uniPromises), function() {
          logger.trace('The work is done! =)');
          logger.info('Number of saved universities = ' + total_saved);
          mongoose.connection.close();
        }, function(reason) {
          logger.warn('Problem in some promise: ' + reason);
        });
      })
      .on('error',function(error){
        logger.error(error.message);
      });
    };
  };
}

module.exports = Command;
