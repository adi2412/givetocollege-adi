// vim:ft=javascript:ts=2:sw=2:expandtab:autoindent:
/*
  Copyright (c) 2012 All Right Reserved Flocklabs, http://flocklabs.com
*/

var nodemailer = require("nodemailer"),
    jade = require('jade'),
    fs = require('fs'),
    config = require("./configuration"),
    util = require("util"),
    emailQueue = require("./queue");


var smtpTransport;

var sendEmail = function(from, to, subject, text, html) {
  var email =
    { from: from,
      to: to,
      subject: subject,
      text: text,
      html: html };

  var message = JSON.stringify( email );
  emailQueue.publishEmail(message);
};

exports.sendEmailTemplate = function(from, to, subject, context, template) {
  try {
    var text = getTemplateText(context, template);
    var html = getTemplateHtml(context, template);
    sendEmail(from, to, subject, text, html);
  } catch(error) {
    console.log("Error while sending email: " + error);
  }
};

var getTemplateText = function(context, template) {
  var txtTemplateFile = __dirname + '/config/templates/' + template + '.txt.jade';
  var text = getTemplate(context, txtTemplateFile);
  return text;
};

var getTemplateHtml = function(context, template) {
  var htmlTemplateFile = __dirname + '/config/templates/' + template + '.html.jade';
  var html = getTemplate(context, htmlTemplateFile);
  return html;
};

var getTemplate = function(context,templateFilename) {
  var text;
  if (fs.existsSync(templateFilename)) {
    var jadeTemplate = jade.compile(fs.readFileSync(templateFilename,'utf8'));
    text = jadeTemplate(context);
  } else {
    throw new Error('No template in ' + templateFilename);
  }
  return text;
};

exports.sendEmail = sendEmail;

exports.sendMailToServer = function(email) {
  if (! smtpTransport) {
    smtpTransport = nodemailer.createTransport(config.get("email:protocol"), {
      host: config.get("email:host"),
      secureConnection: config.get("email:secureConnection"),
      port: config.get("email:port"),
      auth: {
           user: config.get("email:user"),
           pass: config.get("email:password")
      }
    });
  }

  smtpTransport.sendMail(email,function(error, response) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent, response: " + response.message);
    }
  });
};

exports.closeTransport = function() {
  if(smtpTransport) {
    smtpTransport.close();
  }
};
