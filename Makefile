REPORTER = dot
REPORTER_NODEUNIT = junit

all: install_dependencies install_server

clean: clean_reports
	rm -rf ./lib ./lib_tests ./lib_instrumented ./packages

clean_reports:
	rm -rf ./reports

install_server:
	mkdir -p ./lib
	rsync -r --exclude=.svn ./src/ ./lib

install_dependencies: install_server
	cd ./lib && npm install -d

install_dependencies_instrumented:
	cd ./lib_instrumented && npm install -d

run_server:
	cd ./lib
	nodemon app.js

TIMESTAMP:=$(shell date +%Y%m%d-%H%M%S)
PACKAGE:=gtc-$(TIMESTAMP).tar.gz
package_lib:
	mkdir -p ./packages
	tar -czvf packages/$(PACKAGE) ./lib/

package: clean install_server install_dependencies package_lib

install_tests:
	rsync -r --exclude=.svn ./tests/ ./lib_tests
	rsync -r --exclude=.svn ./src/config ./lib_tests/acceptance
	cd ./lib_tests/acceptance/config && \
	    cp overrides/configs/${CONF}.conf ./override.conf
	rsync ./src/configuration.js ./lib_tests/acceptance/configuration.js
	rsync -r ./src/messages ./lib_tests/acceptance

install_tests_dependencies:
	cd lib_tests && npm install -d

unit_test:
	cd src && nodeunit tests

OLD_UNIT_TESTS:=$(shell find lib/tests.old -iname 'test*.js')
tests: create_reports_directory
	mkdir -p ./reports/nodeunit
	./lib/node_modules/nodeunit/bin/nodeunit \
	    --reporter $(REPORTER_NODEUNIT) \
	    --output ./reports/nodeunit $(OLD_UNIT_TESTS)

new_tests: install_server
	cd ./lib && grunt test

unittests_jenkins:
	mkdir -p reports/unittests
	cd ./lib && grunt --no-color unittests_jenkins | egrep -v "^(Running|Done|Warning|Aborted)" 1> ../reports/unittests/tests.xml

acceptance_tests:
	./lib_tests/node_modules/.bin/mocha \
		--timeout 10000 \
		--reporter $(REPORTER) \
		--bail \
		lib_tests/acceptance

cover:
	rm -rf ./lib_instrumented
	/usr/local/bin/jscoverage ./src ./lib_instrumented
	cd ./lib_instrumented/config && cp overrides/configs/${CONF}.conf ./override.conf

create_cover_reports: create_reports_directory
	mkdir -p reports/cover
	./scripts/node-jscoverage-executer/node_modules/whiskey/bin/whiskey --coverage-dir ./reports/cover --coverage-files `echo ./scripts/node-jscoverage-executer/cover/[0-9]*.json)` --coverage-reporter html

create_reports_directory:
	mkdir -p ./reports

grunt:
	cd ./lib && grunt

docs: create_reports_directory
	groc --no-github -o reports/docs -e "src/public/**/*.js" -i src/app.js "src/**/*.js"

jslint: create_reports_directory
	mkdir -p ./reports/jslint
	find ./src -wholename "*/node_modules/*" -prune -o -wholename "*/public/*" -prune -o -name "*.js" -print0 | xargs -0 jshint --checkstyle-reporter > ./reports/jslint/jslint_src.xml || true
	find ./tests -wholename "*/node_modules/*" -prune -o -wholename "*/public/*" -prune -o -name "*.js" -print0 | xargs -0 jshint --checkstyle-reporter > ./reports/jslint/jslint_tests.xml || true

CONFIG_DIR:=./lib/config
override_config:
	cp $(CONFIG_DIR)/overrides/configs/$(CONF).conf $(CONFIG_DIR)/override.conf
ifeq ($(wildcard $(CONFIG_DIR)/overrides/log4js/$(CONF).json),)
	@echo "No log4js config found, no override for log4js."
else
	cp $(CONFIG_DIR)/overrides/log4js/$(CONF).json $(CONFIG_DIR)/log4js_configuration.json
endif

production_deploy: clean clean_production_deploy install_server
	cd ./lib && npm install -d --production
	rsync --delete -r --exclude=.svn ./lib/ /var/lib/gtc

clean_production_deploy:
	rm -rf /var/lib/gtc/*

.PHONY: tests docs
